//! Common imports for all the modules.

pub type OrError<T> = Result<T, anyhow::Error>;
pub type Json = serde_json::Value;
pub type KbBackend = tui::backend::CrosstermBackend<std::io::Stdout>;
pub type KbTerminal = tui::Terminal<KbBackend>;

pub use anyhow::{anyhow, bail, Context};
pub use log::{debug, error, info, warn};
pub use std::convert::{TryFrom, TryInto};

pub use crate::newtypes::*;
pub use crate::palette;
pub use crate::smart_regex;
