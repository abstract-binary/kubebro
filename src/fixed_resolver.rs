//! A DNS resolver that always returns the same value regardless of
//! the host it was queried for.
//!
//! Workaround for https://github.com/briansmith/webpki/issues/54
//! and https://github.com/ctz/rustls/issues/281

use crate::import::*;

use std::net::SocketAddr;

pub struct FixedResolver {
    ip_address: SocketAddr,
}

impl FixedResolver {
    pub fn new(ip_address: SocketAddr) -> OrError<Self> {
        Ok(Self { ip_address })
    }
}

impl ureq::Resolver for FixedResolver {
    fn resolve(&self, _netloc: &str) -> std::io::Result<Vec<SocketAddr>> {
        Ok(vec![self.ip_address])
    }
}
