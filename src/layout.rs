use crate::import::*;

use tui::layout::Rect;

pub fn center_vertically(height: u16, rect: Rect) -> OrError<Rect> {
    if rect.height < height {
        bail!(
            "Not enough space to center vertically. Cannot fit height {} into {:?}",
            height,
            rect
        );
    }
    Ok(Rect {
        x: rect.x,
        y: rect.y + (rect.height - height) / 2,
        width: rect.width,
        height,
    })
}

pub fn center_horizontally(width: u16, rect: Rect) -> Rect {
    Rect {
        x: rect.x + (std::cmp::max(0, rect.width as i64 - width as i64) as u16) / 2,
        y: rect.y,
        width,
        height: rect.height,
    }
}
