use crate::import::*;

use crossterm::event::{KeyCode, KeyEvent};
use std::cmp::Ordering;
use std::fmt;

pub use KeyCode::*;

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub struct Key {
    key_code: KeyCode,
    ctrl: bool,
    alt: bool,
}

impl From<char> for Key {
    fn from(ch: char) -> Self {
        Self {
            key_code: Char(ch),
            ctrl: false,
            alt: false,
        }
    }
}

impl From<KeyCode> for Key {
    fn from(key_code: KeyCode) -> Self {
        Self {
            key_code,
            ctrl: false,
            alt: false,
        }
    }
}

impl From<KeyEvent> for Key {
    fn from(key_event: KeyEvent) -> Self {
        use crossterm::event::KeyModifiers as Mod;
        let ctrl = key_event.modifiers.contains(Mod::CONTROL);
        let alt = key_event.modifiers.contains(Mod::ALT);
        let shift = key_event.modifiers.contains(Mod::SHIFT);
        let key_code = match key_event.code {
            Char(ch) => {
                if shift {
                    Char(ch.to_ascii_uppercase())
                } else {
                    Char(ch.to_ascii_lowercase())
                }
            }
            key_code => key_code,
        };
        Self {
            key_code,
            ctrl,
            alt,
        }
    }
}

impl From<Key> for KeyEvent {
    fn from(key: Key) -> Self {
        use crossterm::event::KeyModifiers as Mod;
        let mut modifiers = Mod::NONE;
        if key.ctrl {
            modifiers |= Mod::CONTROL;
        }
        if key.alt {
            modifiers |= Mod::ALT;
        }
        match key.key_code {
            Char(ch) => {
                if ch.is_uppercase() {
                    modifiers |= Mod::SHIFT;
                }
                let code = Char(ch);
                Self { code, modifiers }
            }
            code => Self { code, modifiers },
        }
    }
}

impl std::str::FromStr for Key {
    type Err = anyhow::Error;

    fn from_str(str: &str) -> OrError<Self> {
        match str.chars().collect::<Vec<char>>().as_slice() {
            [ch] => Ok((*ch).into()),
            ['R', 'e', 't'] => Ok(Enter.into()),
            ['E', 's', 'c'] => Ok(Esc.into()),
            ['B', 'k', 's', 'p'] => Ok(Backspace.into()),
            ['H', 'o', 'm', 'e'] => Ok(Home.into()),
            ['E', 'n', 'd'] => Ok(End.into()),
            ['S', 'p', 'c'] => Ok(' '.into()),
            ['U', 'p'] => Ok(Up.into()),
            ['D', 'o', 'w', 'n'] => Ok(Down.into()),
            ['L', 'e', 'f', 't'] => Ok(Left.into()),
            ['R', 'i', 'g', 'h', 't'] => Ok(Right.into()),
            ['P', 'a', 'g', 'e', 'U', 'p'] => Ok(PageUp.into()),
            ['P', 'a', 'g', 'e', 'D', 'o', 'w', 'n'] => Ok(PageDown.into()),
            ['C', '-', ch] => Ok(Self {
                key_code: Char(*ch),
                ctrl: true,
                alt: false,
            }),
            ['M', '-', ch] => Ok(Self {
                key_code: Char(*ch),
                ctrl: false,
                alt: true,
            }),
            chars => bail!("Cannot parse {:?} as Key", chars),
        }
    }
}

impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.ctrl {
            write!(f, "C-")?;
        }
        if self.alt {
            write!(f, "M-")?;
        }
        match self.key_code {
            Enter => write!(f, "Ret"),
            Backspace => write!(f, "Bksp"),
            Char(' ') => write!(f, "Spc"),
            Char(ch) => write!(f, "{}", ch),
            F(n) => write!(f, "F{}", n),
            key_code => write!(f, "{:?}", key_code),
        }
    }
}

impl PartialOrd for Key {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.to_string().partial_cmp(&other.to_string())
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Keys(Vec<Key>);

impl Keys {
    pub fn new(keys: &[&str]) -> OrError<Self> {
        let keys = keys
            .iter()
            .map(|k| k.parse())
            .collect::<OrError<Vec<Key>>>()?;
        Ok(Self(keys))
    }

    pub fn iter(&self) -> impl Iterator<Item = &Key> {
        self.0.iter()
    }
}

macro_rules! keys {
    ($($e:expr),*) => {
	crate::key::Keys::new(&[$($e),*]).unwrap()
    }
}

impl fmt::Display for Keys {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut iter = self.0.iter();
        if let Some(k) = iter.next() {
            write!(f, "{}", k)?;
            for k in iter {
                write!(f, ",{}", k)?;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        assert_eq!(
            Key::from('a'),
            Key {
                key_code: Char('a'),
                ctrl: false,
                alt: false
            }
        );
        assert_eq!(
            Key::from('A'),
            Key {
                key_code: Char('A'),
                ctrl: false,
                alt: false
            }
        );
        assert_eq!(
            "A".parse::<Key>().unwrap(),
            Key {
                key_code: Char('A'),
                ctrl: false,
                alt: false
            }
        );
        assert_eq!(
            "C-A".parse::<Key>().unwrap(),
            Key {
                key_code: Char('A'),
                ctrl: true,
                alt: false
            }
        );
        assert_eq!(
            "M-A".parse::<Key>().unwrap(),
            Key {
                key_code: Char('A'),
                ctrl: false,
                alt: true
            }
        );
    }
}
