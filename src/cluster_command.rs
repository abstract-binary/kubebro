use crate::cluster_data::*;
use crate::endpoint::Endpoint;
use crate::import::*;
use crate::path::{AllLogsPath, Path};
use crate::tagged_line::TaggedLine;
use crate::unique::Unique;

use std::fmt;
use std::hash::{Hash, Hasher};
use std::sync::{mpsc, Arc};
use std::thread;

macro_rules! enum_query {
    ($($query_type:ident),*) => {
        #[derive(Clone, Debug)]
        pub enum Query {
	    $($query_type($query_type),)*
        }

	impl ClusterCommand for Query {
	    type Response = Response;

	    fn execute(
		&self,
		endpoint: Arc<Endpoint>,
		responses_tx: mpsc::SyncSender<Response>
	    ) -> OrError<()> {
		match self {
		    $(Query::$query_type(x) => {
                        let x = x.clone();
			let (sub_responses_tx, sub_responses_rx) = mpsc::sync_channel(1);
                        // TODO This thread here is annoying because
                        // we're using a real thing (a thread) to work
                        // around a type problem (the required `into`
                        // call).
                        let executer = thread::spawn(move || {
                            x.execute(endpoint, sub_responses_tx)
                        });
                        for resp in sub_responses_rx.into_iter() {
                            responses_tx.send(resp.into())?;
                        }
                        match executer.join() {
                            Ok(Ok(())) => Ok(()),
                            Ok(Err(err)) => Err(err),
                            Err(err) => bail!("ClusterCommand executer thread crashed: {:?}", err)
                        }
		    })*
		}
	    }

	    fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
		match self {
		    $(Query::$query_type(x) => x.streaming(),)*
		}
	    }
	}

    };
}

enum_query!(
    ListPods,
    ListServices,
    ListIngresses,
    ListDeployments,
    ListDaemonSets,
    ListStatefulSets,
    ListNamespaces,
    ListNodes,
    ListServiceAccounts,
    ListRoleBindings,
    ListRoles,
    ListConfigMaps,
    ListPersistentVolumeClaims,
    ListPersistentVolumes,
    ListSecrets,
    GetPod,
    GetService,
    GetIngress,
    GetDeployment,
    GetDaemonSet,
    GetNode,
    GetStatefulSet,
    GetServiceAccount,
    GetRoleBinding,
    GetRole,
    GetConfigMap,
    GetPersistentVolumeClaim,
    GetPersistentVolume,
    GetSecret,
    RestartDeployment,
    RestartDaemonSet,
    RestartStatefulSet,
    ReloadIngress,
    DeletePod,
    DeletePersistentVolume,
    StreamAllLogs,
    SetNodeUnschedulable,
    SetNodeSchedulable
);

pub trait TryFromResponse {
    fn try_from_response(resp: &Response) -> OrError<&Self>;
}

macro_rules! enum_response {
    ($($query:ident, $response:ident);* ;) => {
        // TODO-soon Maybe box these variants so that their sizes
        // don't vary by too much.
        #[allow(clippy::large_enum_variant, clippy::enum_variant_names)]
        #[derive(Clone, Debug)]
        pub enum Response {
            $($query($response),)*
        }

	impl Response {
	    pub fn json(&self) -> Option<&Json> {
		match self {
		    $(Response::$query(ref x) => x.json(),)*
		}
	    }
	}

	$(
	impl TryFromResponse for $response {
	    fn try_from_response(resp: &Response) -> OrError<&Self> {
		match resp {
		    Response::$query(x) => Ok(x),
		    _ => bail!("Unexpected Response kind"),
		}
	    }
	}
	)*
    };
}

enum_response!(
    ListPods, ListPodsResponse;
    ListServices, ListServicesResponse;
    ListIngresses, ListIngressesResponse;
    ListDeployments, ListDeploymentsResponse;
    ListDaemonSets, ListDaemonSetsResponse;
    ListStatefulSets, ListStatefulSetsResponse;
    ListNamespaces, ListNamespacesResponse;
    ListNodes, ListNodesResponse;
    ListServiceAccounts, ListServiceAccountsResponse;
    ListRoleBindings, ListRoleBindingsResponse;
    ListRoles, ListRolesResponse;
    ListConfigMaps, ListConfigMapsResponse;
    ListPersistentVolumeClaims, ListPersistentVolumeClaimsResponse;
    ListPersistentVolumes, ListPersistentVolumesResponse;
    ListSecrets, ListSecretsResponse;
    GetPod, GetPodResponse;
    GetService, GetServiceResponse;
    GetIngress, GetIngressResponse;
    GetDeployment, GetDeploymentResponse;
    GetDaemonSet, GetDaemonSetResponse;
    GetNode, GetNodeResponse;
    GetStatefulSet, GetStatefulSetResponse;
    GetServiceAccount, GetServiceAccountResponse;
    GetRoleBinding, GetRoleBindingResponse;
    GetRole, GetRoleResponse;
    GetConfigMap, GetConfigMapResponse;
    GetPersistentVolumeClaim, GetPersistentVolumeClaimResponse;
    GetPersistentVolume, GetPersistentVolumeResponse;
    GetSecret, GetSecretResponse;
    GenericExecuteResponse, GenericExecuteResponse;
    StreamLine, StreamLine;
);

pub trait ClusterCommand {
    type Response;

    fn execute(
        &self,
        endpoint: Arc<Endpoint>,
        results: mpsc::SyncSender<Self::Response>,
    ) -> OrError<()>;

    /// This is used by `PrettyPager`.  If it returns `Some(_)`, then
    /// the pager will call `Cache::stream_lines`; otherwise, it will
    /// call `Cache::get` on the current `Path`.
    fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>>;
}

pub trait OneoffCommand
where
    Self: fmt::Debug + fmt::Display + Send + Sync,
    Self: ClusterCommand<Response = GenericExecuteResponse>,
{
    fn id(&self) -> &Unique;

    fn context(&self) -> &KContext;
}

impl Hash for Box<dyn OneoffCommand> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id().hash(state);
    }
}

impl PartialEq for Box<dyn OneoffCommand> {
    fn eq(&self, other: &Self) -> bool {
        self.id() == other.id()
    }
}

impl Eq for Box<dyn OneoffCommand> {}

#[derive(Clone, Debug)]
pub struct GenericExecuteResponse {
    pub json: Json,
}

impl GenericExecuteResponse {
    pub fn json(&self) -> Option<&Json> {
        Some(&self.json)
    }
}

impl From<GenericExecuteResponse> for Response {
    fn from(x: GenericExecuteResponse) -> Self {
        Response::GenericExecuteResponse(x)
    }
}

macro_rules! impl_oneoff_command {
    ($type:ty) => {
        impl OneoffCommand for $type {
            fn id(&self) -> &Unique {
                &self.id
            }

            fn context(&self) -> &KContext {
                &self.context
            }
        }
    };
}

pub trait ListResponseItems {
    type Item;

    fn items(&self) -> &Vec<Self::Item>;
}

macro_rules! list_command {
    ($query_type:ident,
     $response_type:ident,
     $object_struct:ident,
     $api_path_all:literal,
     $api_path_namespace:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            /// If `None`, all namespaces will be queried.
            namespace: Option<KNamespace>,
        }

        impl $query_type {
            pub fn new(namespace: Option<KNamespace>) -> Self {
                Self { namespace }
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        #[derive(Clone, Debug)]
        pub struct $response_type {
            pub items: Vec<$object_struct>,
            pub json: Json,
        }

        impl $response_type {
            pub fn json(&self) -> Option<&Json> {
                Some(&self.json)
            }
        }

        impl From<$response_type> for Response {
            fn from(x: $response_type) -> Self {
                Response::$query_type(x)
            }
        }

        impl ListResponseItems for $response_type {
            type Item = $object_struct;

            fn items(&self) -> &Vec<Self::Item> {
                &self.items
            }
        }

        impl ClusterCommand for $query_type {
            type Response = $response_type;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                // TODO-soon This and other list queries should take
                // into account list continuations.
                let api_path = match self.namespace {
                    None => $api_path_all.to_string(),
                    Some(ref namespace) => {
                        format!($api_path_namespace, namespace = namespace)
                    }
                };
                let json: Json = endpoint
                    .request("GET", &api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                // jq ".items | .[] | .metadata.name"
                let items = json["items"]
                    .as_array()
                    .map(|v| {
                        v.into_iter()
                            .filter_map(|v| match $object_struct::try_from(v) {
                                Err(err) => {
                                    error!("Failed to parse list item: {}", err);
                                    None
                                }
                                Ok(item) => Some(item),
                            })
                            .collect()
                    })
                    .unwrap_or_else(Vec::new);
                responses_tx.try_send(Self::Response { json, items })?;
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };

    ($query_type:ident,
     $response_type:ident,
     $object_struct:ident,
     $api_path_all:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type;

        impl $query_type {
            #[allow(clippy::new_without_default)]
            pub fn new() -> Self {
                Self
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        #[derive(Clone, Debug)]
        pub struct $response_type {
            pub items: Vec<$object_struct>,
            pub json: Json,
        }

        impl $response_type {
            pub fn json(&self) -> Option<&Json> {
                Some(&self.json)
            }
        }

        impl From<$response_type> for Response {
            fn from(x: $response_type) -> Self {
                Response::$query_type(x)
            }
        }

        impl ListResponseItems for $response_type {
            type Item = $object_struct;

            fn items(&self) -> &Vec<Self::Item> {
                &self.items
            }
        }

        impl ClusterCommand for $query_type {
            type Response = $response_type;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                // TODO-soon This and other list queries should take
                // into account list continuations.
                let api_path = $api_path_all.to_string();
                let json: Json = endpoint
                    .request("GET", &api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                // jq ".items | .[] | .metadata.name"
                let items = json["items"]
                    .as_array()
                    .map(|v| {
                        v.into_iter()
                            .filter_map(|v| match $object_struct::try_from(v) {
                                Err(err) => {
                                    error!("Failed to parse list item: {}", err);
                                    None
                                }
                                Ok(item) => Some(item),
                            })
                            .collect()
                    })
                    .unwrap_or_else(Vec::new);
                responses_tx.try_send(Self::Response { json, items })?;
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-pod-v1-core
list_command!(
    ListPods,
    ListPodsResponse,
    Pod,
    "/api/v1/pods",
    "/api/v1/namespaces/{namespace}/pods"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-service-v1-core
list_command!(
    ListServices,
    ListServicesResponse,
    Service,
    "/api/v1/services",
    "/api/v1/namespaces/{namespace}/services"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-ingress-v1-networking-k8s-io
list_command!(
    ListIngresses,
    ListIngressesResponse,
    Ingress,
    "/apis/networking.k8s.io/v1/ingresses",
    "/apis/networking.k8s.io/v1/namespaces/{namespace}/ingresses"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-ingress-v1-networking-k8s-io
list_command!(
    ListDeployments,
    ListDeploymentsResponse,
    Deployment,
    "/apis/apps/v1/deployments",
    "/apis/apps/v1/namespaces/{namespace}/deployments"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-daemonset-v1-apps
list_command!(
    ListDaemonSets,
    ListDaemonSetsResponse,
    DaemonSet,
    "/apis/apps/v1/daemonsets",
    "/apis/apps/v1/namespaces/{namespace}/daemonsets"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-statefulset-v1-apps
list_command!(
    ListStatefulSets,
    ListStatefulSetsResponse,
    StatefulSet,
    "/apis/apps/v1/statefulsets",
    "/apis/apps/v1/namespaces/{namespace}/statefulsets"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-namespace-v1-core
list_command!(
    ListNamespaces,
    ListNamespacesResponse,
    Namespace,
    "/api/v1/namespaces"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.21/#list-node-v1-core
list_command!(ListNodes, ListNodesResponse, Node, "/api/v1/nodes");

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-serviceaccount-v1-core
list_command!(
    ListServiceAccounts,
    ListServiceAccountsResponse,
    ServiceAccount,
    "/api/v1/serviceaccounts",
    "/api/v1/namespaces/{namespace}/serviceaccounts"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-rolebinding-v1-rbac-authorization-k8s-io
list_command!(
    ListRoleBindings,
    ListRoleBindingsResponse,
    RoleBinding,
    "/apis/rbac.authorization.k8s.io/v1/rolebindings",
    "/apis/rbac.authorization.k8s.io/v1/namespaces/{namespace}/rolebindings"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-role-v1-rbac-authorization-k8s-io
list_command!(
    ListRoles,
    ListRolesResponse,
    Role,
    "/apis/rbac.authorization.k8s.io/v1/roles",
    "/apis/rbac.authorization.k8s.io/v1/namespaces/{namespace}/roles"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-configmap-v1-core
list_command!(
    ListConfigMaps,
    ListConfigMapsResponse,
    ConfigMap,
    "/api/v1/configmaps",
    "/api/v1/namespaces/{namespace}/configmaps"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#list-all-namespaces-persistentvolumeclaim-v1-core
list_command!(
    ListPersistentVolumeClaims,
    ListPersistentVolumeClaimsResponse,
    PersistentVolumeClaim,
    "/api/v1/persistentvolumeclaims",
    "/api/v1/namespaces/{namespace}/persistentvolumeclaims"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.21/#list-persistentvolume-v1-core
list_command!(
    ListPersistentVolumes,
    ListPersistentVolumesResponse,
    PersistentVolume,
    "/api/v1/persistentvolumes"
);

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#secret-v1-core
list_command!(
    ListSecrets,
    ListSecretsResponse,
    Secret,
    "/api/v1/secrets",
    "/api/v1/namespaces/{namespace}/secrets"
);

macro_rules! get_command {
    (namespaced; $query_type:ident, $response_type:ident, $object_struct:ident, $api_path:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            pub name: KName,
            pub namespace: KNamespace,
            pub context: KContext,
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        #[derive(Clone, Debug)]
        pub struct $response_type {
            pub item: $object_struct,
            pub json: Json,
        }

        impl $response_type {
            pub fn json(&self) -> Option<&Json> {
                Some(&self.json)
            }
        }

        impl From<$response_type> for Response {
            fn from(x: $response_type) -> Self {
                Response::$query_type(x)
            }
        }

        impl ClusterCommand for $query_type {
            type Response = $response_type;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path =
                    &format!($api_path, namespace = self.namespace, name = self.name);
                let json: Json = endpoint
                    .request("GET", api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                let item = $object_struct::try_from(&json)?;
                if let Err(err) = responses_tx.send(Self::Response { json, item }) {
                    error!("Error sending response to execution {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };

    (cluster; $query_type:ident, $response_type:ident, $object_struct:ident, $api_path:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            pub name: KName,
            pub context: KContext,
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        #[derive(Clone, Debug)]
        pub struct $response_type {
            pub item: $object_struct,
            pub json: Json,
        }

        impl $response_type {
            pub fn json(&self) -> Option<&Json> {
                Some(&self.json)
            }
        }

        impl From<$response_type> for Response {
            fn from(x: $response_type) -> Self {
                Response::$query_type(x)
            }
        }

        impl ClusterCommand for $query_type {
            type Response = $response_type;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path = &format!($api_path, name = self.name);
                let json: Json = endpoint
                    .request("GET", api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                let item = $object_struct::try_from(&json)?;
                if let Err(err) = responses_tx.send(Self::Response { json, item }) {
                    error!("Error sending response to execution {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };
}

get_command!(
    namespaced;
    GetPod,
    GetPodResponse,
    Pod,
    "/api/v1/namespaces/{namespace}/pods/{name}"
);

get_command!(
    namespaced;
    GetService,
    GetServiceResponse,
    Service,
    "/api/v1/namespaces/{namespace}/services/{name}"
);

get_command!(
    namespaced;
    GetIngress,
    GetIngressResponse,
    Ingress,
    "/apis/networking.k8s.io/v1/namespaces/{namespace}/ingresses/{name}"
);

get_command!(
    namespaced;
    GetDeployment,
    GetDeploymentResponse,
    Deployment,
    "/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
);

get_command!(
    namespaced;
    GetDaemonSet,
    GetDaemonSetResponse,
    DaemonSet,
    "/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
);

get_command!(
    namespaced;
    GetStatefulSet,
    GetStatefulSetResponse,
    StatefulSet,
    "/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
);

get_command!(
    namespaced;
    GetServiceAccount,
    GetServiceAccountResponse,
    ServiceAccount,
    "/api/v1/namespaces/{namespace}/serviceaccounts/{name}"
);

get_command!(
    namespaced;
    GetRoleBinding,
    GetRoleBindingResponse,
    RoleBinding,
    "/apis/rbac.authorization.k8s.io/v1/namespaces/{namespace}/rolebindings/{name}"
);

get_command!(
    namespaced;
    GetRole,
    GetRoleResponse,
    Role,
    "/apis/rbac.authorization.k8s.io/v1/namespaces/{namespace}/roles/{name}"
);

get_command!(
    namespaced;
    GetConfigMap,
    GetConfigMapResponse,
    ConfigMap,
    "/api/v1/namespaces/{namespace}/configmaps/{name}"
);

get_command!(
    namespaced;
    GetPersistentVolumeClaim,
    GetPersistentVolumeClaimResponse,
    PersistentVolumeClaim,
    "/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}"
);

get_command!(
    cluster;
    GetPersistentVolume,
    GetPersistentVolumeResponse,
    PersistentVolume,
    "/api/v1/persistentvolumes/{name}"
);

get_command!(
    namespaced;
    GetSecret,
    GetSecretResponse,
    Secret,
    "/api/v1/namespaces/{namespace}/secrets/{name}"
);

get_command!(
    cluster;
    GetNode,
    GetNodeResponse,
    Node,
    "/api/v1/nodes/{name}"
);

macro_rules! patch_command {
    (namespace; $query_type:ident, $api_path:literal, $patch:expr) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            id: Unique,
            name: KName,
            namespace: KNamespace,
            context: KContext,
        }

        impl $query_type {
            pub fn oneoff(
                name: KName,
                namespace: KNamespace,
                context: KContext,
            ) -> Arc<Box<dyn OneoffCommand>> {
                Arc::new(Box::new(Self {
                    id: Unique::new(),
                    name,
                    namespace,
                    context,
                }))
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        impl fmt::Display for $query_type {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(
                    f,
                    "RESTART {}/{}/{}",
                    self.context, self.namespace, self.name
                )
            }
        }

        impl_oneoff_command!($query_type);

        impl ClusterCommand for $query_type {
            type Response = GenericExecuteResponse;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path =
                    &format!($api_path, namespace = self.namespace, name = self.name);
                let payload = $patch;
                debug!("PATCH {}: {}", api_path, payload);
                let json: Json = endpoint
                    .request("PATCH", api_path)
                    .set("Accept", "application/json")
                    .set("Content-Type", "application/strategic-merge-patch+json")
                    .send_string(&payload)?
                    .into_json()?;
                if let Err(err) = responses_tx.send(Self::Response { json }) {
                    error!("Error sending response to path {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };

    (cluster; $query_type:ident, $api_path:literal, $patch:expr) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            id: Unique,
            name: KName,
            context: KContext,
        }

        impl $query_type {
            pub fn oneoff(name: KName, context: KContext) -> Arc<Box<dyn OneoffCommand>> {
                Arc::new(Box::new(Self {
                    id: Unique::new(),
                    name,
                    context,
                }))
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        impl fmt::Display for $query_type {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, "PATCH {}/@cluster/{}", self.context, self.name)
            }
        }

        impl_oneoff_command!($query_type);

        impl ClusterCommand for $query_type {
            type Response = GenericExecuteResponse;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path = &format!($api_path, name = self.name);
                let payload = $patch;
                debug!("PATCH {}: {}", api_path, payload);
                let json: Json = endpoint
                    .request("PATCH", api_path)
                    .set("Accept", "application/json")
                    .set("Content-Type", "application/strategic-merge-patch+json")
                    .send_string(&payload)?
                    .into_json()?;
                if let Err(err) = responses_tx.send(Self::Response { json }) {
                    error!("Error sending response to path {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };
}

macro_rules! restart_command {
    ($query_type:ident, $api_path:literal) => {
        patch_command!(
            namespace;
            $query_type,
            $api_path,
            (format!(
                r#"{{
                     "spec": {{
                       "template": {{
                         "metadata": {{
                           "annotations": {{
                             "kubebro.abstractbinary.org/restartedAt": "{}"
                           }}
                         }}
                       }}
                     }}
                   }}"#,
                chrono::Utc::now()
            ))
        );
    };
}

restart_command!(
    RestartDeployment,
    "/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
);

restart_command!(
    RestartDaemonSet,
    "/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
);

restart_command!(
    RestartStatefulSet,
    "/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
);

macro_rules! reload_command {
    ($query_type:ident, $api_path:literal) => {
        patch_command!(
            namespace;
            $query_type,
            $api_path,
            (format!(
                r#"{{
                     "metadata": {{
                       "annotations": {{
                         "kubebro.abstractbinary.org/reloadedAt": "{}"
                       }}
                     }}
                   }}"#,
                chrono::Utc::now()
            ))
        );
    };
}

reload_command!(
    ReloadIngress,
    "/apis/networking.k8s.io/v1/namespaces/{namespace}/ingresses/{name}"
);

patch_command!(
    cluster;
    SetNodeUnschedulable,
    "/api/v1/nodes/{name}",
    r#"{"spec": {"unschedulable": true}}"#
);

patch_command!(
    cluster;
    SetNodeSchedulable,
    "/api/v1/nodes/{name}",
    r#"{"spec": {"unschedulable": false}}"#
);

macro_rules! delete_command {
    (namespace; $query_type:ident, $api_path:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            id: Unique,
            name: KName,
            namespace: KNamespace,
            context: KContext,
        }

        impl $query_type {
            pub fn oneoff(
                name: KName,
                namespace: KNamespace,
                context: KContext,
            ) -> Arc<Box<dyn OneoffCommand>> {
                Arc::new(Box::new(Self {
                    id: Unique::new(),
                    name,
                    namespace,
                    context,
                }))
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        impl fmt::Display for $query_type {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(
                    f,
                    "DELETE {}/{}/{}",
                    self.context, self.namespace, self.name
                )
            }
        }

        impl_oneoff_command!($query_type);

        impl ClusterCommand for $query_type {
            type Response = GenericExecuteResponse;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path =
                    &format!($api_path, namespace = self.namespace, name = self.name);
                debug!("DELETE {}", api_path);
                let json: Json = endpoint
                    .request("DELETE", api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                if let Err(err) = responses_tx.send(Self::Response { json }) {
                    error!("Error sending response to delete {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };

    (cluster; $query_type:ident, $api_path:literal) => {
        #[derive(Clone, Debug)]
        pub struct $query_type {
            id: Unique,
            name: KName,
            context: KContext,
        }

        impl $query_type {
            pub fn oneoff(name: KName, context: KContext) -> Arc<Box<dyn OneoffCommand>> {
                Arc::new(Box::new(Self {
                    id: Unique::new(),
                    name,
                    context,
                }))
            }
        }

        impl From<$query_type> for Query {
            fn from(x: $query_type) -> Self {
                Query::$query_type(x)
            }
        }

        impl fmt::Display for $query_type {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                write!(f, "DELETE {}/@cluster/{}", self.context, self.name)
            }
        }

        impl_oneoff_command!($query_type);

        impl ClusterCommand for $query_type {
            type Response = GenericExecuteResponse;

            fn execute(
                &self,
                endpoint: Arc<Endpoint>,
                responses_tx: mpsc::SyncSender<Self::Response>,
            ) -> OrError<()> {
                let api_path = &format!($api_path, name = self.name);
                debug!("DELETE {}", api_path);
                let json: Json = endpoint
                    .request("DELETE", api_path)
                    .set("Accept", "application/json")
                    .call()?
                    .into_json()?;
                if let Err(err) = responses_tx.send(Self::Response { json }) {
                    error!("Error sending response to delete {}: {}", api_path, err);
                }
                Ok(())
            }

            fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
                None
            }
        }
    };
}

delete_command!(
    namespace;
    DeletePod,
    "/api/v1/namespaces/{namespace}/pods/{name}"
);

delete_command!(
    cluster;
    DeletePersistentVolume,
    "/api/v1/persistentvolumes/{name}"
);

pub trait StreamCommand
where
    Self: fmt::Debug + fmt::Display + Send + Sync,
    Self: ClusterCommand<Response = StreamLine>,
{
    fn path(&self) -> Arc<Box<dyn Path>>;

    fn context(&self) -> &KContext;
}

impl Hash for Box<dyn StreamCommand> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.path().hash(state);
    }
}

impl PartialEq for Box<dyn StreamCommand> {
    fn eq(&self, other: &Self) -> bool {
        self.path() == other.path()
    }
}

impl Eq for Box<dyn StreamCommand> {}

#[derive(Clone, Debug)]
pub struct StreamLine {
    pub line: TaggedLine,
}

impl StreamLine {
    pub fn json(&self) -> Option<&Json> {
        None
    }
}

impl From<StreamLine> for Response {
    fn from(x: StreamLine) -> Self {
        Response::StreamLine(x)
    }
}

#[derive(Clone, Debug)]
pub struct StreamAllLogs {
    pod: KName,
    namespace: KNamespace,
    context: KContext,
}

impl StreamAllLogs {
    pub fn new(pod: &KName, namespace: &KNamespace, context: &KContext) -> Self {
        Self {
            pod: pod.clone(),
            namespace: namespace.clone(),
            context: context.clone(),
        }
    }
}

impl StreamCommand for StreamAllLogs {
    fn path(&self) -> Arc<Box<dyn Path>> {
        Arc::new(Box::new(AllLogsPath::new(
            &self.pod,
            &self.namespace,
            &self.context,
        )))
    }

    fn context(&self) -> &KContext {
        &self.context
    }
}

impl From<StreamAllLogs> for Query {
    fn from(x: StreamAllLogs) -> Self {
        Query::StreamAllLogs(x)
    }
}

impl fmt::Display for StreamAllLogs {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "STREAM {}/{}/{}", self.context, self.namespace, self.pod)
    }
}

impl ClusterCommand for StreamAllLogs {
    type Response = StreamLine;

    // https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.20/#read-log-pod-v1-core
    fn execute(
        &self,
        endpoint: Arc<Endpoint>,
        responses_tx: mpsc::SyncSender<Self::Response>,
    ) -> OrError<()> {
        let api_path = format!(
            "/api/v1/namespaces/{namespace}/pods/{pod}/log",
            namespace = self.namespace,
            pod = self.pod
        );
        debug!("TAIL {}", api_path);
        let pod: Pod = {
            let (get_pod_responses_tx, get_pod_responses_rx) = mpsc::sync_channel(1);
            let get_pod_cmd = GetPod {
                name: self.pod.clone(),
                namespace: self.namespace.clone(),
                context: self.context.clone(),
            };
            get_pod_cmd.execute(Arc::clone(&endpoint), get_pod_responses_tx)?;
            get_pod_responses_rx
                .recv()
                .map(Result::Ok)
                .unwrap_or_else(|err| {
                    Err(anyhow!("Could not find pod {:?}: {:?}", get_pod_cmd, err))
                })?
                .item
        };
        let init_containers: Vec<KContainer> = pod.init_containers.into_iter().collect();
        let containers: Vec<KContainer> = pod.containers.into_iter().collect();
        let num_containers = init_containers.len() + containers.len();
        if !init_containers.is_empty() || !containers.is_empty() {
            let max_src_len = containers.iter().map(|x| x.len()).max().unwrap();
            debug!(
                "Pod {} has containers. Init: {:?}.  Regular: {:?}",
                self.pod, init_containers, containers
            );
            let tail_containers = |containers: Vec<KContainer>| -> OrError<()> {
                let tasks: Vec<_> = containers
                    .into_iter()
                    .map(|container| {
                        let endpoint = Arc::clone(&endpoint);
                        let responses_tx = responses_tx.clone();
                        let api_path = api_path.clone();
                        thread::spawn(move || {
                            stream_all_logs_container(
                                endpoint,
                                api_path,
                                container,
                                num_containers,
                                max_src_len,
                                responses_tx,
                            )
                        })
                    })
                    .collect();
                for t in tasks {
                    if let Err(err) = t.join() {
                        bail!("Log tailing thread crashed: {:?}", err);
                    }
                }
                Ok(())
            };
            tail_containers(init_containers)?;
            tail_containers(containers)?;
        }
        Ok(())
    }

    fn streaming(&self) -> Option<Arc<Box<dyn StreamCommand>>> {
        Some(Arc::new(Box::new(self.clone())))
    }
}

fn stream_all_logs_container(
    endpoint: Arc<Endpoint>,
    api_path: String,
    container: KContainer,
    num_containers: usize,
    max_src_len: usize,
    responses_tx: mpsc::SyncSender<StreamLine>,
) -> OrError<()> {
    use std::io::{BufRead, BufReader};
    let bytes = endpoint
        .request("GET", &api_path)
        .set("Accept", "application/json")
        .query("follow", "true")
        .query("container", &container)
        .call()?
        .into_reader();
    let buf = BufReader::new(bytes);
    let lines = buf.lines();
    let src = if num_containers > 1 {
        Some(container.to_string())
    } else {
        None
    };
    let send_line = |line: String| -> Result<(), ()> {
        match responses_tx.send(StreamLine {
            line: TaggedLine {
                src: src.clone(),
                max_src_len,
                text: line,
            },
        }) {
            Ok(()) => Ok(()),
            Err(err) => {
                error!("Error sending response to stream {}: {}", api_path, err);
                Err(())
            }
        }
    };
    for line in lines {
        match line {
            Err(err) => {
                error!("Error streaming lines for {}: {}", api_path, err);
            }
            Ok(line) => {
                if send_line(line).is_err() {
                    break;
                }
            }
        }
    }
    let _: Result<(), ()> = send_line(format!(
        "--Kubebro-- Log for container {} reached EOF --Kubebro--",
        container
    ));
    info!("Done streaming lines for {}", api_path);
    Ok(())
}
