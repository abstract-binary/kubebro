#[allow(unused_imports)]
use crate::import::*;

use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::dialog::Dialog;
use crate::health_check::{HealthCheck, HealthResult};
use crate::key::Key;
use crate::key_bind::{KeyBind, KeyBindDesc};
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::path::Path;
use crate::table::{Index, Table, TableContext};
use crate::unique::Unique;
use crate::window_result::WindowResult;
use crate::window_result::WindowResult as WR;

use std::collections::HashMap;
use std::fmt;
use std::sync::Arc;
use std::time::Duration;

pub trait ResourceData
where
    Self: fmt::Debug,
{
    type ListResponse;

    const INDEX_HEADER: &'static str;
    const NON_INDEX_HEADERS: &'static [&'static str];

    fn to_row(&self) -> OrError<Vec<Cell>>;

    fn index(&self) -> Index;

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![]
    }

    fn handle_key_event(
        &self,
        _key: Key,
        _context: KContext,
        _cache: Arc<Cache>,
    ) -> WindowResult {
        WR::NotHandled
    }
}

#[derive(Debug)]
pub struct ResourceTable<D>
where
    D: ResourceData,
{
    path: Arc<Box<dyn Path>>,
    cache: Arc<Cache>,
    data: HashMap<Index, D>,
    extra_data: Option<(Index, D)>,
}

impl<D: ResourceData> ResourceTable<D> {
    pub fn new(cache: Arc<Cache>, path: Arc<Box<dyn Path>>) -> Self {
        Self {
            path,
            cache: Arc::clone(&cache),
            data: HashMap::new(),
            extra_data: None,
        }
    }

    pub fn new_with_extra_data(
        cache: Arc<Cache>,
        path: Arc<Box<dyn Path>>,
        extra_data: (Index, D),
    ) -> Self {
        Self {
            path,
            cache: Arc::clone(&cache),
            data: HashMap::new(),
            extra_data: Some(extra_data),
        }
    }
}

impl<D> ResourceTable<D>
where
    D: ResourceData + Clone + 'static,
    D::ListResponse: cc::TryFromResponse,
    D::ListResponse: cc::ListResponseItems<Item = D>,
{
    pub fn new_table(cache: Arc<Cache>, path: Arc<Box<dyn Path>>) -> Box<dyn Table> {
        Box::new(Self::new(cache, path))
    }

    pub fn new_table_with_extra_data(
        cache: Arc<Cache>,
        path: Arc<Box<dyn Path>>,
        extra_data: (Index, D),
    ) -> Box<dyn Table> {
        Box::new(Self::new_with_extra_data(cache, path, extra_data))
    }
}

impl<D> Table for ResourceTable<D>
where
    D: ResourceData + Clone + 'static,
    D::ListResponse: cc::TryFromResponse,
    D::ListResponse: cc::ListResponseItems<Item = D>,
{
    fn self_update(
        &mut self,
        refresh_rate: Duration,
        cached_id: &mut Unique,
        last_updated: &mut LastUpdated,
    ) -> OrError<()> {
        let res = self.cache.get(Arc::clone(&self.path), refresh_rate);
        let res_last_updated = (&res).into();
        use crate::cache::Result::*;
        match res {
            Err(error) => {
                error!("Failed to get {}: {}", self.path, error);
                *last_updated = res_last_updated;
            }
            Ok(Missing) => *last_updated = res_last_updated,
            Ok(Fresh(resp, _, new_cached_id)) | Ok(Stale(resp, _, new_cached_id)) => {
                if *cached_id != new_cached_id {
                    use cc::TryFromResponse;
                    let res: OrError<&D::ListResponse> =
                        D::ListResponse::try_from_response(&*resp);
                    match res {
                        Ok(resp) => {
                            use cc::ListResponseItems;
                            *cached_id = new_cached_id;
                            *last_updated = res_last_updated;
                            self.data = resp
                                .items()
                                .iter()
                                .map(|x| (x.index(), (*x).clone()))
                                .collect();
                            if let Some((ref extra_data_key, ref extra_data_value)) =
                                self.extra_data
                            {
                                self.data.insert(
                                    extra_data_key.clone(),
                                    extra_data_value.clone(),
                                );
                            }
                        }
                        Err(error) => {
                            error!("{} for {}: {:?}", error, self.path, resp);
                            *last_updated = LU::Updated(std::time::Instant::now());
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn index_header(&self) -> &str {
        D::INDEX_HEADER
    }

    fn non_index_headers(&self) -> &[&str] {
        D::NON_INDEX_HEADERS
    }

    fn rows(&self) -> Vec<(Index, OrError<Vec<Cell>>)> {
        let mut rows: Vec<_> = self
            .data
            .values()
            .map(|x| (x.index(), x.to_row()))
            .collect();
        rows.sort_by(|(idx1, _), (idx2, _)| idx1.cmp(idx2));
        rows
    }

    fn path(&self) -> Arc<Box<dyn Path>> {
        Arc::clone(&self.path)
    }

    fn key_binds(&self, context: TableContext) -> Vec<KeyBindDesc> {
        match context.selected.and_then(|idx| self.data.get(&idx)) {
            None => vec![],
            Some(d) => d.key_binds(),
        }
    }

    fn handle_key_event(&mut self, key: Key, context: TableContext) -> WindowResult {
        match context.selected.as_ref().and_then(|idx| self.data.get(idx)) {
            None => WR::NotHandled,
            Some(d) => {
                let context: Option<KContext> = self.path.context().cloned();
                match context {
                    None => {
                        error!("Unknown context for path {} when handling resource key event", self.path);
                        WR::NotHandled
                    }
                    Some(context) => {
                        d.handle_key_event(key, context, Arc::clone(&self.cache))
                    }
                }
            }
        }
    }
}

impl<D> HealthCheck for ResourceTable<D>
where
    D: HealthCheck + ResourceData,
{
    fn healthy(&self) -> HealthResult {
        let mut issues: Vec<_> = self
            .data
            .iter()
            .map(|(idx, d)| d.healthy().map(|error| format!("{}: {}", idx, error)))
            .collect();
        issues.sort();
        issues.into_iter().collect()
    }
}

pub fn prompt_and_act(
    name: KName,
    namespace: Option<KNamespace>,
    what: &str,
    action: &str,
    cc: Arc<Box<dyn cc::OneoffCommand>>,
    cache: Arc<Cache>,
) -> WindowResult {
    let what = Arc::new(what.to_string());
    let action = Arc::new(action.to_string());
    let execute_restart = {
        let name = name.clone();
        let action = Arc::clone(&action);
        move |()| {
            info!("Executing cluster command {:?}", cc);
            match cache
                .oneoff(Arc::clone(&cc), format!("Executed {} on {}", action, name))
            {
                Ok(_) => {}
                Err(error) => error!("Cluster command {:?} failed: {}", cc, error),
            }
            WR::CloseSelf
        }
    };
    let dont_restart = {
        let what = Arc::clone(&what);
        let action = Arc::clone(&action);
        move |()| {
            info!("Did not {} {}", action, what);
            WR::CloseSelf
        }
    };
    WR::PushOverlayWindow(Box::new(Dialog::new(
        "Confirmation",
        &format!(
            "Are you sure you want to {} {} {}/{}?",
            action,
            what,
            namespace.unwrap_or_else(|| "@cluster".into()),
            name
        ),
        vec![
            KeyBind::new(keys!["y"], "Yes", "", Box::new(execute_restart)),
            KeyBind::new(keys!["n"], "No", "", Box::new(dont_restart)),
        ],
    )))
}

pub fn prompt_and_delete(
    name: KName,
    namespace: Option<KNamespace>,
    what: &str,
    cc: Arc<Box<dyn cc::OneoffCommand>>,
    cache: Arc<Cache>,
) -> WindowResult {
    let what = Arc::new(what.to_string());
    let execute_delete = {
        let name = name.clone();
        move |()| {
            info!("Executing cluster command {:?}", cc);
            match cache.oneoff(Arc::clone(&cc), format!("Deleted {}", name)) {
                Ok(_) => {}
                Err(error) => error!("Cluster command {:?} failed: {}", cc, error),
            }
            WR::CloseSelf
        }
    };
    let dont_delete = {
        let what = Arc::clone(&what);
        move |()| {
            info!("Did not delete {}", what);
            WR::CloseSelf
        }
    };
    WR::PushOverlayWindow(Box::new(Dialog::new(
        "Confirmation",
        &format!(
            "Are you sure you want to delete {} {}/{}?",
            what,
            namespace.unwrap_or_else(|| "@cluster".into()),
            name
        ),
        vec![
            KeyBind::new(keys!["y"], "Yes", "", Box::new(execute_delete)),
            KeyBind::new(keys!["n"], "No", "", Box::new(dont_delete)),
        ],
    )))
}
