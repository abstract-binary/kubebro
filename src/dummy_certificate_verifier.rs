#[allow(unused_imports)]
use crate::import::*;

use rustls::{
    Certificate, RootCertStore, ServerCertVerified, ServerCertVerifier, TLSError,
};
use webpki::DNSNameRef;

pub struct DummyCertificateVerifier;

impl ServerCertVerifier for DummyCertificateVerifier {
    fn verify_server_cert(
        &self,
        _roots: &RootCertStore,
        _presented_certs: &[Certificate],
        _dns_name: DNSNameRef<'_>,
        _ocsp_response: &[u8],
    ) -> Result<ServerCertVerified, TLSError> {
        Ok(ServerCertVerified::assertion())
    }
}
