use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_data as cd;
use crate::health_check::HealthCheck;
use crate::import::*;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::path::Path;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::{Index, Table};
use crate::unique::Unique;

use std::sync::Arc;
use std::time::Duration;

const INDEX_HEADER: &str = "Resource";
const NON_INDEX_HEADERS: &[&str] = &["Count", "Healthy"];

pub enum TableScope {
    Namespace,
    Cluster,
}

#[derive(Debug)]
pub struct ClusterOverviewTable {
    path: Arc<Box<dyn Path>>,
    cache: Arc<Cache>,
    child_tables: Vec<(Index, Box<dyn ClusterOverviewChild>, Unique, LastUpdated)>,
    selected: Option<Index>,
}

impl ClusterOverviewTable {
    pub fn new_shared(
        scope: TableScope,
        cache: Arc<Cache>,
        path: Arc<Box<dyn Path>>,
    ) -> Box<dyn Table> {
        Box::new(Self::new(scope, cache, path))
    }

    fn new(scope: TableScope, cache: Arc<Cache>, path: Arc<Box<dyn Path>>) -> Self {
        let child_tables: Vec<(
            Index,
            Box<dyn ClusterOverviewChild>,
            Unique,
            LastUpdated,
        )> = {
            type Constructor =
                fn(Arc<Cache>, Arc<Box<(dyn Path)>>) -> Box<(dyn ClusterOverviewChild)>;
            let children = match scope {
                TableScope::Namespace => vec![
                    (
                        "deployments",
                        ResourceTable::<cd::Deployment>::new_child_table as Constructor,
                    ),
                    ("ingresses", ResourceTable::<cd::Ingress>::new_child_table),
                    ("pods", ResourceTable::<cd::Pod>::new_child_table),
                    ("services", ResourceTable::<cd::Service>::new_child_table),
                    (
                        "daemonsets",
                        ResourceTable::<cd::DaemonSet>::new_child_table as Constructor,
                    ),
                    (
                        "statefulsets",
                        ResourceTable::<cd::StatefulSet>::new_child_table,
                    ),
                    (
                        "serviceaccounts",
                        ResourceTable::<cd::ServiceAccount>::new_child_table,
                    ),
                    (
                        "rolebindings",
                        ResourceTable::<cd::RoleBinding>::new_child_table,
                    ),
                    ("roles", ResourceTable::<cd::Role>::new_child_table),
                    (
                        "configmaps",
                        ResourceTable::<cd::ConfigMap>::new_child_table,
                    ),
                    (
                        "persistentvolumeclaims",
                        ResourceTable::<cd::PersistentVolumeClaim>::new_child_table,
                    ),
                    ("secrets", ResourceTable::<cd::Secret>::new_child_table),
                ],

                TableScope::Cluster => vec![
                    (
                        "nodes",
                        ResourceTable::<cd::Node>::new_child_table as Constructor,
                    ),
                    (
                        "persistentvolumes",
                        ResourceTable::<cd::PersistentVolume>::new_child_table,
                    ),
                ],
            };
            children
                .into_iter()
                .filter_map(|(name, constructor)| match path.extend(name) {
                    Ok(child_path) => Some((
                        name.into(),
                        constructor(Arc::clone(&cache), Arc::new(child_path)),
                        Unique::new(),
                        LU::FetchingData,
                    )),
                    Err(error) => {
                        error!(
                            "Failed to construct child table {} of {}: {}",
                            name, path, error
                        );
                        None
                    }
                })
                .collect()
        };
        Self {
            child_tables,
            path,
            cache,
            selected: None,
        }
    }
}

impl Table for ClusterOverviewTable {
    fn index_header(&self) -> &str {
        INDEX_HEADER
    }

    fn non_index_headers(&self) -> &[&str] {
        NON_INDEX_HEADERS
    }

    fn rows(&self) -> Vec<(Index, OrError<Vec<Cell>>)> {
        self.child_tables
            .iter()
            .map(|(idx, child_table, _, _)| {
                (
                    idx.clone(),
                    row!(child_table.rows().len(), child_table.healthy()),
                )
            })
            .collect()
    }

    fn path(&self) -> Arc<Box<dyn Path>> {
        Arc::clone(&self.path)
    }

    fn self_update(
        &mut self,
        refresh_rate: Duration,
        cached_id: &mut Unique,
        last_updated: &mut LastUpdated,
    ) -> OrError<()> {
        for (_, child_table, ref mut cached_id, ref mut last_updated) in
            &mut self.child_tables
        {
            child_table.self_update(refresh_rate, cached_id, last_updated)?;
        }
        let new_last_updated = {
            self.child_tables
                .iter()
                .map(|(_, _, _, last_updated)| *last_updated)
                .max()
                .unwrap_or(LU::NeverUpdates)
        };
        if new_last_updated != *last_updated {
            *last_updated = new_last_updated;
            *cached_id = Unique::new();
        }
        Ok(())
    }
}

pub trait ClusterOverviewChild: Table + HealthCheck {}

impl<D: ResourceData> ResourceTable<D> {
    pub fn new_child_table(
        cache: Arc<Cache>,
        path: Arc<Box<dyn Path>>,
    ) -> Box<dyn ClusterOverviewChild>
    where
        ResourceTable<D>: ClusterOverviewChild,
    {
        Box::new(Self::new(cache, path))
    }
}
