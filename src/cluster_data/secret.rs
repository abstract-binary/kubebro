use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Secret {
    pub name: KName,
    pub namespace: KNamespace,
    pub data: HashMap<String, String>,
    pub type_: Option<String>,
}

impl TryFrom<&Json> for Secret {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Secret", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "Secret", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let data =
            select_as_map(json, "Secret", "$.data").unwrap_or_else(|_| HashMap::new());
        let type_ = select_as_str(json, "Secret", "$.type").ok();
        Ok(Self {
            name,
            namespace,
            data,
            type_,
        })
    }
}

impl ResourceData for Secret {
    type ListResponse = cc::ListSecretsResponse;

    const INDEX_HEADER: &'static str = "Secret";
    const NON_INDEX_HEADERS: &'static [&'static str] = &["Type", "Data"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        use crate::cell;
        row!(self.type_.as_ref(), cell::DataTable(&self.data))
    }
}

impl HealthCheck for Secret {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<Secret> {}
