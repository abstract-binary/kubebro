use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::condition_status::ConditionStatus;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::collections::HashMap;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Node {
    pub name: KName,
    pub pod_cidr: Option<String>,
    pub conditions: HashMap<String, ConditionStatus>,
    pub cpu_capacity: u64,
    pub mem_capacity: u64,
    pub ephemeral_storage_capacity: u64,
    pub taints: Vec<String>,
    pub unschedulable: bool,
}

impl TryFrom<&Json> for Node {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Node", "$.metadata.name")?.into();
        let pod_cidr = select_as_str_opt(json, "Node", "$.spec.podCIDR")?;
        let conditions = select_as_conditions(json, "Node", "$.status.conditions[*]");
        let cpu_capacity = select_as_u64_from_str(json, "Node", "$.status.capacity.cpu")?;
        let mem_capacity =
            select_as_bytes_size(json, "Node", "$.status.capacity.memory").unwrap_or(0);
        let ephemeral_storage_capacity =
            select_as_bytes_size(json, "Node", "$.status.capacity.ephemeral-storage")
                .unwrap_or(0);
        let taints = select_as_collect_str(json, "Node", "$.spec.taints[*].effect");
        let unschedulable =
            select_as_bool(json, "Node", "$.spec.unschedulable")?.unwrap_or(false);
        Ok(Self {
            name,
            pod_cidr,
            conditions,
            cpu_capacity,
            mem_capacity,
            ephemeral_storage_capacity,
            taints,
            unschedulable,
        })
    }
}

impl ResourceData for Node {
    type ListResponse = cc::ListNodesResponse;

    const INDEX_HEADER: &'static str = "Node";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[
        "Pod CIDR",
        "Ready",
        "CPUs",
        "Mem",
        "Temp Storage",
        "Pressure",
        "Taints",
    ];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        let pressure = self
            .conditions
            .iter()
            .filter_map(|(name, status)| {
                if name != "Ready" && status == &ConditionStatus::True {
                    Some(name.clone())
                } else {
                    None
                }
            })
            .collect::<Vec<String>>();
        row!(
            self.pod_cidr.as_ref(),
            self.conditions.get("Ready"),
            self.cpu_capacity,
            pretty_bytes(self.mem_capacity),
            pretty_bytes(self.ephemeral_storage_capacity),
            crate::cell::NonEmptyIsRed(&pressure),
            crate::cell::NonEmptyIsRed(&self.taints)
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(
            ["X"],
            "Cordon/Uncordon",
            "Cordon/uncordon the selected node"
        )]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "X" => resource_table::prompt_and_act(
                self.name.clone(),
                None,
                "node",
                if self.unschedulable {
                    "uncordon"
                } else {
                    "cordon"
                },
                if self.unschedulable {
                    cc::SetNodeSchedulable::oneoff(self.name.clone(), context)
                } else {
                    cc::SetNodeUnschedulable::oneoff(self.name.clone(), context)
                },
                cache,
            ),
            _ => WR::NotHandled,
        }
    }
}

impl HealthCheck for Node {
    fn healthy(&self) -> HealthResult {
        use ConditionStatus::*;
        use HealthResult as HR;
        match self.conditions.get("Ready") {
            Some(True) => HR::Healthy,
            None | Some(False) | Some(Unknown) => HR::Unhealthy("Not ready".into()),
        }
    }
}

impl ClusterOverviewChild for ResourceTable<Node> {}

fn pretty_bytes(bytes: u64) -> String {
    let bytes = bytes as f64;
    if bytes < 1000.0 {
        format!("{:.2} bytes", bytes)
    } else if bytes < 1_000_000.0 {
        format!("{:.2} KB", bytes / 1_000.0)
    } else if bytes < 1_000_000_000.0 {
        format!("{:.2} MB", bytes / 1_000_000.0)
    } else {
        format!("{:.2} GB", bytes / 1_000_000_000.0)
    }
}
