use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::import::*;
use crate::resource_table::ResourceData;
use crate::table::Index;

#[derive(Clone, Debug)]
pub struct Namespace {
    pub name: KName,
}

impl TryFrom<&Json> for Namespace {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Namespace", "$.metadata.name")?.into();
        Ok(Self { name })
    }
}

impl ResourceData for Namespace {
    type ListResponse = cc::ListNamespacesResponse;

    const INDEX_HEADER: &'static str = "Namespace";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!()
    }
}
