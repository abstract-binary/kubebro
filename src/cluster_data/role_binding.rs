use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

#[derive(Clone, Debug)]
pub struct RoleBinding {
    pub name: KName,
    pub namespace: KNamespace,
    pub service_accounts: Vec<(KNamespace, KName)>,
    pub users: Vec<KName>,
    pub roles: Vec<KName>,
}

impl TryFrom<&Json> for RoleBinding {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "RoleBinding", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "RoleBinding", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let service_accounts = {
            select_as_pairs(
                json,
                "RoleBinding",
                "$.subjects[?(@.kind=='ServiceAccount')]",
                ("namespace", "name"),
            )
            .into_iter()
            .map(|(ns, s)| (ns.into(), s.into()))
            .collect()
        };
        let users = select_as_collect_str(
            json,
            "RoleBinding",
            "$.subjects[?(@.kind=='User')].name",
        );
        let roles = select_as_collect_str(
            json,
            "RoleBinding",
            "$.roleRef[?(@.kind=='Role')].name",
        );
        Ok(Self {
            name,
            namespace,
            service_accounts,
            users,
            roles,
        })
    }
}

impl ResourceData for RoleBinding {
    type ListResponse = cc::ListRoleBindingsResponse;

    const INDEX_HEADER: &'static str = "Role Binding";
    const NON_INDEX_HEADERS: &'static [&'static str] =
        &["Roles", "Service Accounts", "Users"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(&self.roles, &self.service_accounts, &self.users)
    }
}

impl HealthCheck for RoleBinding {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<RoleBinding> {}
