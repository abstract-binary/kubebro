use crate::condition_status::ConditionStatus;
use crate::import::*;

use std::collections::HashMap;

pub fn select_as_str_opt(
    json: &Json,
    _object_api_name: &str,
    path: &str,
) -> OrError<Option<String>> {
    let res = jsonpath_lib::select(json, path)?;
    if res.len() == 1 {
        json_as_string(res[0]).map(Option::Some)
    } else {
        Ok(None)
    }
}

pub fn select_as_str(json: &Json, object_api_name: &str, path: &str) -> OrError<String> {
    match select_as_str_opt(json, object_api_name, path)? {
        Some(res) => Ok(res),
        None => bail!("{} missing field {}", object_api_name, path),
    }
}

pub fn select_as_collect_str<T, U>(json: &Json, object_api_name: &str, path: &str) -> T
where
    T: Default + std::iter::FromIterator<U>,
    U: From<String>,
{
    match jsonpath_lib::select(json, path) {
        Err(err) => {
            debug!("json_manip::select_as_vec_str error: {}", err);
            T::default()
        }
        Ok(res) => res
            .into_iter()
            .map(|v| json_as_string(v).map(|s| s.into()))
            .collect::<OrError<T>>()
            .unwrap_or_else(|error| {
                debug!("{} field {} expected vec: {}", object_api_name, path, error);
                T::default()
            }),
    }
}

pub fn select_as_selector(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> Option<String> {
    match jsonpath_lib::select(json, path) {
        Err(err) => {
            debug!("json_manip::select_as_selector error: {}", err);
            None
        }
        Ok(res) => {
            if res.len() == 1 {
                match res[0].as_object() {
                    None => {
                        debug!(
                            "{} field {} expected object: {}",
                            object_api_name, path, res[0]
                        );
                        None
                    }
                    Some(m) => m
                        .iter()
                        .map(|(k, v)| json_as_string(v).map(|v| format!("{}={}", k, v)))
                        .collect::<OrError<Vec<String>>>()
                        .map(|v| Some(v.join(", ")))
                        .unwrap_or_else(|error| {
                            debug!(
                                "{} field {} expected string: {}",
                                object_api_name, path, error
                            );
                            None
                        }),
                }
            } else {
                None
            }
        }
    }
}

/// Returns a primitive JSON value as a string (even if it was
/// originally a number or something else).  Doesn't work on
/// non-primitive values.
pub fn json_as_string(json: &Json) -> OrError<String> {
    match json.as_str() {
        Some(s) => Ok(s.into()),
        None => match json.as_u64() {
            Some(n) => Ok(n.to_string()),
            None => match json.as_i64() {
                Some(n) => Ok(n.to_string()),
                None => match json.as_f64() {
                    Some(f) => Ok(f.to_string()),
                    None => bail!("json_as_string: {}", json),
                },
            },
        },
    }
}

pub fn select_as_u64(json: &Json, object_api_name: &str, path: &str) -> OrError<u64> {
    let res = jsonpath_lib::select(json, path)?;
    if res.is_empty() {
        // Kubernetes regularly elides numeric fields when their value
        // is zero.
        Ok(0)
    } else if res.len() == 1 {
        res[0].as_u64().map(Result::Ok).unwrap_or_else(|| {
            bail!(
                "{} field {} expected u64: {}",
                object_api_name,
                path,
                res[0]
            )
        })
    } else {
        bail!("{} missing or duplicate field {}", object_api_name, path)
    }
}

pub fn select_as_u64_from_str(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> OrError<u64> {
    let res = jsonpath_lib::select(json, path)?;
    if res.is_empty() {
        // Kubernetes regularly elides numeric fields when their value
        // is zero.
        Ok(0)
    } else if res.len() == 1 {
        json_as_string(res[0])
            .and_then(|str| {
                str.parse::<u64>()
                    .map_err(|err| anyhow!("convert to u64: {}", err))
            })
            .map_err(|err| {
                anyhow!(
                    "{} field {} expected string convertible to u64: {}: {}",
                    object_api_name,
                    path,
                    res[0],
                    err
                )
            })
    } else {
        bail!("{} missing or duplicate field {}", object_api_name, path)
    }
}

pub fn select_as_bytes_size(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> OrError<u64> {
    let mut str = select_as_str(json, object_api_name, path)?;
    let mut mult = 1;
    if let Some(num) = str.strip_suffix("Gi") {
        str = num.to_string();
        mult = 1024 * 1024 * 1024;
    }
    if let Some(num) = str.strip_suffix("Mi") {
        str = num.to_string();
        mult = 1024 * 1024;
    }
    if let Some(num) = str.strip_suffix("Ki") {
        str = num.to_string();
        mult = 1024;
    }
    str.parse::<u64>()
        .map_err(|err| {
            anyhow!(
                "{} field {} failed to convert {} to u64: {}",
                object_api_name,
                path,
                str,
                err
            )
        })
        .map(|n| n * mult)
}

pub fn select_as_pairs(
    json: &Json,
    object_api_name: &str,
    path: &str,
    (key, value): (&str, &str),
) -> Vec<(String, String)> {
    match jsonpath_lib::select(json, path) {
        Err(err) => {
            debug!("json_manip::select_as_pairs error: {}", err);
            vec![]
        }
        Ok(res) => res
            .into_iter()
            .map(|v| match v.as_object() {
                None => bail!("expected object: {}", v),
                Some(m) => match (m.get(key), m.get(value)) {
                    (None, None) => {
                        bail!("missing both {} and {}: {:?}", key, value, m)
                    }
                    (None, Some(value)) => Ok(("".into(), json_as_string(value)?)),
                    (Some(key), None) => Ok((json_as_string(key)?, "".into())),
                    (Some(key), Some(value)) => {
                        Ok((json_as_string(key)?, json_as_string(value)?))
                    }
                },
            })
            .filter_map(|pair| match pair {
                Ok(pair) => Some(pair),
                Err(error) => {
                    debug!("{} field {} {}", object_api_name, path, error);
                    None
                }
            })
            .collect(),
    }
}

pub fn select_project_as_map(
    json: &Json,
    object_api_name: &str,
    path: &str,
    key: &str,
    value: &str,
) -> HashMap<String, String> {
    select_as_pairs(json, object_api_name, path, (key, value))
        .into_iter()
        .collect()
}

pub fn select_as_map(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> OrError<HashMap<String, String>> {
    let res = jsonpath_lib::select(json, path)?;
    if res.len() == 1 {
        Ok(res[0]
            .as_object()
            .map(Result::Ok)
            .unwrap_or_else(|| {
                bail!(
                    "{} field {} expected object: {}",
                    object_api_name,
                    path,
                    res[0]
                )
            })?
            .into_iter()
            .map(|(k, v)| json_as_string(v).map(|s| (k.clone(), s)))
            .collect::<OrError<Vec<(String, String)>>>()?
            .into_iter()
            .collect())
    } else {
        bail!("{} missing or duplicate field {}", object_api_name, path)
    }
}

pub fn select_as_conditions(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> HashMap<String, ConditionStatus> {
    select_project_as_map(json, object_api_name, path, "type", "status")
        .into_iter()
        .map(|(k, v)| {
            let v = match v.as_str() {
                "True" => ConditionStatus::True,
                "False" => ConditionStatus::False,
                _ => ConditionStatus::Unknown,
            };
            (k, v)
        })
        .collect()
}

pub fn select_as_bool(
    json: &Json,
    object_api_name: &str,
    path: &str,
) -> OrError<Option<bool>> {
    let res = jsonpath_lib::select(json, path)?;
    if res.len() == 1 {
        match res[0].as_bool() {
            None => bail!(
                "{} field {} cannot parse as bool: {}",
                object_api_name,
                path,
                res[0]
            ),
            Some(b) => Ok(Some(b)),
        }
    } else {
        Ok(None)
    }
}
