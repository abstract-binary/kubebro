use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Ingress {
    pub name: KName,
    pub namespace: KNamespace,
    pub hosts: Vec<String>,
    pub addresses: Vec<String>,
}

impl TryFrom<&Json> for Ingress {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Ingress", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "Ingress", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let hosts = select_as_collect_str(json, "Ingress", "$.spec.rules[*].host");
        let addresses = select_as_collect_str(
            json,
            "Ingress",
            "$.status.loadBalancer.ingress.[*].ip",
        );
        Ok(Self {
            name,
            namespace,
            hosts,
            addresses,
        })
    }
}

impl ResourceData for Ingress {
    type ListResponse = cc::ListIngressesResponse;

    const INDEX_HEADER: &'static str = "Ingress";
    const NON_INDEX_HEADERS: &'static [&'static str] = &["Hosts", "Addresses"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(&self.hosts, &self.addresses)
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(["R"], "Reload", "Reload the selected ingress")]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "R" => resource_table::prompt_and_act(
                self.name.clone(),
                Some(self.namespace.clone()),
                "ingress",
                "reload",
                cc::ReloadIngress::oneoff(
                    self.name.clone(),
                    self.namespace.clone(),
                    context,
                ),
                cache,
            ),
            _ => WR::NotHandled,
        }
    }
}

impl HealthCheck for Ingress {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<Ingress> {}
