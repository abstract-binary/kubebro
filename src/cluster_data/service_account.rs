use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

#[derive(Clone, Debug)]
pub struct ServiceAccount {
    pub name: KName,
    pub namespace: KNamespace,
}

impl TryFrom<&Json> for ServiceAccount {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "ServiceAccount", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "ServiceAccount", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        Ok(Self { name, namespace })
    }
}

impl ResourceData for ServiceAccount {
    type ListResponse = cc::ListServiceAccountsResponse;

    const INDEX_HEADER: &'static str = "Service Account";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!()
    }
}

impl HealthCheck for ServiceAccount {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<ServiceAccount> {}
