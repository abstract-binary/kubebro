use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::condition_status::ConditionStatus;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::collections::HashMap;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Deployment {
    pub name: KName,
    pub namespace: KNamespace,
    pub replicas: u64,
    pub ready_replicas: u64,
    pub updated_replicas: u64,
    pub available_replicas: u64,
    pub conditions: HashMap<String, ConditionStatus>,
}

impl TryFrom<&Json> for Deployment {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Deployment", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "Deployment", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let replicas = select_as_u64(json, "Deployment", "$.status.replicas")?;
        let ready_replicas = select_as_u64(json, "Deployment", "$.status.readyReplicas")?;
        let updated_replicas =
            select_as_u64(json, "Deployment", "$.status.updatedReplicas")?;
        let available_replicas =
            select_as_u64(json, "Deployment", "$.status.availableReplicas")?;
        let conditions =
            select_as_conditions(json, "Deployment", "$.status.conditions.[*]");
        Ok(Self {
            name,
            namespace,
            replicas,
            ready_replicas,
            updated_replicas,
            available_replicas,
            conditions,
        })
    }
}

impl HealthCheck for Deployment {
    fn healthy(&self) -> HealthResult {
        use ConditionStatus::*;
        use HealthResult as HR;
        match self.conditions.get("Available") {
            None | Some(Unknown) | Some(False) => {
                HR::Unhealthy("not available".to_string())
            }
            Some(True) => HR::Healthy,
        }
    }
}

impl ClusterOverviewChild for ResourceTable<Deployment> {}

impl ResourceData for Deployment {
    type ListResponse = cc::ListDeploymentsResponse;

    const INDEX_HEADER: &'static str = "Deployment";
    const NON_INDEX_HEADERS: &'static [&'static str] =
        &["Ready", "Up-to-date", "Available", "HealthCheck"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(
            (self.ready_replicas, self.replicas),
            self.updated_replicas,
            self.available_replicas,
            self.healthy()
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(
            ["R"],
            "Restart",
            "Restart the selected deployment"
        )]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "R" => resource_table::prompt_and_act(
                self.name.clone(),
                Some(self.namespace.clone()),
                "deployment",
                "restart",
                cc::RestartDeployment::oneoff(
                    self.name.clone(),
                    self.namespace.clone(),
                    context,
                ),
                cache,
            ),
            _ => WR::NotHandled,
        }
    }
}
