use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct PersistentVolume {
    pub name: KName,
    pub storage_capacity: String,
    pub storage_class_name: Option<String>,
    pub access_modes: Vec<String>,
    /// https://kubernetes.io/docs/concepts/storage/persistent-volumes#phase
    pub phase: String,
    pub error_reason: Option<String>,
    pub claim_ref: Option<String>,
}

impl TryFrom<&Json> for PersistentVolume {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "PersistentVolume", "$.metadata.name")?.into();
        let storage_capacity =
            select_as_str(json, "PersistentVolume", "$.spec.capacity.storage")?;
        let storage_class_name =
            select_as_str_opt(json, "PersistentVolume", "$.spec.storageClassName")?;
        let access_modes =
            select_as_collect_str(json, "PersistentVolume", "$.spec.accessModes[*]");
        let phase = select_as_str(json, "PersistentVolume", "$.status.phase")?;
        let error_reason =
            select_as_str_opt(json, "PersistentVolume", "$.status.reason")?;
        let claim_ref =
            select_as_str_opt(json, "PersistentVolume", "$.spec.claimRef.name")?;
        Ok(Self {
            name,
            storage_capacity,
            storage_class_name,
            access_modes,
            phase,
            error_reason,
            claim_ref,
        })
    }
}

impl ResourceData for PersistentVolume {
    type ListResponse = cc::ListPersistentVolumesResponse;

    const INDEX_HEADER: &'static str = "Persistent Volume";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[
        "Capacity",
        "Storage Class",
        "AccessModes",
        "Phase",
        "Healthy",
        "Claim Ref",
    ];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(
            &self.storage_capacity,
            self.storage_class_name.as_ref(),
            &self.access_modes,
            &self.phase,
            self.healthy(),
            self.claim_ref.as_ref()
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(
            ["D"],
            "Delete",
            "Delete the selected persistent volume"
        )]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "D" => {
                if self.phase == "Bound" {
                    error!("Will not delete a bound PV. Unbind it first.");
                    WR::Handled
                } else {
                    resource_table::prompt_and_delete(
                        self.name.clone(),
                        None,
                        "persistent volume",
                        cc::DeletePersistentVolume::oneoff(self.name.clone(), context),
                        cache,
                    )
                }
            }
            _ => WR::NotHandled,
        }
    }
}

impl HealthCheck for PersistentVolume {
    fn healthy(&self) -> HealthResult {
        if self.phase == "Failed" {
            HealthResult::Unhealthy(
                self.error_reason
                    .as_ref()
                    .cloned()
                    .unwrap_or_else(|| "failed".to_string()),
            )
        } else {
            HealthResult::Healthy
        }
    }
}

impl ClusterOverviewChild for ResourceTable<PersistentVolume> {}
