use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct DaemonSet {
    pub name: KName,
    pub namespace: KNamespace,
    pub desired_scheduled: u64,
    pub current_scheduled: u64,
    pub ready_replicas: u64,
    pub available_replicas: u64,
    pub updated_replicas: u64,
}

impl TryFrom<&Json> for DaemonSet {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "DaemonSet", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "DaemonSet", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let desired_scheduled =
            select_as_u64(json, "DaemonSet", "$.status.desiredNumberScheduled")?;
        let current_scheduled =
            select_as_u64(json, "DaemonSet", "$.status.currentNumberScheduled")?;
        let ready_replicas = select_as_u64(json, "DaemonSet", "$.status.numberReady")?;
        let available_replicas =
            select_as_u64(json, "DaemonSet", "$.status.numberAvailable")?;
        let updated_replicas =
            select_as_u64(json, "DaemonSet", "$.status.updatedNumberScheduled")?;
        Ok(Self {
            name,
            namespace,
            desired_scheduled,
            current_scheduled,
            ready_replicas,
            available_replicas,
            updated_replicas,
        })
    }
}

impl HealthCheck for DaemonSet {
    fn healthy(&self) -> HealthResult {
        use HealthResult as HR;
        if self.current_scheduled < self.desired_scheduled {
            HR::Unhealthy(format!(
                "under replicated: {}/{}",
                self.current_scheduled, self.desired_scheduled
            ))
        } else if self.updated_replicas < self.desired_scheduled {
            HR::Unhealthy(format!(
                "out of date: {}/{}",
                self.updated_replicas, self.desired_scheduled
            ))
        } else if self.available_replicas < self.desired_scheduled {
            HR::Unhealthy(format!(
                "not available: {}/{}",
                self.available_replicas, self.desired_scheduled
            ))
        } else if self.ready_replicas < self.desired_scheduled {
            HR::Unhealthy(format!(
                "not ready: {}/{}",
                self.ready_replicas, self.desired_scheduled
            ))
        } else {
            HR::Healthy
        }
    }
}

impl ClusterOverviewChild for ResourceTable<DaemonSet> {}

impl ResourceData for DaemonSet {
    type ListResponse = cc::ListDaemonSetsResponse;

    const INDEX_HEADER: &'static str = "DaemonSet";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[
        "Scheduled",
        "Ready",
        "Up-to-date",
        "Available",
        "HealthCheck",
    ];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(
            (self.current_scheduled, self.desired_scheduled),
            self.ready_replicas,
            self.updated_replicas,
            self.available_replicas,
            self.healthy()
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(
            ["R"],
            "Restart",
            "Restart the selected DaemonSet"
        )]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "R" => resource_table::prompt_and_act(
                self.name.clone(),
                Some(self.namespace.clone()),
                "daemon set",
                "restart",
                cc::RestartDaemonSet::oneoff(
                    self.name.clone(),
                    self.namespace.clone(),
                    context,
                ),
                cache,
            ),
            _ => WR::NotHandled,
        }
    }
}
