use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::condition_status::ConditionStatus;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use chrono::{DateTime, FixedOffset};
use std::collections::{HashMap, HashSet};
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Pod {
    pub name: KName,
    pub namespace: KNamespace,
    pub status: String,
    pub node_name: Option<String>,
    pub pod_ip: Option<String>,
    pub container_ports: Vec<(String, String)>,
    pub conditions: HashMap<String, ConditionStatus>,
    pub created: DateTime<FixedOffset>,
    pub containers: HashSet<KContainer>,
    pub init_containers: HashSet<KContainer>,
}

impl TryFrom<&Json> for Pod {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Pod", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "Pod", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let status = select_as_str(json, "Pod", "$.status.phase")
            .unwrap_or_else(|_| "Unknown".into());
        let node_name = select_as_str_opt(json, "Pod", "$.spec.nodeName")?;
        let pod_ip = select_as_str_opt(json, "Pod", "$.status.podIP")?;
        let mut container_ports: Vec<(String, String)> = select_as_pairs(
            json,
            "Pod",
            "$.spec.containers[*].ports[*]",
            ("name", "containerPort"),
        );
        container_ports.dedup();
        let conditions = select_as_conditions(json, "Pod", "$.status.conditions[*]");
        let created = DateTime::parse_from_rfc3339(&select_as_str(
            json,
            "Pod",
            "$.metadata.creationTimestamp",
        )?)?;
        let containers = select_as_collect_str(json, "Pod", "$.spec.containers.[*].name");
        let init_containers =
            select_as_collect_str(json, "Pod", "$.spec.initContainers.[*].name");
        Ok(Self {
            name,
            namespace,
            status,
            node_name,
            pod_ip,
            container_ports,
            conditions,
            created,
            containers,
            init_containers,
        })
    }
}

impl HealthCheck for Pod {
    fn healthy(&self) -> HealthResult {
        use HealthResult as HR;
        #[allow(clippy::wildcard_in_or_patterns)]
        match self.status.as_str() {
            "Succeeded" => HR::Healthy,
            "Pending" | "Running" => {
                use ConditionStatus::*;
                match self.conditions.get("Ready") {
                    Some(True) => HR::Healthy,
                    None | Some(False) | Some(Unknown) => {
                        HR::Unhealthy("Not ready".into())
                    }
                }
            }
            "Failed" | "Unknown" | _ => HR::Unhealthy(self.status.clone()),
        }
    }
}

impl ClusterOverviewChild for ResourceTable<Pod> {}

impl ResourceData for Pod {
    type ListResponse = cc::ListPodsResponse;

    const INDEX_HEADER: &'static str = "Pod";
    const NON_INDEX_HEADERS: &'static [&'static str] =
        &["Status", "Ready", "Created", "Node", "Address", "Ports"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        use crate::cell;
        #[allow(clippy::wildcard_in_or_patterns)]
        let status_color = match self.status.as_str() {
            "Pending" => palette::CELL_QUESTIONABLE_FG,
            "Running" => palette::CELL_HEALTHY_FG,
            "Succeeded" => palette::CELL_HEALTHY_FG,
            "Failed" => palette::CELL_UNHEALTHY_FG,
            "Unknown" | _ => palette::CELL_QUESTIONABLE_FG,
        };
        let container_ports: Vec<String> = self
            .container_ports
            .iter()
            .map(|(name, port)| {
                if name.is_empty() {
                    port.clone()
                } else {
                    format!("{}:{}", name, port)
                }
            })
            .collect();
        row!(
            (self.status.as_str(), status_color),
            self.conditions.get("Ready"),
            cell::TimeAgo(self.created),
            self.node_name.as_ref(),
            self.pod_ip.as_ref(),
            &container_ports
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![
            key_bind!(["D"], "Delete", "Delete the selected pod"),
            key_bind!(["L"], "Pod logs", "Show all logs for this pod"),
        ]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::path::AllLogsPath;
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "D" => resource_table::prompt_and_delete(
                self.name.clone(),
                Some(self.namespace.clone()),
                "pod",
                cc::DeletePod::oneoff(self.name.clone(), self.namespace.clone(), context),
                cache,
            ),
            "L" => WR::Goto(Arc::new(Box::new(AllLogsPath::new(
                &self.name,
                &self.namespace,
                &context,
            )))),
            _ => WR::NotHandled,
        }
    }
}
