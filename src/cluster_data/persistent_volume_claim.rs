use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

use chrono::{DateTime, FixedOffset};

#[derive(Clone, Debug)]
pub struct PersistentVolumeClaim {
    pub name: KName,
    pub namespace: KNamespace,
    pub created: DateTime<FixedOffset>,
    pub status: String,
    pub storage_class: String,
    pub access_modes: Vec<String>,
    pub capacity: Option<String>,
}

impl TryFrom<&Json> for PersistentVolumeClaim {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name =
            select_as_str(json, "PersistentVolumeClaim", "$.metadata.name")?.into();
        let namespace =
            select_as_str(json, "PersistentVolumeClaim", "$.metadata.namespace")
                .unwrap_or_else(|_| "default".into())
                .into();
        let created = DateTime::parse_from_rfc3339(&select_as_str(
            json,
            "PersistentVolumeClaim",
            "$.metadata.creationTimestamp",
        )?)?;
        let status = select_as_str(json, "PersistentVolumeClaim", "$.status.phase")
            .unwrap_or_else(|_| "Unknown".into());
        let storage_class =
            select_as_str(json, "PersistentVolumeClaim", "$.spec.storageClassName")
                .unwrap_or_else(|_| "Unknown".into());
        let access_modes = {
            select_as_collect_str(
                json,
                "PersistentVolumeClaim",
                "$.status.accessModes[*]",
            )
        };
        let capacity =
            select_as_str(json, "PersistentVolumeClaim", "$.status.capacity.storage")
                .ok();
        Ok(Self {
            name,
            namespace,
            created,
            status,
            storage_class,
            access_modes,
            capacity,
        })
    }
}

impl ResourceData for PersistentVolumeClaim {
    type ListResponse = cc::ListPersistentVolumeClaimsResponse;

    const INDEX_HEADER: &'static str = "PersistentVolumeClaim";
    const NON_INDEX_HEADERS: &'static [&'static str] = &[
        "Status",
        "Access Modes",
        "Capacity",
        "Storage Class",
        "Created",
    ];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(
            &self.status,
            self.access_modes.join(", "),
            self.capacity.as_ref(),
            &self.storage_class,
            self.created.naive_local().to_string()
        )
    }
}

impl HealthCheck for PersistentVolumeClaim {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<PersistentVolumeClaim> {}
