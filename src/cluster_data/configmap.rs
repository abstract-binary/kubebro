use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

use chrono::{DateTime, FixedOffset};
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct ConfigMap {
    pub name: KName,
    pub namespace: KNamespace,
    pub created: DateTime<FixedOffset>,
    pub data: HashMap<String, String>,
}

impl TryFrom<&Json> for ConfigMap {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "ConfigMap", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "ConfigMap", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let created = DateTime::parse_from_rfc3339(&select_as_str(
            json,
            "ConfigMap",
            "$.metadata.creationTimestamp",
        )?)?;
        let data =
            select_as_map(json, "ConfigMap", "$.data").unwrap_or_else(|_| HashMap::new());
        Ok(Self {
            name,
            namespace,
            created,
            data,
        })
    }
}

impl ResourceData for ConfigMap {
    type ListResponse = cc::ListConfigMapsResponse;

    const INDEX_HEADER: &'static str = "ConfigMap";
    const NON_INDEX_HEADERS: &'static [&'static str] = &["Created", "Data"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        use crate::cell;
        row!(cell::TimeAgo(self.created), cell::DataTable(&self.data))
    }
}

impl HealthCheck for ConfigMap {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<ConfigMap> {}
