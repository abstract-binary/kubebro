use super::json_manip::*;
use crate::cache::Cache;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;
use crate::window_result::WindowResult;

use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct StatefulSet {
    pub name: KName,
    pub namespace: KNamespace,
    pub replicas: u64,
    pub ready_replicas: u64,
    pub updated_replicas: u64,
}

impl TryFrom<&Json> for StatefulSet {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "StatefulSet", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "StatefulSet", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let replicas = select_as_u64(json, "StatefulSet", "$.status.replicas")?;
        let ready_replicas =
            select_as_u64(json, "StatefulSet", "$.status.readyReplicas")?;
        let updated_replicas =
            select_as_u64(json, "StatefulSet", "$.status.updatedReplicas")?;
        Ok(Self {
            name,
            namespace,
            replicas,
            ready_replicas,
            updated_replicas,
        })
    }
}

impl HealthCheck for StatefulSet {
    fn healthy(&self) -> HealthResult {
        use HealthResult as HR;
        if self.ready_replicas < self.replicas {
            HR::Unhealthy(format!(
                "Not ready ({}<{})",
                self.ready_replicas, self.replicas
            ))
        } else if self.updated_replicas < self.replicas {
            HR::Unhealthy(format!(
                "Out of date ({}<{})",
                self.updated_replicas, self.replicas
            ))
        } else {
            HR::Healthy
        }
    }
}

impl ClusterOverviewChild for ResourceTable<StatefulSet> {}

impl ResourceData for StatefulSet {
    type ListResponse = cc::ListStatefulSetsResponse;

    const INDEX_HEADER: &'static str = "Stateful Set";
    const NON_INDEX_HEADERS: &'static [&'static str] =
        &["Ready", "Up-to-date", "HealthCheck"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        row!(
            (self.ready_replicas, self.replicas),
            self.updated_replicas,
            self.healthy()
        )
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        vec![key_bind!(
            ["R"],
            "Restart",
            "Restart the selected stateful set"
        )]
    }

    fn handle_key_event(
        &self,
        key: Key,
        context: KContext,
        cache: Arc<Cache>,
    ) -> WindowResult {
        use crate::resource_table;
        use WindowResult as WR;
        match key.to_string().as_str() {
            "R" => resource_table::prompt_and_act(
                self.name.clone(),
                Some(self.namespace.clone()),
                "stateful set",
                "restart",
                cc::RestartStatefulSet::oneoff(
                    self.name.clone(),
                    self.namespace.clone(),
                    context,
                ),
                cache,
            ),
            _ => WR::NotHandled,
        }
    }
}
