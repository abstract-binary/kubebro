use super::json_manip::*;
use crate::cell::Cell;
use crate::cluster_command as cc;
use crate::cluster_overview_table::ClusterOverviewChild;
use crate::health_check::{HealthCheck, HealthResult};
use crate::import::*;
use crate::resource_table::{ResourceData, ResourceTable};
use crate::table::Index;

#[derive(Clone, Debug)]
pub struct Service {
    pub name: KName,
    pub namespace: KNamespace,
    pub cluster_ip: Option<String>,
    pub ports: Vec<(String, String)>,
    pub selector: Option<String>,
}

impl TryFrom<&Json> for Service {
    type Error = anyhow::Error;

    fn try_from(json: &Json) -> OrError<Self> {
        let name = select_as_str(json, "Service", "$.metadata.name")?.into();
        let namespace = select_as_str(json, "Service", "$.metadata.namespace")
            .unwrap_or_else(|_| "default".into())
            .into();
        let cluster_ip = select_as_str_opt(json, "Service", "$.spec.clusterIP")?;
        let ports =
            select_as_pairs(json, "Service", "$.spec.ports[*]", ("port", "targetPort"));
        let selector = select_as_selector(json, "Service", "$.spec.selector");
        Ok(Self {
            name,
            namespace,
            cluster_ip,
            ports,
            selector,
        })
    }
}

impl ResourceData for Service {
    type ListResponse = cc::ListServicesResponse;

    const INDEX_HEADER: &'static str = "Service";
    const NON_INDEX_HEADERS: &'static [&'static str] =
        &["ClusterIP", "Ports", "Selector"];

    fn index(&self) -> Index {
        (*self.name).into()
    }

    fn to_row(&self) -> OrError<Vec<Cell>> {
        let ports: Vec<String> = self
            .ports
            .iter()
            .map(|(port, target_port)| {
                if port == target_port {
                    port.clone()
                } else {
                    format!("{}->{}", port, target_port)
                }
            })
            .collect();
        row!(self.cluster_ip.as_ref(), &ports, self.selector.as_ref())
    }
}

impl HealthCheck for Service {
    fn healthy(&self) -> HealthResult {
        HealthResult::Unknown
    }
}

impl ClusterOverviewChild for ResourceTable<Service> {}
