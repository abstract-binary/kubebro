#[allow(unused_imports)]
use crate::import::*;

use std::fmt;
use std::iter::FromIterator;

pub trait HealthCheck {
    fn healthy(&self) -> HealthResult;
}

#[derive(Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum HealthResult {
    Unknown,
    Healthy,
    Unhealthy(String),
}

use HealthResult::*;

impl HealthResult {
    pub fn map<F>(self, f: F) -> Self
    where
        F: Fn(String) -> String,
    {
        match self {
            Unknown => Unknown,
            Healthy => Healthy,
            Unhealthy(error) => Unhealthy(f(error)),
        }
    }
}

impl fmt::Display for HealthResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Unknown => Ok(()),
            Healthy => write!(f, "Ok"),
            Unhealthy(error) => write!(f, "{}", error),
        }
    }
}

impl FromIterator<HealthResult> for HealthResult {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = HealthResult>,
    {
        let mut healthy = 0;
        let unhealthy: Vec<_> = iter
            .into_iter()
            .filter_map(|hr| match hr {
                Unknown => None,
                Healthy => {
                    healthy += 1;
                    None
                }
                Unhealthy(error) => Some(error),
            })
            .collect();
        if unhealthy.is_empty() {
            if healthy == 0 {
                Unknown
            } else {
                Healthy
            }
        } else {
            Unhealthy(unhealthy.join(", "))
        }
    }
}
