use crate::import::*;
use crate::key::Key;
use crate::key_bind::{KeyBind, KeyBindDesc};
use crate::window::Window;
use crate::window_result::{WindowResult as WR, WindowResult};

use tui::{buffer::Buffer, layout::Rect};

#[derive(Debug)]
pub struct Dialog {
    title: String,
    text: String,
    options: Vec<KeyBind<()>>,
}

impl Dialog {
    pub fn new(title: &str, text: &str, options: Vec<KeyBind<()>>) -> Self {
        Self {
            title: title.to_string(),
            text: text.to_string(),
            options,
        }
    }
}

impl Window for Dialog {
    fn tick(&mut self) {}

    fn render(&mut self, buffer: &mut Buffer, _tick: u64, rect: Rect) {
        use tui::layout::{Alignment, Constraint::*, Layout};
        use tui::style::Style;
        use tui::text::{Span, Spans};
        use tui::widgets::*;
        let vrect;
        match crate::layout::center_vertically(5, rect) {
            Ok(r) => vrect = r,
            Err(error) => {
                error!("Failed to render dialog: {}", error);
                return;
            }
        }
        let rect = crate::layout::center_horizontally(4 + self.text.len() as u16, vrect);
        if rect.height < 5 {
            error!("Not enough space to render dialog: {:?}", rect);
            return;
        }
        let block = Block::default()
            .title(self.title.clone())
            .borders(Borders::ALL);
        Clear.render(rect, buffer);
        block.render(rect, buffer);
        let rects = {
            // - A :: top border
            // - B :: text
            // - C :: empty line
            // - D :: options
            // - E :: bottom border
            Layout::default()
                .constraints(
                    [
                        Length(1), /* A */
                        Min(1),    /* B */
                        Length(1), /* C */
                        Min(1),    /* D */
                        Length(1), /* E */
                    ]
                    .as_ref(),
                )
                .split(rect)
        };
        let text_line = Paragraph::new(self.text.clone()).alignment(Alignment::Center);
        text_line.render(rects[1], buffer);
        let options_line = Paragraph::new(Spans::from(
            self.options
                .iter()
                .flat_map(|kb| {
                    vec![
                        Span::raw(kb.keys.to_string()),
                        Span::styled(
                            kb.short_desc,
                            Style::default()
                                .bg(palette::OPTIONS_BG)
                                .fg(palette::OPTIONS_FG),
                        ),
                        Span::raw(" "),
                    ]
                })
                .collect::<Vec<Span>>(),
        ))
        .alignment(Alignment::Center);
        options_line.render(rects[3], buffer);
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        let key_binds = crate::key_bind::to_map(&self.options);
        match key_binds.get(&key) {
            None => {
                info!("Ignoring input {} (valid keys are {:?})", key, key_binds);
                WR::NotHandled
            }
            Some(kb) => (kb.action)(()),
        }
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        self.options.iter().map(KeyBind::desc).collect()
    }
}
