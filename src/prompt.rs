#[allow(unused_imports)]
use crate::import::*;

#[derive(Debug)]
pub struct Prompt {
    pub before_text: String,
    pub text: String,
}

impl Prompt {
    pub fn new(before_text: impl Into<String>) -> Self {
        Self {
            before_text: before_text.into(),
            text: "".into(),
        }
    }
}
