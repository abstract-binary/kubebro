use crate::import::*;

use anyhow::Context as _;
use std::collections::HashMap;
use std::fmt;
use std::panic::{RefUnwindSafe, UnwindSafe};
use std::sync::Arc;

type Yaml = serde_yaml::Value;

pub trait KubeConfig
where
    Self: fmt::Debug + UnwindSafe + RefUnwindSafe + Send + Sync,
{
    fn current_context(&self) -> Option<&KContext>;
    fn contexts(&self) -> HashMap<KContext, Context>;
}

pub struct Context<'a> {
    name: &'a KContext,
    cluster: &'a Cluster,
    user: &'a User,
}

impl<'a> Context<'a> {
    pub fn name(&self) -> &KContext {
        self.name
    }

    pub fn user(&self) -> &str {
        &self.user.name
    }

    pub fn cluster(&self) -> &str {
        &self.cluster.name
    }

    pub fn server(&self) -> &str {
        &self.cluster.server
    }

    pub fn cluster_certificate_authority_data(&self) -> Option<OrError<Arc<Vec<u8>>>> {
        self.cluster.certificate_authority.as_ref().map(|x| x.get())
    }

    #[allow(clippy::type_complexity)]
    pub fn client_certificate_and_key(
        &self,
    ) -> Option<OrError<(Arc<Vec<u8>>, Arc<Vec<u8>>)>> {
        match (&self.user.client_certificate, &self.user.client_key) {
            (None, _) | (_, None) => None,
            (Some(cert), Some(key)) => {
                let get = || Ok((cert.get()?, key.get()?));
                Some(get())
            }
        }
    }

    pub fn client_token(&self) -> Option<OrError<Arc<Vec<u8>>>> {
        self.user.token.as_ref().map(|x| x.get())
    }

    pub fn insecure_skip_tls_verify(&self) -> Option<bool> {
        self.cluster.insecure_skip_tls_verify
    }
}

/// In-memory representation of a kubeconfig (e.g. ~/.kube/config).
///
/// The Go type is available here:
///   https://github.com/kubernetes/client-go/blob/master/tools/clientcmd/api/types.go
///
#[derive(Debug)]
pub struct FileKubeConfig {
    current_context: Option<KContext>,
    contexts: HashMap<KContext, ContextRefs>,
    users: HashMap<String, User>,
    clusters: HashMap<String, Cluster>,
}

impl FileKubeConfig {
    pub fn from_file(filename: &str) -> OrError<Self> {
        let file = std::fs::File::open(filename)
            .with_context(|| format!("Cannot read kube config {}", filename))?;
        let yaml: Yaml = serde_yaml::from_reader(file)
            .with_context(|| format!("Kubeconfig {} is invalid YAML", filename))?;
        let current_context =
            yaml_get(&yaml, &["current-context"], Yaml::as_str).map(|x| x.into());
        let mut users = HashMap::new();
        if let Some(us) = yaml_get(&yaml, &["users"], Yaml::as_sequence) {
            for u in us {
                let get_user = || -> Option<User> {
                    let name = yaml_get_string(u, &["name"])?;
                    let client_key = DataRef::get_yaml(
                        u,
                        &["user", "client-key-data"],
                        &["user", "client-key"],
                    );
                    let client_certificate = DataRef::get_yaml(
                        u,
                        &["user", "client-certificate-data"],
                        &["user", "client-certificate"],
                    );
                    let token = {
                        let prog = yaml_get_string(
                            u,
                            &["user", "auth-provider", "config", "cmd-path"],
                        );
                        let args = yaml_get_string(
                            u,
                            &["user", "auth-provider", "config", "cmd-args"],
                        );
                        let path = yaml_get_string(
                            u,
                            &["user", "auth-provider", "config", "token-key"],
                        );
                        match (prog, args, path) {
                            (None, _, _) | (_, None, _) | (_, _, None) => None,
                            (Some(prog), Some(args), Some(path)) => {
                                let args: Vec<String> = args
                                    .split(' ')
                                    .filter_map(|str| {
                                        if str.is_empty() {
                                            None
                                        } else {
                                            Some(str.to_string())
                                        }
                                    })
                                    .collect();
                                path.strip_prefix('{')
                                    .and_then(|path| path.strip_suffix('}'))
                                    .map(|path| DataRef::Command {
                                        prog,
                                        args,
                                        path: format!("${}", path),
                                    })
                            }
                        }
                    };
                    Some(User {
                        name,
                        client_key,
                        client_certificate,
                        token,
                    })
                };
                match get_user() {
                    None => {
                        error!("User in KubeConfig missing fields: {:?}", u);
                    }
                    Some(user) => {
                        users.insert(user.name.clone(), user);
                    }
                }
            }
        }
        let mut contexts = HashMap::new();
        if let Some(cs) = yaml_get(&yaml, &["contexts"], Yaml::as_sequence) {
            for c in cs {
                let get_context = || -> Option<ContextRefs> {
                    let name = yaml_get_string(c, &["name"])?.into();
                    let user = yaml_get_string(c, &["context", "user"])?;
                    let cluster = yaml_get_string(c, &["context", "cluster"])?;
                    Some(ContextRefs {
                        name,
                        cluster,
                        user,
                    })
                };
                match get_context() {
                    None => {
                        error!("Context in KubeConfig missing fields: {:?}", c);
                    }
                    Some(context) => {
                        contexts.insert(context.name.clone(), context);
                    }
                }
            }
        }
        let mut clusters = HashMap::new();
        if let Some(cs) = yaml_get(&yaml, &["clusters"], Yaml::as_sequence) {
            for c in cs {
                let get_cluster = || -> Option<Cluster> {
                    let name = yaml_get_string(c, &["name"])?;
                    let certificate_authority = DataRef::get_yaml(
                        c,
                        &["cluster", "certificate-authority-data"],
                        &["cluster", "certificate-authority"],
                    );
                    let insecure_skip_tls_verify = yaml_get(
                        c,
                        &["cluster", "insecure-skip-tls-verify"],
                        Yaml::as_bool,
                    );
                    let server = yaml_get_string(c, &["cluster", "server"])?;
                    Some(Cluster {
                        name,
                        server,
                        certificate_authority,
                        insecure_skip_tls_verify,
                    })
                };
                match get_cluster() {
                    None => {
                        error!("Cluster in KubeConfig missing fields: {:?}", c);
                    }
                    Some(cluster) => {
                        clusters.insert(cluster.name.clone(), cluster);
                    }
                }
            }
        }
        Ok(Self {
            current_context,
            contexts,
            users,
            clusters,
        })
    }

    pub fn new_for_test(port: u16) -> Self {
        let mut contexts = HashMap::new();
        let mut clusters = HashMap::new();
        let mut users = HashMap::new();
        let test_context = KContext::from("test-context");
        let test_user = "test-user".to_string();
        let test_cluster = "test-cluster".to_string();
        contexts.insert(
            test_context.clone(),
            ContextRefs {
                name: test_context,
                user: test_user.clone(),
                cluster: test_cluster.clone(),
            },
        );
        clusters.insert(
            test_cluster.clone(),
            Cluster {
                name: test_cluster,
                server: format!("http://localhost:{}", port),
                certificate_authority: None,
                insecure_skip_tls_verify: Some(true),
            },
        );
        users.insert(
            test_user.clone(),
            User {
                name: test_user,
                client_key: None,
                client_certificate: None,
                token: None,
            },
        );
        Self {
            current_context: Some("test-context".into()),
            contexts,
            users,
            clusters,
        }
    }
}

impl KubeConfig for FileKubeConfig {
    fn current_context(&self) -> Option<&KContext> {
        self.current_context.as_ref()
    }

    fn contexts(&self) -> HashMap<KContext, Context> {
        self.contexts
            .iter()
            .filter_map(|(context_name, context)| {
                match (
                    self.clusters.get(&context.cluster),
                    self.users.get(&context.user),
                ) {
                    (None, _) | (_, None) => {
                        info!("Missing cluster or user for context {}", &context.name);
                        None
                    }
                    (Some(cluster), Some(user)) => Some((
                        context_name.clone(),
                        Context {
                            name: context_name,
                            cluster,
                            user,
                        },
                    )),
                }
            })
            .collect()
    }
}

#[derive(Debug)]
pub struct ContextRefs {
    pub name: KContext,
    pub cluster: String,
    pub user: String,
}

#[derive(Debug)]
struct Cluster {
    name: String,
    server: String,
    certificate_authority: Option<DataRef>,
    insecure_skip_tls_verify: Option<bool>,
}

#[derive(Debug)]
struct User {
    name: String,
    client_key: Option<DataRef>,
    client_certificate: Option<DataRef>,
    token: Option<DataRef>,
}

#[derive(Debug)]
enum DataRef {
    Data(Arc<Vec<u8>>),
    File(String),
    Command {
        prog: String,
        args: Vec<String>,
        path: String,
    },
}

impl DataRef {
    fn get_yaml(yaml: &Yaml, data_keys: &[&str], file_keys: &[&str]) -> Option<DataRef> {
        match yaml_get_string(yaml, data_keys) {
            Some(data) => Some(DataRef::Data(Arc::new(base64::decode(data).ok()?))),
            None => Some(DataRef::File(yaml_get_string(yaml, file_keys)?)),
        }
    }

    fn get(&self) -> OrError<Arc<Vec<u8>>> {
        match self {
            DataRef::Data(ref data) => Ok(Arc::clone(data)),
            DataRef::File(ref filename) => Ok(Arc::new(
                std::fs::read_to_string(filename)
                    .with_context(|| format!("Cannot read certificate {}", filename))?
                    .into(),
            )),
            DataRef::Command { prog, args, path } => {
                use std::process::Command;
                let output = Command::new(prog).args(args).output()?;
                if !output.status.success() {
                    bail!(
                        "Error executing: {} {:?}; Stderr: {}",
                        prog,
                        args,
                        String::from_utf8_lossy(&output.stderr)
                    );
                }
                let json: Json = serde_json::from_slice(&output.stdout)?;
                json_select_as_str(&json, path)?
                    .ok_or_else(|| {
                        anyhow!(
                            "Did not find path {} in config: {}",
                            path,
                            String::from_utf8_lossy(&output.stdout)
                        )
                    })
                    .map(|value| Arc::new(value.into_bytes()))
            }
        }
    }
}

fn yaml_str(str: &str) -> Yaml {
    Yaml::String(str.into())
}

fn yaml_get<'a, F, U>(mut yaml: &'a Yaml, keys: &[&str], convert: F) -> Option<U>
where
    F: Fn(&'a Yaml) -> Option<U>,
    U: 'a,
{
    for key in keys {
        yaml = yaml.as_mapping()?.get(&yaml_str(key))?;
    }
    convert(yaml)
}

fn yaml_get_string(yaml: &Yaml, keys: &[&str]) -> Option<String> {
    yaml_get(yaml, keys, Yaml::as_str).map(str::to_string)
}

pub fn json_select_as_str(json: &Json, path: &str) -> OrError<Option<String>> {
    let res = jsonpath_lib::select(json, path)?;
    if res.is_empty() {
        Ok(None)
    } else if res.len() == 1 {
        match res[0].as_str() {
            Some(str) => Ok(Some(str.to_string())),
            None => bail!(
                "path {} in json did not contain a string: {:?}",
                path,
                res[0]
            ),
        }
    } else {
        bail!(
            "path {} in json contained multiple results: {:?}",
            path,
            res
        );
    }
}
