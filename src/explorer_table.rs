use crate::cell::Cell;
use crate::explorer_view::ExplorerView;
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::prompt::Prompt;
use crate::table::{Index, Table, TableContext};
use crate::unique::Unique;
use crate::window_result::WindowResult;

use regex::Regex;
use std::sync::Arc;
use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect, widgets::StatefulWidget};

// The numbers below are i64 for ease of coding.  Semantically, they
// should be usize of u16, but we frequently have to subtract them in
// this module, and it's annoying to have to cast back and forth
// between i64s and the smaller unsigned values.
#[derive(Debug)]
pub struct ExplorerTable {
    table: Box<dyn Table>,
    selected: Option<i64>,
    cached_id: Unique,
    last_updated: LastUpdated,
    offset_rows: i64,
    /// The `offset_columns` field has weird semantics.  The first
    /// column is always shown, and `offset_columns` determines which
    /// is the **second** column to be shown.  So, a value of 0 means
    /// that the second shown column is the second real column, and a
    /// value of 1 means that the second shown column is the third
    /// real column.
    offset_columns: usize,
    cached_rows: Vec<(Index, OrError<Vec<Cell>>)>,
    cached_rows_id: Unique,
    window_height: i64,
    prompt: Option<Prompt>,
    active_search: Option<Regex>,
}

impl ExplorerTable {
    pub fn new(table: Box<dyn Table>, refresh_rate: Duration) -> Self {
        let cached_rows = table.rows();
        let cached_id = Unique::new();
        let mut t = Self {
            table,
            selected: None,
            cached_id,
            last_updated: LU::FetchingData,
            offset_rows: 0,
            offset_columns: 0,
            cached_rows,
            cached_rows_id: cached_id,
            window_height: 0,
            prompt: None,
            active_search: None,
        };
        let _: OrError<()> = t.self_update(refresh_rate);
        t
    }

    pub fn new_view(
        table: Box<dyn Table>,
        refresh_rate: Duration,
    ) -> Box<dyn ExplorerView> {
        Box::new(Self::new(table, refresh_rate))
    }

    fn get_selected(&self) -> Option<Index> {
        self.selected.and_then(|x| {
            if 0 <= x {
                self.cached_rows.get(x as usize).map(|(idx, _)| idx.clone())
            } else {
                None
            }
        })
    }

    fn search_next(&mut self) {
        if let Some(active_search) = self.active_search.as_ref() {
            let start_from = self.selected.map(|x| x + 1).unwrap_or(self.offset_rows);
            self.selected = {
                match find_next(&self.cached_rows, start_from, active_search) {
                    Some(x) => Some(x),
                    None => find_next(&self.cached_rows, 0, active_search),
                }
            };
        }
    }

    fn search_previous(&mut self) {
        if let Some(active_search) = self.active_search.as_ref() {
            let num_rows = self.cached_rows.len() as i64;
            let start_from = if let Some(selected) = self.selected {
                std::cmp::max(selected - 1, 0)
            } else {
                std::cmp::min(self.offset_rows + self.window_height - 1, num_rows - 1)
            };
            self.selected = {
                match find_prev(&self.cached_rows, start_from, active_search) {
                    None => find_prev(&self.cached_rows, num_rows - 1, active_search),
                    Some(x) => Some(x),
                }
            };
        }
    }
}

impl ExplorerView for ExplorerTable {
    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()> {
        self.table
            .self_update(refresh_rate, &mut self.cached_id, &mut self.last_updated)
    }

    fn last_updated(&self) -> LastUpdated {
        self.last_updated
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        if self.prompt.is_some() {
            vec![
                key_bind!(["Esc"], "Exit search", "Exit search"),
                key_bind!(
                    ["Ret"],
                    "Search",
                    "Highlight matching lines and navigate between them"
                ),
            ]
        } else {
            let mut key_binds = vec![
                key_bind!(["/"], "Search", "Search the text"),
                key_bind_adv!(
                    ["h", "j", "k", "l", "Left", "Down", "Up", "Right"],
                    "Move",
                    "Move around the table"
                ),
                key_bind_adv!(
                    ["PageUp", "PageDown"],
                    "Move",
                    "Move around the table by screenfuls"
                ),
                key_bind_adv!(
                    ["g", "Home"],
                    "Move to top",
                    "Move to the top of the table"
                ),
                key_bind_adv!(
                    ["G", "End"],
                    "Move to bottom",
                    "Move to the bottom of the table"
                ),
                key_bind_adv!(["Bksp"], "Back", "Go back to the previous table"),
                key_bind!(
                    ["Ret"],
                    "Drill in",
                    "Drill in to the currently selected row"
                ),
                key_bind!(["Esc"], "Up", "Go to the parent table"),
            ];
            if self.active_search.is_some() {
                key_binds.push(key_bind!(["n"], "Next", "Go to the next search match"));
                key_binds.push(key_bind!(
                    ["N"],
                    "Prev",
                    "Go to the previous search match"
                ));
            }
            let context = TableContext {
                selected: self.get_selected(),
            };
            key_binds
                .into_iter()
                .chain(self.table.key_binds(context))
                .collect()
        }
    }

    fn prompt(&self) -> Option<&Prompt> {
        self.prompt.as_ref()
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        use std::cmp::min;
        use WindowResult as WR;
        if self.prompt.is_some() {
            match key.to_string().as_str() {
                "Esc" => {
                    self.prompt = None;
                    WR::Handled
                }
                "Bksp" => {
                    let _: Option<char> = self.prompt.as_mut().unwrap().text.pop();
                    WR::Handled
                }
                "Ret" => {
                    let search_text = self.prompt.take().unwrap().text;
                    if search_text.is_empty() {
                        self.active_search = None;
                    } else {
                        self.active_search = smart_regex::new(&search_text);
                        self.search_next();
                    }
                    WR::Handled
                }
                str => match str.chars().collect::<Vec<char>>().as_slice() {
                    [ch] => {
                        // Prompt is definitely `Some` in this branch.
                        self.prompt.as_mut().unwrap().text.push(*ch);
                        WR::Handled
                    }
                    _ => WR::NotHandled,
                },
            }
        } else {
            let context = TableContext {
                selected: self.get_selected(),
            };
            match self.table.handle_key_event(key, context) {
                WR::NotHandled => {
                    match key.to_string().as_ref() {
                        "Ret" => match self.get_selected() {
                            None => {
                                info!(
                                    "{}: No selected row to drill into",
                                    self.table.path()
                                );
                                WR::Handled
                            }
                            Some(index) => match self.table.path().extend(&index) {
                                Ok(new_path) => WR::Goto(Arc::new(new_path)),
                                Err(err) => {
                                    error!(
                                        "Error constructing drill-in path ({} + {}): {}",
                                        self.table.path(),
                                        index,
                                        err
                                    );
                                    WR::Handled
                                }
                            },
                        },
                        "Esc" | "Bksp" => WR::CloseSelf,
                        "Down" | "j" => {
                            self.selected = Some(match self.selected {
                                None => 0,
                                Some(selected) => {
                                    min(selected + 1, self.cached_rows.len() as i64 - 1)
                                }
                            });
                            WR::Handled
                        }
                        "Up" | "k" => {
                            self.selected = self.selected.and_then(|selected| {
                                if selected == 0 {
                                    None // Give users a way to deselect
                                } else {
                                    Some(selected - 1)
                                }
                            });
                            WR::Handled
                        }
                        "Left" | "h" => {
                            if self.offset_columns >= 1 {
                                self.offset_columns -= 1;
                            }
                            WR::Handled
                        }
                        "Right" | "l" => {
                            if self.offset_columns < self.table.non_index_headers().len()
                            {
                                self.offset_columns += 1;
                            }
                            WR::Handled
                        }
                        "g" | "Home" => {
                            self.selected = Some(0);
                            WR::Handled
                        }
                        "G" | "End" => {
                            self.selected = Some(self.cached_rows.len() as i64 - 1);
                            WR::Handled
                        }
                        "PageDown" => {
                            let new_offset = self.offset_rows + self.window_height;
                            let num_rows = self.cached_rows.len() as i64;
                            if new_offset < num_rows {
                                self.offset_rows = new_offset;
                                self.selected = Some(new_offset);
                            }
                            WR::Handled
                        }
                        "PageUp" => {
                            let new_offset = self.offset_rows - self.window_height;
                            if new_offset >= 0 {
                                self.offset_rows = new_offset;
                                self.selected = Some(new_offset);
                            }
                            WR::Handled
                        }
                        "/" => {
                            self.prompt = Some(Prompt::new("Find:"));
                            WR::Handled
                        }
                        "n" => {
                            self.search_next();
                            WR::Handled
                        }
                        "N" => {
                            self.search_previous();
                            WR::Handled
                        }
                        _ => WR::NotHandled,
                    }
                }
                res => res,
            }
        }
    }

    fn render(&mut self, buffer: &mut Buffer, rect: Rect) {
        use std::cmp::max;
        use tui::{
            layout::Constraint,
            style::{Modifier, Style},
            text::Text,
            widgets as W,
        };
        if rect.height < 2 {
            error!("Not enough space to render explorer table");
            return;
        }
        self.window_height = (rect.height as i64) - 1;
        if self.cached_rows_id != self.cached_id {
            let old_selected = self.get_selected();
            self.cached_rows_id = self.cached_id;
            self.cached_rows = self.table.rows();
            self.selected = old_selected.and_then(|old_selected| {
                self.cached_rows
                    .iter()
                    .position(|(idx, _)| *idx == old_selected)
                    .map(|x| x as i64)
            });
        }
        match self.selected {
            None => (),
            Some(selected) => {
                if selected < self.offset_rows {
                    // Move the screen up to see selected.
                    self.offset_rows = selected;
                } else if selected >= self.offset_rows + self.window_height {
                    self.offset_rows = selected - self.window_height + 1;
                    info!("Updated offset_rows to {}", self.offset_rows);
                }
            }
        }
        let overflow_style = Style::default()
            .bg(palette::OVERFLOW_CELL_BG)
            .fg(palette::OVERFLOW_CELL_TEXT_FG);
        // One for the index column, one for the overflow-left column
        // (which might not get shown), and then the non-index columns.
        let num_cols = 1
            + 1
            + max(
                0,
                self.table.non_index_headers().len() as i64 - self.offset_columns as i64,
            ) as usize;
        let mut column_widths: Vec<Constraint> = Vec::with_capacity(num_cols);
        let centerpiece = {
            let selected_style = Style::default().add_modifier(Modifier::REVERSED);
            let mut col_lengths = vec![0; num_cols];
            let header = {
                let cell_style = Style::default()
                    .fg(palette::TEXT_COLOR)
                    .add_modifier(Modifier::BOLD);
                let mut cells = vec![
                    W::Cell::from(self.table.index_header()).style(cell_style),
                    W::Cell::from("<").style(overflow_style),
                ];
                col_lengths[0] = self.table.index_header().len();
                col_lengths[1] = 1;
                let mut other_cells = self
                    .table
                    .non_index_headers()
                    .iter()
                    .skip(self.offset_columns)
                    .enumerate()
                    .map(|(idx, h)| {
                        col_lengths[idx + 2] = h.len();
                        W::Cell::from(*h).style(cell_style)
                    })
                    .collect();
                cells.append(&mut other_cells);
                cells
            };
            let rows: Vec<(Vec<W::Cell>, Style)> = self
                .cached_rows
                .iter()
                .enumerate()
                .skip(self.offset_rows as usize)
                .take(self.window_height as usize)
                .map(|(idx, (index, row))| match row {
                    Err(error) => {
                        // Force a block
                        (
                            vec![
                                W::Cell::from(index.to_string()),
                                W::Cell::from(format!("Error: {}", error)),
                            ],
                            Style::default().fg(palette::ERROR_TEXT_FG),
                        )
                    }
                    Ok(row) => {
                        let mut cells = vec![
                            W::Cell::from(Text::raw(index.to_string())),
                            W::Cell::from("<").style(overflow_style),
                        ];
                        col_lengths[0] = max(col_lengths[0], index.len());
                        col_lengths[1] = 1;
                        let mut other_cells = row
                            .iter()
                            .skip(self.offset_columns)
                            .enumerate()
                            .map(|(idx, c)| {
                                col_lengths[idx + 2] = max(col_lengths[idx + 2], c.len());
                                c.clone().into_tui_cell()
                            })
                            .collect();
                        cells.append(&mut other_cells);
                        let style = if idx % 2 == 0 {
                            Style::default()
                        } else {
                            Style::default().bg(palette::ALTERNATE_ROW_BG)
                        };
                        (cells, style)
                    }
                })
                .collect();
            // Maybe cutoff the columns if there's not enough space
            let mut columns_to_display = 0;
            let mut total_col_lengths = 0;
            // We want to fill at least 0.5 of the row, and no more
            // than 1.1. of it.  The magic numbers were picked
            // experimentally to "look good".
            let min_length = rect.width as f64 * 0.5;
            let max_length = rect.width as f64 * 1.1;
            for col_len in &col_lengths {
                if min_length < (total_col_lengths as f64)
                    && (1 + col_len + total_col_lengths) as f64 > max_length
                {
                    break;
                }
                total_col_lengths += 1 + col_len;
                columns_to_display += 1;
            }
            // Only show the overflow-left column if we're skipping columns.
            let skip_left = if self.offset_columns > 0 { 0 } else { 1 };
            let line_truncated = columns_to_display < num_cols;
            let header = {
                let mut cells: Vec<W::Cell> = vec![header[0].clone()]
                    .into_iter()
                    .chain(
                        header
                            .into_iter()
                            .skip(1 + skip_left)
                            .take(columns_to_display - 1 - skip_left),
                    )
                    .collect();
                if line_truncated {
                    cells.push(W::Cell::from(">").style(overflow_style));
                }
                W::Row::new(cells)
                    .style(Style::default().bg(palette::TABLE_HEADER_BG))
                    .height(1)
            };
            let rows: Vec<W::Row> = rows
                .into_iter()
                .map(|(cells, style)| {
                    let mut cells: Vec<W::Cell> = vec![cells[0].clone()]
                        .into_iter()
                        .chain(
                            cells
                                .into_iter()
                                .skip(1 + skip_left)
                                .take(columns_to_display - 1 - skip_left),
                        )
                        .collect();
                    if line_truncated {
                        cells.push(W::Cell::from(">").style(overflow_style))
                    }
                    W::Row::new(cells).style(style)
                })
                .collect();
            let mut col_lengths: Vec<usize> = vec![col_lengths[0]]
                .into_iter()
                .chain(
                    col_lengths
                        .into_iter()
                        .skip(1 + skip_left)
                        .take(columns_to_display - 1 - skip_left),
                )
                .collect();
            if line_truncated {
                col_lengths.push(1);
            }
            if line_truncated {
                // If the line is too long, penalize long columns and
                // give them a smaller proportion of the total width.
                for c in &mut col_lengths {
                    *c = 1 + (*c as f64).sqrt() as usize;
                }
            }
            let total_col_lengths_rescaled: usize = col_lengths.iter().sum();
            let col_lengths_len = col_lengths.len();
            for (idx, col_length) in col_lengths.into_iter().enumerate() {
                if (self.offset_columns > 0 && idx == 1)
                    || (line_truncated && idx == col_lengths_len - 1)
                {
                    column_widths.push(Constraint::Length(1));
                } else {
                    column_widths.push(Constraint::Ratio(
                        col_length as u32,
                        total_col_lengths_rescaled as u32,
                    ));
                }
            }
            W::Table::new(rows)
                .header(header)
                .highlight_style(selected_style)
                .widths(&column_widths)
        };
        let mut table_state = W::TableState::default();
        table_state.select(self.selected.map(|x| (x - self.offset_rows) as usize));
        centerpiece.render(rect, buffer, &mut table_state);
    }
}

fn find_next(
    rows: &[(Index, OrError<Vec<Cell>>)],
    start_from: i64,
    active_search: &Regex,
) -> Option<i64> {
    find_gen(rows.iter().enumerate(), start_from as usize, active_search)
}

fn find_prev(
    rows: &[(Index, OrError<Vec<Cell>>)],
    start_from: i64,
    active_search: &Regex,
) -> Option<i64> {
    let skip = rows.len() - (start_from as usize) - 1;
    find_gen(rows.iter().enumerate().rev(), skip, active_search)
}

fn find_gen<'a>(
    rows: impl Iterator<Item = (usize, &'a (Index, OrError<Vec<Cell>>))>,
    skip: usize,
    active_search: &Regex,
) -> Option<i64> {
    rows.skip(skip).find_map(|(idx_num, (idx, row))| {
        let index_matches = active_search.is_match(idx);
        let row_matches = match row {
            Err(err) => active_search.is_match(&err.to_string()),
            Ok(cells) => cells.iter().any(|cell| active_search.is_match(cell.text())),
        };
        if index_matches || row_matches {
            Some(idx_num as i64)
        } else {
            None
        }
    })
}
