use crate::cell::Cell;
use crate::import::*;
use crate::kube_config::KubeConfig;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::path::Path;
use crate::table::{Index, Table};
use crate::unique::Unique;

use std::sync::Arc;
use std::time::Duration;

const INDEX_HEADER: &str = "Context";
const NON_INDEX_HEADERS: &[&str] = &["User", "Cluster"];

#[derive(Debug)]
pub struct ContextsTable {
    path: Arc<Box<dyn Path>>,
    kube_config: Arc<Box<dyn KubeConfig>>,
    selected: Option<Index>,
}

impl ContextsTable {
    pub fn new_shared(
        kube_config: Arc<Box<dyn KubeConfig>>,
        path: Arc<Box<dyn Path>>,
    ) -> Box<dyn Table> {
        Box::new(Self::new(kube_config, path))
    }

    fn new(kube_config: Arc<Box<dyn KubeConfig>>, path: Arc<Box<dyn Path>>) -> Self {
        Self {
            path,
            kube_config,
            selected: None,
        }
    }
}

impl Table for ContextsTable {
    fn index_header(&self) -> &str {
        INDEX_HEADER
    }

    fn non_index_headers(&self) -> &[&str] {
        NON_INDEX_HEADERS
    }

    fn rows(&self) -> Vec<(Index, OrError<Vec<Cell>>)> {
        let mut rows: Vec<(Index, OrError<Vec<Cell>>)> = self
            .kube_config
            .contexts()
            .iter()
            .map(|(name, context)| {
                let idx: Index = (**name).into();
                (idx, row!(context.user(), context.cluster()))
            })
            .collect();
        rows.sort_by_key(|(index, _)| index.clone());
        rows
    }

    fn path(&self) -> Arc<Box<dyn Path>> {
        Arc::clone(&self.path)
    }

    fn self_update(
        &mut self,
        _: Duration,
        _: &mut Unique,
        last_updated: &mut LastUpdated,
    ) -> OrError<()> {
        *last_updated = LU::NeverUpdates;
        Ok(())
    }
}
