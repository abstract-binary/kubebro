// Don't use `import` here bacause this crate is used in `import`.
// use crate::import::*;

use regex::{Regex, RegexBuilder};

/// Create a new `Regex`.  If there was a regex compilation error,
/// returns `None`.  If the regex contained no uppercase letters and
/// no special characters, then case-insensitive mode is used.
pub fn new(str: &str) -> Option<Regex> {
    let case_sensitive = str.chars().any(|ch| {
        ch.is_uppercase()
            || ch == '.'
            || ch == '?'
            || ch == '*'
            || ch == '+'
            || ch == '('
            || ch == ')'
            || ch == '['
            || ch == ']'
            || ch == '{'
            || ch == '}'
    });
    RegexBuilder::new(str)
        .case_insensitive(!case_sensitive)
        .build()
        .ok()
}
