#[allow(unused_imports)]
use crate::import::*;

use crate::cache;

use std::time::Instant;

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum LastUpdated {
    NeverUpdates,
    Updated(Instant),
    FetchingData,
}

use LastUpdated as LU;

impl<T> From<&OrError<cache::Result<T>>> for LastUpdated {
    fn from(res: &OrError<cache::Result<T>>) -> LastUpdated {
        use cache::Result::*;
        match res {
            Err(_) => {
                // Set the `last_updated` timestamp to now so that we
                // effectively throttle the frequency of the
                // refreshes.  Without this, we query the cache at 60
                // FPS.
                LU::Updated(Instant::now())
            }
            Ok(Missing) => LU::FetchingData,
            Ok(Fresh(_, last_updated, _)) => LU::Updated(*last_updated),
            Ok(Stale(_, _, _)) => LU::FetchingData,
        }
    }
}
