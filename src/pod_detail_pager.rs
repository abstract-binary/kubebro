use crate::import::*;

use crate::explorer_pager::ExplorerPager;
use crate::explorer_view::ExplorerView;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::LastUpdated;
use crate::prompt::Prompt;
use crate::window_result::{WindowResult as WR, WindowResult};

use std::sync::Arc;
use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect};

pub struct PodDetailPager(ExplorerPager);

impl PodDetailPager {
    pub fn new_view(x: ExplorerPager) -> Box<dyn ExplorerView> {
        Box::new(Self(x))
    }
}

impl ExplorerView for PodDetailPager {
    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()> {
        self.0.self_update(refresh_rate)
    }

    fn last_updated(&self) -> LastUpdated {
        self.0.last_updated()
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        let mut key_binds = self.0.key_binds();
        key_binds.push(key_bind!(["L"], "Pod logs", "Show all logs for this pod"));
        key_binds
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        let res = self.0.handle_key_event(key);
        match res {
            WR::Handled | WR::CloseSelf | WR::PushOverlayWindow(_) | WR::Goto(_) => res,
            WR::NotHandled => match key.to_string().as_str() {
                "L" => match self.0.path().extend("logs") {
                    Ok(new_path) => WR::Goto(Arc::new(new_path)),
                    Err(err) => {
                        error!(
                            "Error constructing logs path ({} + logs): {}",
                            self.0.path(),
                            err
                        );
                        WR::Handled
                    }
                },
                _ => res,
            },
        }
    }

    fn render(&mut self, buffer: &mut Buffer, rect: Rect) {
        self.0.render(buffer, rect)
    }

    fn prompt(&self) -> Option<&Prompt> {
        self.0.prompt()
    }
}
