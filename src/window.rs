#[allow(unused_imports)]
use crate::import::*;

use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::path::Path;
use crate::prompt::Prompt;
use crate::window_result::WindowResult;

use std::sync::Arc;
use tui::{buffer::Buffer, layout::Rect};

pub trait Window {
    fn tick(&mut self);

    fn render(&mut self, buffer: &mut Buffer, tick: u64, rect: Rect);

    fn handle_key_event(&mut self, key: Key) -> WindowResult;

    fn key_binds(&self) -> Vec<KeyBindDesc>;

    fn path(&self) -> Option<Arc<Box<dyn Path>>> {
        None
    }

    fn prompt(&self) -> Option<&Prompt> {
        None
    }
}
