#![cfg_attr(debug_assertions, allow(dead_code, unused_imports, unused_variables))]
#![recursion_limit = "512"]

use clap::{App, Arg, SubCommand};
use simplelog::LevelFilter;
use std::os::unix::io::RawFd;
use std::sync::{Arc, Mutex};
use std::time::Duration;

#[macro_use]
mod import;
#[macro_use]
pub mod key;
#[macro_use]
mod key_bind;
#[macro_use]
mod cell;
#[macro_use]
mod table;
#[macro_use]
mod pager;
#[macro_use]
mod resource_table;

pub mod kube_config;
pub mod palette;
pub mod path;
pub mod smart_regex;
pub mod ui;
pub mod workflow;

mod cache;
mod cluster_command;
mod cluster_data;
mod cluster_overview_table;
mod condition_status;
mod contexts_table;
mod crippled_certificate_verifier;
mod dialog;
mod dummy_certificate_verifier;
mod endpoint;
mod explorer;
mod explorer_pager;
mod explorer_table;
mod explorer_view;
mod fixed_resolver;
mod health_check;
mod help;
mod last_updated;
mod layout;
mod newtypes;
mod pod_detail_pager;
mod pretty_pager;
mod prompt;
mod screen_logger;
mod tagged_line;
mod text_frag;
mod unique;
mod window;
mod window_result;

use import::*;
use key::Key;
use kube_config::{FileKubeConfig, KubeConfig};
use screen_logger::ScreenLogger;
use workflow::Workflow;

pub use cell::enable_stable_times;

const VERSION: &str = env!("CARGO_PKG_VERSION");
const DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");

pub fn main() -> OrError<()> {
    let matches = App::new("kubebro")
        .version(VERSION)
        .about(format!("https://gitlab.com/scvalex/kubebro/\n\n{}", DESCRIPTION).as_str())
        .arg(
            Arg::with_name("kubeconfig")
                .long("kubeconfig")
                .env("KUBECONFIG")
                .value_name("FILE")
                .help("Kubeconfig file to use (default: $HOME/.kube/config)")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("path")
                .value_name("PATH")
                .help(
                    "Path to navigate to (e.g. /CLUSTER, /CLUSTER/pods, \
		     /CLUSTER/pods/POD )",
                )
                .takes_value(true),
        )
        .arg(
            Arg::with_name("pin").long("pin").help(
                "When \"pinned\", the browser cannot navigate above the starting path",
            ),
        )
        .arg(
            Arg::with_name("debug")
                .long("debug")
                .help("Enable debug logging"),
        )
        .arg(
            Arg::with_name("save-log")
                .long("save-log")
                .takes_value(true)
                .help("Save the log to the given file"),
        )
        .subcommand(SubCommand::with_name("debug").about("show debug information"))
        .subcommand(
            SubCommand::with_name("demo")
                .arg(
                    Arg::with_name("workflow")
                        .long("workflow")
                        .takes_value(true)
                        .required(true)
                        .help("Workflow file to run"),
                )
                .about("automatically run commands from a workflow file"),
        )
        .get_matches();

    let home = get_home()?;
    let kubeconfig_file = matches
        .value_of("kubeconfig")
        .map(|s| s.into())
        .unwrap_or_else(|| format!("{}/.kube/config", home));
    let kube_config: Arc<Box<dyn KubeConfig>> =
        Arc::new(Box::new(FileKubeConfig::from_file(&kubeconfig_file)?));

    let path = matches
        .value_of("path")
        .map(|s| s.parse::<Box<dyn path::Path>>())
        .unwrap_or_else(|| {
            use path::*;
            let default_path = match kube_config
                .current_context()
                .and_then(|s| kube_config.contexts().get(s).map(|_| s))
            {
                None => Box::new(RootPath),
                Some(context) => RootPath.extend(context)?,
            };
            Ok(default_path)
        })
        .context("Failed to parse path argument")?;

    let save_log = matches.value_of("save-log");

    let pin = matches.is_present("pin");

    let level_filter = {
        if matches.is_present("debug") {
            LevelFilter::Debug
        } else {
            LevelFilter::Info
        }
    };

    if let Some(_matches) = matches.subcommand_matches("debug") {
        println!("Kubeconfig filename: {}", kubeconfig_file);
        println!("Kubeconfig: {:?}", kube_config);
        println!("Path: {}", path);
    } else {
        use std::panic::{catch_unwind, resume_unwind};
        let (log_fd, screen_logger) = setup_logging(save_log, level_filter, true)?;
        let terminal = Arc::new(Mutex::new(setup_terminal()?));
        match catch_unwind(|| {
            if let Some(matches) = matches.subcommand_matches("demo") {
                use path::Path;
                let workflow =
                    Workflow::read_file(matches.value_of("workflow").unwrap())?;
                let path =
                    Arc::new(workflow.initial_path.parse::<Box<dyn Path>>().unwrap());
                let mut poll_next_key = demo_poll_next_key(workflow)?;
                ui::run(
                    terminal,
                    &mut poll_next_key,
                    kube_config,
                    path,
                    pin,
                    log_fd,
                    screen_logger,
                )
            } else {
                let path = Arc::new(path);
                ui::run(
                    terminal,
                    &mut poll_next_key,
                    kube_config,
                    path,
                    pin,
                    log_fd,
                    screen_logger,
                )
            }
        }) {
            Ok(Ok(())) => teardown_terminal()?,
            Ok(Err(error)) => {
                teardown_terminal()?;
                print_last_log_messages(log_fd, save_log)?;
                return Err(error);
            }
            Err(error) => {
                teardown_terminal()?;
                print_last_log_messages(log_fd, save_log)?;
                resume_unwind(error)
            }
        }
    }
    Ok(())
}

fn get_home() -> OrError<String> {
    match std::env::var("HOME") {
        Ok(home) => Ok(home),
        Err(error) => bail!("Missing HOME environment variable: {}", error),
    }
}

fn setup_terminal() -> OrError<KbTerminal> {
    use crossterm::execute;
    use crossterm::terminal::{enable_raw_mode, EnterAlternateScreen};
    use std::io::stdout;
    use tui::{backend::CrosstermBackend, Terminal};

    enable_raw_mode()?;
    let mut stdout = stdout();
    execute!(stdout, EnterAlternateScreen)?;

    match std::env::var("TERM") {
        Ok(str) => info!("TERM={}", str),
        Err(error) => error!("No TERM variable: {}", error),
    }

    let backend = CrosstermBackend::new(stdout);
    let terminal = Terminal::new(backend)?;
    Ok(terminal)
}

fn teardown_terminal() -> OrError<()> {
    use crossterm::execute;
    use crossterm::terminal::{disable_raw_mode, LeaveAlternateScreen};
    use std::io::stdout;

    let mut stdout = stdout();
    execute!(stdout, LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(())
}

pub fn setup_logging(
    save_log: Option<&str>,
    level_filter: LevelFilter,
    redirect_stderr: bool,
) -> OrError<(RawFd, ScreenLogger)> {
    use simplelog::{CombinedLogger, ConfigBuilder, WriteLogger};
    use std::fs::OpenOptions;
    use std::fs::{remove_file, File};
    use std::os::unix::io::AsRawFd;
    let file = match save_log {
        Some(save_log) => OpenOptions::new()
            .create(true)
            .append(true)
            .open(save_log)
            .context(format!("Error creating log file: {}", save_log))?,
        None => {
            let path = tmp_log_filename();
            let file = File::create(&path)?;
            // Remove the file to make it "temporary".
            remove_file(&path)?;
            file
        }
    };
    let fd = file.as_raw_fd();
    if redirect_stderr {
        unsafe {
            libc::dup2(fd, libc::STDERR_FILENO);
        }
    }
    let write_logger = WriteLogger::new(
        level_filter,
        // Filter out jsonpath_lib log messages which are very verbose
        // in debug mode.
        ConfigBuilder::new()
            .add_filter_ignore_str("jsonpath_lib")
            .build(),
        file,
    );
    let screen_logger = ScreenLogger::new();
    CombinedLogger::init(vec![Box::new(screen_logger.clone()), write_logger])?;
    info!("Logger initialized");
    Ok((fd, screen_logger))
}

fn print_last_log_messages(log_fd: RawFd, save_log: Option<&str>) -> OrError<()> {
    use std::process::{Command, ExitStatus};
    println!("Kubebro crashed. Last log messages:");
    let pid = unsafe { libc::getpid() };
    let hidden_file = format!("/proc/{}/fd/{}", pid, log_fd);
    let mut child = Command::new("tail")
        .args(&["-n", "40", &hidden_file])
        .spawn()?;
    let _: ExitStatus = child.wait()?;
    let filename = match save_log {
        Some(filename) => filename.to_string(),
        None => {
            let filename = tmp_log_filename();
            let _: u64 = std::fs::copy(hidden_file, &filename)?;
            filename
        }
    };
    println!("Full log saved to {}", filename);
    Ok(())
}

fn poll_next_key() -> OrError<Option<Key>> {
    if crossterm::event::poll(Duration::from_millis(0))? {
        use crossterm::event::Event;
        match crossterm::event::read()? {
            Event::Key(key) => Ok(Some(Key::from(key))),
            Event::Mouse(_) | Event::Resize(_, _) => Ok(None),
        }
    } else {
        Ok(None)
    }
}

fn demo_poll_next_key(
    workflow: Workflow,
) -> OrError<impl FnMut() -> OrError<Option<Key>>> {
    use std::time::Instant;
    const TIME_BETWEEN_KEYSTROKES: Duration = Duration::from_millis(200);
    const TIME_BETWEEN_STEPS: Duration = Duration::from_millis(2000);
    const LINGER_DELAY: Duration = Duration::from_millis(1000);
    let mut keystrokes: Vec<(Instant, Key)> = vec![];
    let mut now = Instant::now() + LINGER_DELAY;
    for (_, step) in workflow.steps() {
        for input in &step.inputs {
            if input == "Ret" {
                now += LINGER_DELAY;
            }
            let key = input.parse::<Key>()?;
            keystrokes.push((now, key));
            now += TIME_BETWEEN_KEYSTROKES;
        }
        now += TIME_BETWEEN_STEPS;
    }
    keystrokes.push((now + LINGER_DELAY, "C-c".parse::<Key>()?));
    keystrokes.reverse();
    Ok(move || -> OrError<Option<Key>> {
        if keystrokes.is_empty() {
            Ok(Some("C-c".parse::<Key>()?))
        } else {
            let (key_time, _) = keystrokes[keystrokes.len() - 1];
            if Instant::now() >= key_time {
                let (_, key) = keystrokes.pop().unwrap();
                Ok(Some(key))
            } else {
                Ok(None)
            }
        }
    })
}

fn tmp_log_filename() -> String {
    match std::env::var("TMPDIR") {
        Ok(tmpdir) => format!("{}/kubebro.log", tmpdir),
        Err(_) => "kubebro.log".to_string(),
    }
}
