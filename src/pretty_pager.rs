use crate::cache::Cache;
use crate::cluster_command as cc;
use crate::import::*;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::pager::{self, BackAndForthIterator, Pager};
use crate::path::Path;
use crate::tagged_line::TaggedLine;
use crate::text_frag::TextFrag;
use crate::unique::Unique;

use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use tui::style::{Color, Style};

lazy_static! {
    static ref CONTROL_CHARS: Regex = Regex::new("\\x1B\\[[0-9;]*[a-zA-Z]").unwrap();
}

pub struct PrettyPager {
    path: Arc<Box<dyn Path>>,
    cache: Arc<Cache>,
    backend: Option<Backend>,
    /// Be careful because `text_cache` is not always populated with
    /// the whole result.
    text_cache: Vec<Vec<TextFrag>>,
    last_updated: LastUpdated,
    cached_id: Unique,
    colors: HashMap<String, Color>,
    color_idx: usize,
}

#[derive(Debug)]
enum Backend {
    Json(Arc<cc::Response>),
    Lines(Arc<Mutex<Vec<TaggedLine>>>),
}

impl PrettyPager {
    pub fn new(
        path: Arc<Box<dyn Path>>,
        cache: Arc<Cache>,
        refresh_rate: Duration,
    ) -> Box<Self> {
        let mut t = Box::new(Self {
            path,
            cache: Arc::clone(&cache),
            backend: None,
            text_cache: vec![],
            last_updated: LU::FetchingData,
            cached_id: Unique::new(),
            colors: HashMap::new(),
            color_idx: 0,
        });
        let _: OrError<_> = t.self_update(refresh_rate);
        t
    }
}

impl Pager for PrettyPager {
    pager_accessors!();

    fn text(
        &mut self,
        first_line: usize,
        num_lines: usize,
        _wrap_columns: Option<usize>,
    ) -> OrError<Vec<Vec<TextFrag>>> {
        match self.backend {
            None => {
                self.text_cache = vec![];
                Ok(vec![])
            }
            Some(Backend::Json(_)) => Ok(self
                .text_cache
                .iter()
                .skip(first_line)
                .take(num_lines)
                .cloned()
                .collect()),
            Some(Backend::Lines(ref lines)) => {
                let lines = lines
                    .lock()
                    .map_err(|err| anyhow!("Response mutex poisoned: {}", err))?;
                Ok(pretty_print_lines(
                    lines.iter().skip(first_line).take(num_lines),
                    &mut self.colors,
                    &mut self.color_idx,
                )
                .collect())
            }
        }
    }

    fn with_text_iter(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(pager::Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>> {
        match self.backend {
            None => {
                self.text_cache = vec![];
                Ok(f((Box::new(self.text_cache.iter().skip(skip))
                    as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
                    .into()))
            }
            Some(Backend::Json(_)) => Ok(f((Box::new(self.text_cache.iter().skip(skip))
                as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
                .into())),
            Some(Backend::Lines(ref lines)) => {
                let lines = lines
                    .lock()
                    .map_err(|err| anyhow!("Response mutex poisoned: {}", err))?;
                self.text_cache = vec![];
                Ok(f(pretty_print_lines(
                    lines.iter().skip(skip),
                    &mut self.colors,
                    &mut self.color_idx,
                )
                .into()))
            }
        }
    }

    fn with_text_iter_rev(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(pager::Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>> {
        match self.backend {
            None => {
                self.text_cache = vec![];
                Ok(f((Box::new(self.text_cache.iter().rev().skip(skip))
                    as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
                    .into()))
            }
            Some(Backend::Json(_)) => {
                Ok(f((Box::new(self.text_cache.iter().rev().skip(skip))
                    as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
                    .into()))
            }
            Some(Backend::Lines(ref lines)) => {
                let lines = lines
                    .lock()
                    .map_err(|err| anyhow!("Response mutex poisoned: {}", err))?;
                self.text_cache = vec![];
                Ok(f(pretty_print_lines(
                    lines.iter().rev().skip(skip),
                    &mut self.colors,
                    &mut self.color_idx,
                )
                .into()))
            }
        }
    }

    fn num_lines(&self, _wrap_columns: Option<usize>) -> OrError<usize> {
        match self.backend {
            None => Ok(0),
            Some(Backend::Json(_)) => Ok(self.text_cache.len()),
            Some(Backend::Lines(ref lines)) => {
                let lines = lines
                    .lock()
                    .map_err(|err| anyhow!("Response mutex poisoned: {}", err))?;
                Ok(lines.len())
            }
        }
    }

    fn num_columns(
        &self,
        first_line: usize,
        num_lines: usize,
        _wrap_columns: Option<usize>,
    ) -> OrError<usize> {
        match self.backend {
            None => Ok(0),
            Some(Backend::Json(_)) => Ok(self
                .text_cache
                .iter()
                .skip(first_line)
                .take(num_lines)
                .map(|l| l.iter().map(|f| f.text.len()).sum())
                .max()
                .unwrap_or(0)),
            Some(Backend::Lines(ref lines)) => {
                let lines = lines
                    .lock()
                    .map_err(|err| anyhow!("Response mutex poisoned: {}", err))?;
                // For speed, we don't pretty print here.  Instead, we
                // manually add the `max_src_len` to the line length.
                Ok(lines
                    .iter()
                    .skip(first_line)
                    .take(num_lines)
                    .map(|l| {
                        let src_len = match l.src {
                            None => 0,
                            Some(_) => l.max_src_len + 1,
                        };
                        src_len + l.text.len()
                    })
                    .max()
                    .unwrap_or(0))
            }
        }
    }

    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()> {
        let backend_or_err = match self.path.cluster_query() {
            None => bail!("Cluster query not implemented for {}", self.path),
            Some((_, query)) => {
                use cc::ClusterCommand;
                if let Some(stream_command) = query.streaming() {
                    self.cache
                        .stream_lines(stream_command)
                        .map(|x| x.map(|y| Backend::Lines(Arc::clone(&y))))
                } else {
                    self.cache
                        .get(Arc::clone(&self.path), refresh_rate)
                        .map(|x| x.map(|y| Backend::Json(Arc::clone(&y))))
                }
            }
        };
        let last_updated = (&backend_or_err).into();
        use crate::cache::Result::*;
        match backend_or_err {
            Err(error) => {
                error!("Failed to get {}: {}", self.path, error);
                self.last_updated = last_updated;
                self.backend = None;
            }
            Ok(Missing) => {
                self.last_updated = last_updated;
                self.backend = None;
            }
            Ok(Fresh(backend, _, new_cached_id))
            | Ok(Stale(backend, _, new_cached_id)) => {
                if self.cached_id != new_cached_id {
                    self.cached_id = new_cached_id;
                    self.last_updated = last_updated;
                    match &backend {
                        Backend::Json(ref resp) => {
                            self.text_cache = match resp.json() {
                                None => vec![],
                                Some(json) => pretty_print_json(json),
                            };
                        }
                        Backend::Lines(_) => {
                            // We compute the `text_cache` lazily to
                            // better handle very big logs.
                            self.text_cache = vec![];
                        }
                    }
                    self.backend = Some(backend);
                }
            }
        }
        Ok(())
    }
}

// Rules:
// - Empty maps and vectors are printed as {} and [].
// - Non-empty maps and vectors are printed as:
//   * opening delimiter on the current line
//   * items on subsequent lines indented by 2 and followed by comma,
//   * final item does not have a comma
//   * closing delimiter on a separate last line
// - Double quotes in strings are escaped with single backslashes
fn pretty_print_json(json: &Json) -> Vec<Vec<TextFrag>> {
    let mut frags = vec![];
    let mut last_line = vec![];
    pretty_print_json_internal(json, &mut last_line, &mut frags, 2);
    transfer_last_line(&mut last_line, &mut frags, 0);
    frags
}

fn pretty_print_json_internal(
    json: &Json,
    last_line: &mut Vec<TextFrag>,
    frags: &mut Vec<Vec<TextFrag>>,
    indent: usize,
) {
    use serde_json::Value::*;
    let object_key_style = Style::default().fg(palette::JSON_KEY_FG);
    let string_style = Style::default().fg(palette::JSON_STRING_FG);
    let punctuation = |s: &str| -> TextFrag { TextFrag::new(s.into()) };
    match json {
        Null => last_line.push(TextFrag::new("null".into())),
        Bool(x) => last_line.push(TextFrag::new(x.to_string())),
        Number(n) => last_line.push(TextFrag::new(n.to_string())),
        String(s) => {
            pretty_print_json_string(s, last_line, &string_style);
        }
        Array(vs) => {
            if vs.is_empty() {
                last_line.push(punctuation("[]"));
            } else {
                last_line.push(punctuation("["));
                transfer_last_line(last_line, frags, indent - 2);
                let count = vs.len();
                for (idx, v) in vs.iter().enumerate() {
                    pretty_print_json_internal(v, last_line, frags, indent + 2);
                    if idx < count - 1 {
                        last_line.push(punctuation(","));
                    }
                    transfer_last_line(last_line, frags, indent);
                }
                last_line.push(punctuation("]"));
            }
        }
        Object(vs) => {
            if vs.is_empty() {
                last_line.push(punctuation("{}"));
            } else {
                last_line.push(punctuation("{"));
                transfer_last_line(last_line, frags, indent - 2);
                let count = vs.len();
                for (idx, (k, v)) in vs.iter().enumerate() {
                    pretty_print_json_string(k, last_line, &object_key_style);
                    last_line.push(punctuation(": "));
                    pretty_print_json_internal(v, last_line, frags, indent + 2);
                    if idx < count - 1 {
                        last_line.push(punctuation(","));
                    }
                    transfer_last_line(last_line, frags, indent);
                }
                last_line.push(punctuation("}"));
            }
        }
    }
}

fn transfer_last_line(
    last_line: &mut Vec<TextFrag>,
    frags: &mut Vec<Vec<TextFrag>>,
    indent: usize,
) {
    if !last_line.is_empty() {
        let mut v = Vec::with_capacity(1 + last_line.len());
        v.push(TextFrag::new(" ".repeat(indent)));
        v.append(last_line);
        frags.push(v);
    }
}

fn pretty_print_json_string(str: &str, last_line: &mut Vec<TextFrag>, style: &Style) {
    last_line.push(TextFrag::styled("\"".into(), *style));
    last_line.push(TextFrag::styled(str.escape_default().to_string(), *style));
    last_line.push(TextFrag::styled("\"".into(), *style));
}

fn pretty_print_lines<'a, I>(
    lines: I,
    colors: &'a mut HashMap<String, Color>,
    color_idx: &'a mut usize,
) -> Box<dyn BackAndForthIterator<Vec<TextFrag>> + 'a>
where
    I: 'a + Iterator<Item = &'a TaggedLine> + DoubleEndedIterator + ExactSizeIterator,
{
    Box::new(lines.map(
        move |TaggedLine {
                  src,
                  max_src_len,
                  text,
              }| {
            let mut frags = Vec::with_capacity(4);
            if let Some(src) = src {
                frags.push(TextFrag::new(" ".repeat(max_src_len - src.len())));
                frags.push(TextFrag::styled(
                    src.to_string(),
                    src_style(src, colors, color_idx),
                ));
                frags.push(TextFrag::new(" ".into()));
            }
            // TODO-soon Optimize this control char stripping because
            // it turns out that regex replaces are a lot more
            // expensive than a filter.
            frags.push(TextFrag::new(CONTROL_CHARS.replace_all(text, "").into()));
            frags
        },
    ))
}

fn src_style(
    src: &str,
    colors: &mut HashMap<String, Color>,
    color_idx: &mut usize,
) -> Style {
    let color = {
        match colors.get(src) {
            Some(c) => *c,
            None => {
                let color = palette::PAGER_SRC_FG[*color_idx];
                *color_idx = (*color_idx + 1) % palette::PAGER_SRC_FG.len();
                colors.insert(src.into(), color);
                color
            }
        }
    };
    Style::default().fg(color)
}
