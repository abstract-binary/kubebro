#[allow(unused_imports)]
use crate::import::*;

use rustls::{
    Certificate, OwnedTrustAnchor, RootCertStore, ServerCertVerified, ServerCertVerifier,
    TLSError,
};
use std::time::SystemTime;
use webpki::{DNSNameRef, EndEntityCert, SignatureAlgorithm, TrustAnchor};

pub struct CrippledCertificateVerifier;

/// Which signature verification mechanisms we support.  No particular
/// order.
static SUPPORTED_SIG_ALGS: &[&SignatureAlgorithm] = &[
    &webpki::ECDSA_P256_SHA256,
    &webpki::ECDSA_P256_SHA384,
    &webpki::ECDSA_P384_SHA256,
    &webpki::ECDSA_P384_SHA384,
    &webpki::ED25519,
    &webpki::RSA_PSS_2048_8192_SHA256_LEGACY_KEY,
    &webpki::RSA_PSS_2048_8192_SHA384_LEGACY_KEY,
    &webpki::RSA_PSS_2048_8192_SHA512_LEGACY_KEY,
    &webpki::RSA_PKCS1_2048_8192_SHA256,
    &webpki::RSA_PKCS1_2048_8192_SHA384,
    &webpki::RSA_PKCS1_2048_8192_SHA512,
    &webpki::RSA_PKCS1_3072_8192_SHA384,
];

impl ServerCertVerifier for CrippledCertificateVerifier {
    /// Will verify the certificate is valid in the following ways:
    /// - Signed by a valid root
    /// - Not Expired
    ///
    /// Based on a https://github.com/ctz/rustls/issues/578#issuecomment-816712636
    fn verify_server_cert(
        &self,
        roots: &RootCertStore,
        intermediates: &[Certificate],
        _dns_name: DNSNameRef<'_>,
        ocsp_response: &[u8],
    ) -> Result<ServerCertVerified, TLSError> {
        // Get the end-entity cert, tthe chain, and the trust root. Error out if
        // chain is empty.
        let (cert, chain, trustroots) = prepare(roots, intermediates)?;

        // Validate the certificate is valid, signed by a trusted root, and not
        // expired.
        let now = SystemTime::now();
        let webpki_now =
            webpki::Time::try_from(now).map_err(|_| TLSError::FailedToGetCurrentTime)?;

        let _cert: EndEntityCert = cert
            .verify_is_valid_tls_server_cert(
                SUPPORTED_SIG_ALGS,
                &webpki::TLSServerTrustAnchors(&trustroots),
                &chain,
                webpki_now,
            )
            .map_err(TLSError::WebPKIError)
            .map(|_| cert)?;

        if !ocsp_response.is_empty() {
            //trace!("Unvalidated OCSP response: {:?}", ocsp_response.to_vec());
        }
        Ok(ServerCertVerified::assertion())
    }
}

#[allow(clippy::type_complexity)]
fn prepare<'a, 'b>(
    roots: &'b RootCertStore,
    presented_certs: &'a [Certificate],
) -> Result<(EndEntityCert<'a>, Vec<&'a [u8]>, Vec<TrustAnchor<'b>>), TLSError> {
    if presented_certs.is_empty() {
        return Err(TLSError::NoCertificatesPresented);
    }

    // EE cert must appear first.
    let cert = webpki::EndEntityCert::from(&presented_certs[0].0)
        .map_err(TLSError::WebPKIError)?;

    let chain: Vec<&'a [u8]> = presented_certs
        .iter()
        .skip(1)
        .map(|cert| cert.0.as_ref())
        .collect();

    let trustroots: Vec<webpki::TrustAnchor> = roots
        .roots
        .iter()
        .map(OwnedTrustAnchor::to_trust_anchor)
        .collect();

    Ok((cert, chain, trustroots))
}
