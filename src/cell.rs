use crate::condition_status::ConditionStatus;
use crate::health_check::HealthResult;
use crate::import::*;

use chrono::{DateTime, FixedOffset};
use std::collections::HashMap;
use std::sync::atomic::{AtomicBool, Ordering};
use tui::style::{Color, Modifier};
use tui::widgets as W;

static STABLE_TIMES: AtomicBool = AtomicBool::new(false);

#[derive(Clone, Debug)]
pub struct Cell {
    text: String,
    fg: Option<Color>,
    bg: Option<Color>,
    modifier: Modifier,
}

impl Cell {
    pub fn new(text: String) -> Self {
        Self {
            text,
            ..Self::default()
        }
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn len(&self) -> usize {
        self.text.len()
    }

    pub fn into_tui_cell(self) -> W::Cell<'static> {
        use tui::style::Style;
        use tui::text::Text;
        let mut style = Style::default().add_modifier(self.modifier);
        if let Some(fg) = self.fg {
            style = style.fg(fg);
        }
        if let Some(bg) = self.bg {
            style = style.bg(bg);
        }
        W::Cell::from(Text::raw(self.text)).style(style)
    }
}

impl Default for Cell {
    fn default() -> Self {
        Cell {
            text: "".into(),
            fg: None,
            bg: None,
            modifier: Modifier::empty(),
        }
    }
}

macro_rules! impl_cell_from_display {
    ($t:ty) => {
        impl From<$t> for crate::cell::Cell {
            fn from(s: $t) -> Self {
                crate::cell::Cell::new(s.to_string())
            }
        }
    };
}

impl_cell_from_display!(&str);
impl_cell_from_display!(KNamespace);
impl_cell_from_display!(String);
impl_cell_from_display!(&String);
impl_cell_from_display!(u64);
impl_cell_from_display!(usize);

impl From<&Vec<String>> for Cell {
    fn from(v: &Vec<String>) -> Self {
        Self {
            text: v.join(", "),
            ..Self::default()
        }
    }
}

impl From<&Vec<KName>> for Cell {
    fn from(v: &Vec<KName>) -> Self {
        Self {
            text: v
                .iter()
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join(", "),
            ..Self::default()
        }
    }
}

impl From<&Vec<(KNamespace, KName)>> for Cell {
    fn from(v: &Vec<(KNamespace, KName)>) -> Self {
        Self {
            text: v
                .iter()
                .map(|(ns, s)| format!("{}/{}", ns, s))
                .collect::<Vec<String>>()
                .join(", "),
            ..Self::default()
        }
    }
}

impl From<Option<&String>> for Cell {
    fn from(v: Option<&String>) -> Self {
        Self {
            text: v.cloned().unwrap_or_else(|| "".into()),
            ..Self::default()
        }
    }
}

impl From<(u64, u64)> for Cell {
    fn from((x, y): (u64, u64)) -> Self {
        let fg = if x == y {
            palette::CELL_HEALTHY_FG
        } else {
            palette::CELL_QUESTIONABLE_FG
        };
        Self {
            text: format!("{}/{}", x, y),
            fg: Some(fg),
            ..Self::default()
        }
    }
}

impl From<HealthResult> for Cell {
    fn from(hr: HealthResult) -> Self {
        use HealthResult::*;
        let fg = match hr {
            Unknown => None,
            Healthy => Some(palette::CELL_HEALTHY_FG),
            Unhealthy(_) => Some(palette::CELL_UNHEALTHY_FG),
        };
        Self {
            text: hr.to_string(),
            fg,
            ..Self::default()
        }
    }
}

impl<T> From<(T, Color)> for Cell
where
    T: Into<Cell>,
{
    fn from((x, fg): (T, Color)) -> Self {
        Self {
            fg: Some(fg),
            ..x.into()
        }
    }
}

impl From<Option<&ConditionStatus>> for Cell {
    fn from(cs: Option<&ConditionStatus>) -> Self {
        use ConditionStatus::*;
        match cs {
            None | Some(Unknown) | Some(False) => {
                ("Not ready", palette::CELL_QUESTIONABLE_FG).into()
            }
            Some(True) => ("Ready", palette::CELL_HEALTHY_FG).into(),
        }
    }
}

/// Wrap a `HashMap<String, String>` in a `DataTable` to display it as
/// "key1: N bytes, key2: M bytes, ...".
#[derive(Debug)]
pub struct DataTable<'a>(pub &'a HashMap<String, String>);

impl<'a> From<DataTable<'a>> for Cell {
    fn from(data: DataTable<'a>) -> Self {
        let mut items = data
            .0
            .iter()
            .map(|(k, v)| format!("{}: {} bytes", k, v.as_bytes().len()))
            .collect::<Vec<String>>();
        items.sort();
        items.join(", ").into()
    }
}

/// Wrap a `DateTime` in a `TimeAgo` to display it as "23s ago", or
/// "3h4m ago".
pub struct TimeAgo(pub DateTime<FixedOffset>);

impl From<TimeAgo> for Cell {
    fn from(time: TimeAgo) -> Self {
        if STABLE_TIMES.load(Ordering::Relaxed) {
            time.0.naive_utc().to_string().into()
        } else {
            use chrono::Utc;
            let delta = Utc::now() - time.0.with_timezone(&Utc);
            let num_days = delta.num_days();
            let days = if num_days > 0 {
                format!("{}d", num_days)
            } else {
                "".into()
            };
            let num_hours = delta.num_hours() - num_days * 24;
            let hours = if num_hours > 0 {
                format!("{}h", num_hours)
            } else {
                "".into()
            };
            let num_mins = delta.num_minutes() - num_days * 24 * 60 - num_hours * 60;
            let mins = if num_mins > 0 {
                format!("{}m", num_mins)
            } else {
                "".into()
            };
            if days.is_empty() && hours.is_empty() && mins.is_empty() {
                "<1m ago".into()
            } else {
                // Only show the biggest two parts for readability
                if !days.is_empty() {
                    format!("{}{} ago", days, hours).into()
                } else {
                    format!("{}{} ago", hours, mins).into()
                }
            }
        }
    }
}

pub fn enable_stable_times() {
    STABLE_TIMES.store(true, Ordering::Relaxed);
}

/// Wrap a `Vec<_>` in a `NonEmptyIsRed` to display it red if it's not
/// empty.
pub struct NonEmptyIsRed<'a, T>(pub &'a Vec<T>);

impl<'a, T> From<NonEmptyIsRed<'a, T>> for Cell
where
    &'a Vec<T>: Into<Cell>,
{
    fn from(NonEmptyIsRed(data): NonEmptyIsRed<'a, T>) -> Self {
        if data.is_empty() {
            data.into()
        } else {
            Cell {
                fg: Some(palette::CELL_UNHEALTHY_FG),
                ..(data.into())
            }
        }
    }
}

macro_rules! row {
    ($($cell:expr),*) => {
	Ok(vec![$($cell.into()),*])
    }
}
