fn main() {
    match kubebro::main() {
        Ok(()) => (),
        Err(error) => {
            // We have to do this because stderr might have been
            // redirected to the log file at this point.
            println!("{}", error);
            std::process::exit(1);
        }
    }
}
