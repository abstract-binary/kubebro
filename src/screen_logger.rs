/// A logger implementation that captures notices and makes them
/// available to the UI for display in the status bar at the bottom.

#[allow(unused_imports)]
use crate::import::*;

use log::{Level, Log, Metadata, Record};
use simplelog::{Config, LevelFilter, SharedLogger};
use std::fmt;
use std::sync::Arc;
use std::sync::Mutex;

#[derive(Clone)]
pub struct ScreenLogger(Arc<ScreenLoggerInternal>);

#[derive(Debug)]
pub struct Notice {
    pub error: bool,
    pub text: String,
}

struct ScreenLoggerInternal {
    notices: Mutex<Vec<Notice>>,
}

impl ScreenLogger {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self(Arc::new(ScreenLoggerInternal {
            notices: Mutex::new(vec![]),
        }))
    }

    pub fn take_notices(&self) -> Vec<Notice> {
        let Self(ref screen_logger) = self;
        match screen_logger.notices.lock() {
            Err(error) => panic!("Log mutex was poisoned: {}", error),
            Ok(ref mut notices) => notices.drain(..).collect::<Vec<_>>(),
        }
    }
}

impl fmt::Debug for ScreenLogger {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("ScreenLogger")
    }
}

impl Log for ScreenLogger {
    fn enabled(&self, metadata: &Metadata<'_>) -> bool {
        metadata.level() <= LevelFilter::Warn
    }

    fn log(&self, record: &Record<'_>) {
        if self.enabled(record.metadata()) {
            let Self(ref screen_logger) = self;
            match screen_logger.notices.lock() {
                Err(error) => panic!("Log mutex was poisoned: {}", error),
                Ok(ref mut notices) => notices.push(Notice {
                    error: record.level() == Level::Error,
                    text: format!("{}", record.args()),
                }),
            }
        }
    }

    fn flush(&self) {}
}

impl SharedLogger for ScreenLogger {
    fn level(&self) -> LevelFilter {
        LevelFilter::Error
    }

    fn config(&self) -> Option<&Config> {
        None
    }

    fn as_log(self: Box<Self>) -> Box<dyn Log> {
        Box::new(*self)
    }
}
