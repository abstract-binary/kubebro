// Do not use import here because we'll use this module in import.
// use crate::import::*;

use std::fmt;
use std::ops::Deref;

macro_rules! newtype_string {
    ($struct:ident) => {
        #[derive(Clone, Debug, Eq, Hash, PartialEq)]
        pub struct $struct(String);

        impl Deref for $struct {
            type Target = str;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl fmt::Display for $struct {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                fmt::Display::fmt(&**self, f)
            }
        }

        impl From<String> for $struct {
            fn from(str: String) -> Self {
                Self(str)
            }
        }

        impl From<&str> for $struct {
            fn from(str: &str) -> Self {
                Self(str.into())
            }
        }
    };
}

newtype_string!(KContext);
newtype_string!(KName);
newtype_string!(KNamespace);
newtype_string!(KContainer);
