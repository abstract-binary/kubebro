#[allow(unused_imports)]
use crate::import::*;

use crate::key::{Key, Keys};
use crate::window_result::*;

use std::collections::HashMap;
use std::fmt;

pub type Action<C> = Box<dyn Fn(C) -> WindowResult>;

pub struct KeyBind<C> {
    pub keys: Keys,
    pub short_desc: &'static str,
    pub long_desc: &'static str,
    pub action: Action<C>,
}

impl<C> fmt::Debug for KeyBind<C> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("KeyBind")
            .field("keys", &self.keys)
            .field("short_desc", &self.short_desc)
            .finish()
    }
}

impl<C> KeyBind<C> {
    pub fn new(
        keys: Keys,
        short_desc: &'static str,
        long_desc: &'static str,
        action: Action<C>,
    ) -> Self {
        Self {
            keys,
            short_desc,
            long_desc,
            action,
        }
    }

    pub fn desc(&self) -> KeyBindDesc {
        KeyBindDesc::new(self.keys.clone(), self.short_desc, self.long_desc, false)
    }
}

pub fn to_map<C>(key_binds: &[KeyBind<C>]) -> HashMap<Key, &KeyBind<C>> {
    key_binds
        .iter()
        .flat_map(|kb| {
            kb.keys
                .iter()
                .map(|k| (*k, kb))
                .collect::<Vec<(Key, &KeyBind<C>)>>()
        })
        .collect()
}

pub struct KeyBindDesc {
    pub keys: Keys,
    pub short_desc: &'static str,
    pub long_desc: &'static str,
    /// If `advanced = true`, then the key binding is only shown in
    /// the help menu and not in the help bar.
    pub advanced: bool,
}

impl KeyBindDesc {
    pub fn new(
        keys: Keys,
        short_desc: &'static str,
        long_desc: &'static str,
        advanced: bool,
    ) -> KeyBindDesc {
        Self {
            keys,
            short_desc,
            long_desc,
            advanced,
        }
    }
}

macro_rules! key_bind {
    ([$($e:expr),*], $short_desc:expr, $long_desc:expr) => {
	crate::key_bind::KeyBindDesc::new(keys!($($e),*), $short_desc, $long_desc, false)
    }
}

macro_rules! key_bind_adv {
    ([$($e:expr),*], $short_desc:expr, $long_desc:expr) => {
	crate::key_bind::KeyBindDesc::new(keys!($($e),*), $short_desc, $long_desc, true)
    }
}
