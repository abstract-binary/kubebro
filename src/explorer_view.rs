use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::LastUpdated;
use crate::prompt::Prompt;
use crate::window_result::WindowResult;

use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect};

pub trait ExplorerView {
    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()>;
    fn last_updated(&self) -> LastUpdated;

    fn key_binds(&self) -> Vec<KeyBindDesc>;
    fn handle_key_event(&mut self, key: Key) -> WindowResult;

    fn render(&mut self, buffer: &mut Buffer, rect: Rect);

    fn prompt(&self) -> Option<&Prompt> {
        None
    }
}
