#[allow(unused_imports)]
use crate::import::*;

use crate::explorer_pager::ExplorerPager;
use crate::explorer_view::ExplorerView;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::pager::{self, BackAndForthIterator, Pager};
use crate::path::{self, Path};
use crate::prompt::Prompt;
use crate::text_frag::TextFrag;
use crate::window::Window;
use crate::window_result::{WindowResult, WindowResult as WR};

use std::sync::Arc;
use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect};

const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
const VERSION: &str = env!("CARGO_PKG_VERSION");
const DESCRIPTION: &str = env!("CARGO_PKG_DESCRIPTION");
const LICENSE: &str = env!("CARGO_PKG_LICENSE");

pub struct Help(ExplorerPager);

impl Help {
    pub fn new(key_binds: Vec<KeyBindDesc>, common_key_binds: Vec<KeyBindDesc>) -> Self {
        let dummy_explorer = {
            let pager = Box::new(HelpPager::new());
            ExplorerPager::new(pager)
        };
        let mut pager = Box::new(HelpPager::new());
        pager.set_key_binds(vec![
            ("View".into(), key_binds),
            ("Help".into(), dummy_explorer.key_binds()),
            ("Common".into(), common_key_binds),
        ]);
        let explorer = ExplorerPager::new(pager);
        Self(explorer)
    }
}

impl Window for Help {
    fn tick(&mut self) {}

    fn render(&mut self, buffer: &mut Buffer, _tick: u64, rect: Rect) {
        self.0.render(buffer, rect);
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        let mut key_binds = self.0.key_binds();
        key_binds.push(key_bind!(["q"], "Quit", "Quit help"));
        key_binds
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        #[allow(unreachable_patterns)]
        match self.0.handle_key_event(key) {
            WR::NotHandled => match key.to_string().as_str() {
                "q" | "Esc" | "Bksp" => WR::CloseSelf,
                _ => WR::NotHandled,
            },
            res => res,
        }
    }

    fn prompt(&self) -> Option<&Prompt> {
        self.0.prompt()
    }
}

#[derive(Debug)]
struct HelpPager {
    text: Vec<Vec<TextFrag>>,
}

impl HelpPager {
    pub fn new() -> Self {
        Self { text: vec![] }
    }

    pub fn set_key_binds(&mut self, key_binds: Vec<(String, Vec<KeyBindDesc>)>) {
        use tui::style::{Modifier, Style};
        let mut text: Vec<Vec<TextFrag>> = vec![];
        let light_blue = Style::default()
            .fg(palette::HELP_TITLE_FG)
            .add_modifier(Modifier::BOLD);
        let light_green = Style::default()
            .fg(palette::HELP_EMPHASIS_FG)
            .add_modifier(Modifier::BOLD);
        let red = Style::default()
            .fg(palette::HELP_SUPER_EMPHASIS_FG)
            .add_modifier(Modifier::BOLD);
        let bold = Style::default().add_modifier(Modifier::BOLD);
        macro_rules! l(
	    ($($x:expr),*) => (text.push(vec![TextFrag::new(format!($($x,)*))]));
	    ($($x:expr),*; $style:expr) => (text.push(vec![TextFrag::styled(format!($($x,)*), $style)]));
	);
        l!("kubebro {} - (C) 2021 {}", VERSION, AUTHORS; light_blue);
        l!("https://gitlab.com/scvalex/kubebro/"; light_green);
        l!("");
        for l in DESCRIPTION.lines() {
            l!("{}", l);
        }
        l!("");
        l!("Released under {}.", LICENSE);
        l!("This program comes with ABSOLUTELY NO WARRANTY.");
        l!("This is free software, and you are welcome to redistribute it");
        l!("under certain conditions. See https://gitlab.com/scvalex/kubebro/");
        l!("for more details.");
        l!("");
        for (group, key_binds) in key_binds.iter() {
            text.push(vec![
                TextFrag::styled(group.into(), bold),
                TextFrag::new(" key bindings:".into()),
            ]);
            l!("");
            let max_keys_len = 1 + key_binds
                .iter()
                .map(|kbd| kbd.keys.to_string().len())
                .max()
                .unwrap_or(0);
            for kbd in key_binds.iter() {
                let keys = kbd.keys.to_string();
                text.push(vec![
                    TextFrag::new(" ".repeat(max_keys_len - keys.len())),
                    TextFrag::styled(keys, light_blue),
                    TextFrag::styled(": ".into(), light_blue),
                    TextFrag::new(kbd.long_desc.into()),
                ]);
            }
            l!("");
        }
        l!("Press q to return."; red);
        self.text = text;
    }
}

impl Pager for HelpPager {
    fn self_update(&mut self, _: Duration) -> OrError<()> {
        Ok(())
    }

    fn text(
        &mut self,
        first_line: usize,
        num_lines: usize,
        _wrap_columns: Option<usize>,
    ) -> OrError<Vec<Vec<TextFrag>>> {
        Ok(self
            .text
            .iter()
            .skip(first_line)
            .take(num_lines)
            .cloned()
            .collect())
    }

    fn with_text_iter(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(pager::Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>> {
        Ok(f((Box::new(self.text.iter().skip(skip))
            as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
            .into()))
    }

    fn with_text_iter_rev(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(pager::Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>> {
        Ok(f((Box::new(self.text.iter().rev().skip(skip))
            as Box<dyn BackAndForthIterator<&Vec<TextFrag>>>)
            .into()))
    }

    fn num_lines(&self, _wrap_columns: Option<usize>) -> OrError<usize> {
        Ok(self.text.len())
    }

    fn num_columns(
        &self,
        first_line: usize,
        num_lines: usize,
        _wrap_columns: Option<usize>,
    ) -> OrError<usize> {
        Ok(self
            .text
            .iter()
            .skip(first_line)
            .take(num_lines)
            .map(|l| l.iter().map(|f| f.text.len()).sum())
            .max()
            .unwrap_or(0))
    }

    fn last_updated(&self) -> LastUpdated {
        LU::NeverUpdates
    }

    fn path(&self) -> Arc<Box<dyn Path>> {
        Arc::new(Box::new(path::RootPath))
    }
}
