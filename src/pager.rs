#[allow(unused_imports)]
use crate::import::*;

use crate::last_updated::LastUpdated;
use crate::path::Path;
use crate::text_frag::TextFrag;

use std::sync::Arc;
use std::time::Duration;

pub trait Pager
where
    Self: 'static,
{
    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()>;
    fn text(
        &mut self,
        first_line: usize,
        num_lines: usize,
        wrap_columns: Option<usize>,
    ) -> OrError<Vec<Vec<TextFrag>>>;
    fn with_text_iter(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>>;
    fn with_text_iter_rev(
        &mut self,
        skip: usize,
        f: Box<dyn Fn(Iter) -> Option<i64>>,
    ) -> OrError<Option<i64>>;
    fn num_lines(&self, wrap_columns: Option<usize>) -> OrError<usize>;
    fn num_columns(
        &self,
        first_line: usize,
        num_lines: usize,
        wrap_columns: Option<usize>,
    ) -> OrError<usize>;
    fn last_updated(&self) -> LastUpdated;
    fn path(&self) -> Arc<Box<dyn Path>>;
}

macro_rules! pager_accessors {
    () => {
        fn last_updated(&self) -> LastUpdated {
            self.last_updated
        }

        fn path(&self) -> Arc<Box<dyn Path>> {
            Arc::clone(&self.path)
        }
    };
}

pub trait BackAndForthIterator<T>:
    Iterator<Item = T> + DoubleEndedIterator + ExactSizeIterator
{
}

impl<I, F, A> BackAndForthIterator<Vec<TextFrag>> for std::iter::Map<I, F>
where
    I: Iterator<Item = A> + DoubleEndedIterator + ExactSizeIterator,
    F: FnMut(A) -> Vec<TextFrag>,
{
}

impl<'a, I> BackAndForthIterator<&'a Vec<TextFrag>> for std::iter::Skip<I> where
    I: Iterator<Item = &'a Vec<TextFrag>> + DoubleEndedIterator + ExactSizeIterator
{
}

impl<'a, I> BackAndForthIterator<&'a Vec<TextFrag>> for std::iter::Rev<I> where
    I: Iterator<Item = &'a Vec<TextFrag>> + DoubleEndedIterator + ExactSizeIterator
{
}

pub enum Iter<'a> {
    Borrowed(Box<dyn BackAndForthIterator<&'a Vec<TextFrag>> + 'a>),
    Boxed(Box<dyn BackAndForthIterator<Vec<TextFrag>> + 'a>),
}

use Iter::*;

impl<'a> From<Box<dyn BackAndForthIterator<Vec<TextFrag>> + 'a>> for Iter<'a> {
    fn from(x: Box<dyn BackAndForthIterator<Vec<TextFrag>> + 'a>) -> Self {
        Boxed(x)
    }
}

impl<'a> From<Box<dyn BackAndForthIterator<&'a Vec<TextFrag>> + 'a>> for Iter<'a> {
    fn from(x: Box<dyn BackAndForthIterator<&'a Vec<TextFrag>> + 'a>) -> Self {
        Borrowed(x)
    }
}

impl<'a> Iterator for Iter<'a> {
    type Item = RefTextFrags<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Borrowed(x) => x.next().map(RefTextFrags::Borrowed),
            Boxed(x) => x.next().map(RefTextFrags::Owned),
        }
    }
}

impl<'a> DoubleEndedIterator for Iter<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        match self {
            Borrowed(x) => x.next_back().map(RefTextFrags::Borrowed),
            Boxed(x) => x.next_back().map(RefTextFrags::Owned),
        }
    }
}

impl ExactSizeIterator for Iter<'_> {
    fn len(&self) -> usize {
        match self {
            Borrowed(x) => x.len(),
            Boxed(x) => x.len(),
        }
    }
}

impl<'a> BackAndForthIterator<RefTextFrags<'a>> for Iter<'a> {}

pub enum RefTextFrags<'a> {
    Borrowed(&'a Vec<TextFrag>),
    Owned(Vec<TextFrag>),
}

impl AsRef<Vec<TextFrag>> for RefTextFrags<'_> {
    fn as_ref(&self) -> &Vec<TextFrag> {
        use RefTextFrags::*;
        match self {
            Borrowed(x) => x,
            Owned(x) => x,
        }
    }
}
