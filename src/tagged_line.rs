#[allow(unused_imports)]
use crate::import::*;

#[derive(Clone, Debug)]
pub struct TaggedLine {
    pub src: Option<String>,
    pub max_src_len: usize,
    pub text: String,
}
