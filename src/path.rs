use crate::cluster_command as cc;
use crate::import::*;

use mashup::*;
use std::cmp::{Eq, PartialEq};
use std::fmt;
use std::hash::{Hash, Hasher};
use std::panic::{RefUnwindSafe, UnwindSafe};
use std::str::FromStr;

pub trait Path
where
    Self: fmt::Debug + fmt::Display + Send + Sync + UnwindSafe + RefUnwindSafe,
{
    fn context(&self) -> Option<&KContext> {
        None
    }

    fn namespace(&self) -> Option<&KNamespace> {
        None
    }

    fn namespace_resource_kind(&self) -> Option<NamespaceResourceKind> {
        None
    }

    fn cluster_resource_kind(&self) -> Option<ClusterResourceKind> {
        None
    }

    fn name(&self) -> Option<&KName> {
        None
    }

    fn parent(&self) -> Option<Box<dyn Path>>;

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>>;

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)>;

    fn display_kind(&self) -> DisplayKind;
}

impl Hash for Box<dyn Path> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.context().hash(state);
        self.namespace().hash(state);
        self.namespace_resource_kind().hash(state);
        self.cluster_resource_kind().hash(state);
        self.name().hash(state);
    }
}

impl PartialEq for Box<dyn Path> {
    fn eq(&self, other: &Self) -> bool {
        self.context() == other.context()
            && self.namespace() == other.namespace()
            && self.namespace_resource_kind() == other.namespace_resource_kind()
            && self.cluster_resource_kind() == other.cluster_resource_kind()
            && self.name() == other.name()
    }
}

impl Eq for Box<dyn Path> {}

impl FromStr for Box<dyn Path> {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> OrError<Self> {
        let parts: Vec<&str> = s.trim().split('/').collect();
        match parts.as_slice() {
            &[] => bail!("Path does not begin with '/': {}", s),
            &["", ""] => Ok(Box::new(RootPath)),
            &["", context] | &["", context, ""] => Ok(Box::new(ContextPath {
                context: context.into(),
            })),
            &["", context, "@cluster"] | &["", context, "@cluster", ""] => {
                Ok(Box::new(ClusterScopePath {
                    context: context.into(),
                }))
            }
            &["", context, namespace] | &["", context, namespace, ""] => {
                Ok(Box::new(NamespacePath {
                    context: context.into(),
                    namespace: namespace.into(),
                }))
            }
            &["", context, "@cluster", resource_kind]
            | &["", context, "@cluster", resource_kind, ""] => {
                Ok(Box::new(ClusterResourcePath {
                    context: context.into(),
                    resource_kind: resource_kind.parse()?,
                }))
            }
            &["", context, namespace, resource_kind]
            | &["", context, namespace, resource_kind, ""] => {
                Ok(Box::new(NamespaceResourcePath {
                    context: context.into(),
                    namespace: namespace.into(),
                    resource_kind: resource_kind.parse()?,
                }))
            }
            &["", context, "@cluster", resource_kind, name]
            | &["", context, "@cluster", resource_kind, name, ""] => {
                Ok(Box::new(ClusterNamePath {
                    context: context.into(),
                    resource_kind: resource_kind.parse()?,
                    name: name.into(),
                }))
            }
            &["", context, namespace, resource_kind, name]
            | &["", context, namespace, resource_kind, name, ""] => {
                Ok(Box::new(NamespaceNamePath {
                    context: context.into(),
                    namespace: namespace.into(),
                    resource_kind: resource_kind.parse()?,
                    name: name.into(),
                }))
            }
            &["", context, namespace, "pods", pod, "logs"]
            | &["", context, namespace, "pods", pod, "logs", ""] => {
                Ok(Box::new(AllLogsPath {
                    context: context.into(),
                    namespace: namespace.into(),
                    pod: pod.into(),
                }))
            }
            _ => bail!("Invalid path: {}", s),
        }
    }
}

pub enum DisplayKind {
    ContextTable,
    NamespaceTable,
    NamespaceResourceTable(NamespaceResourceKind),
    ClusterOverviewTableClusterScope,
    ClusterOverviewTableNamespaceScope,
    ClusterResourceTable(ClusterResourceKind),
    PrettyPager,
    PodDetailPager,
}

#[derive(Clone, Copy, Debug)]
pub struct RootPath;

impl fmt::Display for RootPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "/")
    }
}

impl Path for RootPath {
    fn parent(&self) -> Option<Box<dyn Path>> {
        None
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        Ok(Box::new(ContextPath {
            context: str.into(),
        }))
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        None
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::ContextTable
    }
}

#[derive(Clone, Debug)]
struct ContextPath {
    context: KContext,
}

impl fmt::Display for ContextPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "/{}", self.context)
    }
}

impl Path for ContextPath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(RootPath))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        match str {
            "@cluster" => Ok(Box::new(ClusterScopePath {
                context: self.context.clone(),
            })),
            str => Ok(Box::new(NamespacePath {
                namespace: str.into(),
                context: self.context.clone(),
            })),
        }
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((&self.context, cc::ListNamespaces::new().into()))
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::NamespaceTable
    }
}

#[derive(Clone, Debug)]
struct NamespacePath {
    context: KContext,
    namespace: KNamespace,
}

impl fmt::Display for NamespacePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "/{}/{}", self.context, self.namespace)
    }
}

impl Path for NamespacePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn namespace(&self) -> Option<&KNamespace> {
        Some(&self.namespace)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(ContextPath {
            context: self.context.clone(),
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        let resource_kind: NamespaceResourceKind = str.parse()?;
        Ok(Box::new(NamespaceResourcePath {
            resource_kind,
            namespace: self.namespace.clone(),
            context: self.context.clone(),
        }))
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        None
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::ClusterOverviewTableNamespaceScope
    }
}

#[derive(Clone, Debug)]
struct ClusterScopePath {
    context: KContext,
}

impl fmt::Display for ClusterScopePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "/{}/@cluster", self.context)
    }
}

impl Path for ClusterScopePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(ContextPath {
            context: self.context.clone(),
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        let resource_kind: ClusterResourceKind = str.parse()?;
        Ok(Box::new(ClusterResourcePath {
            resource_kind,
            context: self.context.clone(),
        }))
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        None
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::ClusterOverviewTableClusterScope
    }
}

macro_rules! replace_expr {
    ($_t:tt $sub:expr) => {
        $sub
    };
}

macro_rules! mk_resource_kind_just_types {
    ($enum_name:ident; $all_name:ident; $($singular:ident, $plural:ident, $str:literal);* ;) => {
    	#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
	pub enum $enum_name {
	    $($plural,)*
	}
	pub use $enum_name::*;

	const $all_name: [$enum_name; (0usize $(+ replace_expr!($plural 1usize))*)] =
	    [$($plural,)*];

	impl fmt::Display for $enum_name {
	    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
		    $($plural => write!(f, $str),)*
		}
	    }
	}

	impl FromStr for $enum_name {
	    type Err = anyhow::Error;

	    fn from_str(s: &str) -> OrError<Self> {
		match s {
		    $($str => Ok($plural),)*
		    s => bail!(
			"Invalid *ResourceKind: {} (expected one of {})",
			s,
			$all_name
			    .iter()
			    .map(|x| format!("{} ", x))
			    .collect::<String>()
		    ),
		}
	    }
	}
    }
}

// TODO-soon When concat_idents! stabilizes, use that instead of mashup.
macro_rules! mk_resource_kind {
    (namespace; $mod:ident; $enum_name:ident; $all_name:ident; $($singular:ident, $plural:ident, $str:literal);* ;) => {
        mod $mod {
            use super::*;

            mk_resource_kind_just_types!($enum_name; $all_name; $($singular, $plural, $str;)*);

	    mashup! {
	        $(
		    l["List" $plural] = List $plural;
		    g["Get" $singular] = Get $singular;
	        )*
	    }

	    l! {
	        impl From<($enum_name, &KNamespace)> for cc::Query {
		    fn from((resource_kind, namespace): ($enum_name, &KNamespace)) -> cc::Query {
		        match resource_kind {
			    $($plural => cc::"List" $plural::new(Some(namespace.clone())).into(),)*
		        }
		    }
	        }
	    }

	    g! {
	        impl From<($enum_name, &KName, &KNamespace, &KContext)> for cc::Query {
		    fn from((resource_kind, name, namespace, context): ($enum_name, &KName, &KNamespace, &KContext)) -> cc::Query {
		        match resource_kind {
			    $($plural => (cc::"Get" $singular {
			        name: name.clone(),
			        namespace: namespace.clone(),
			        context: context.clone()
			    }).into(),)*
		        }
		    }
	        }
	    }
        }

        pub use $mod::$enum_name;
        use $mod::$enum_name::*;
    };

    (cluster; $mod:ident; $enum_name:ident; $all_name:ident; $($singular:ident, $plural:ident, $str:literal);* ;) => {
        mod $mod {
            use super::*;

            mk_resource_kind_just_types!($enum_name; $all_name; $($singular, $plural, $str;)*);

	    mashup! {
	        $(
		    l["List" $plural] = List $plural;
		    g["Get" $singular] = Get $singular;
	        )*
	    }

	    l! {
	        impl From<$enum_name> for cc::Query {
		    fn from(resource_kind: $enum_name) -> cc::Query {
		        match resource_kind {
			    $($plural => cc::"List" $plural::new().into(),)*
		        }
		    }
	        }
	    }

	    g! {
	        impl From<($enum_name, &KName, &KContext)> for cc::Query {
		    fn from((resource_kind, name, context): ($enum_name, &KName, &KContext)) -> cc::Query {
		        match resource_kind {
			    $($plural => (cc::"Get" $singular {
			        name: name.clone(),
			        context: context.clone()
			    }).into(),)*
		        }
		    }
	        }
	    }
        }

        pub use $mod::$enum_name;
    }
}

mk_resource_kind!(
    namespace;
    namespace_resource_kinds;
    NamespaceResourceKind; NAMESPACE_RESOURCE_KINDS;
    Pod, Pods, "pods";
    Service, Services, "services";
    Ingress, Ingresses, "ingresses";
    Deployment, Deployments, "deployments";
    DaemonSet, DaemonSets, "daemonsets";
    StatefulSet, StatefulSets, "statefulsets";
    ServiceAccount, ServiceAccounts, "serviceaccounts";
    RoleBinding, RoleBindings, "rolebindings";
    Role, Roles, "roles";
    ConfigMap, ConfigMaps, "configmaps";
    PersistentVolumeClaim, PersistentVolumeClaims, "persistentvolumeclaims";
    Secret, Secrets, "secrets";
);

mk_resource_kind!(
    cluster;
    cluster_resource_kinds;
    ClusterResourceKind; CLUSTER_RESOURCE_KINDS;
    Node, Nodes, "nodes";
    PersistentVolume, PersistentVolumes, "persistentvolumes";
);

#[derive(Clone, Debug, Hash)]
struct NamespaceResourcePath {
    resource_kind: NamespaceResourceKind,
    namespace: KNamespace,
    context: KContext,
}

impl fmt::Display for NamespaceResourcePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "/{}/{}/{}",
            self.context, self.namespace, self.resource_kind
        )
    }
}

impl Path for NamespaceResourcePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn namespace(&self) -> Option<&KNamespace> {
        Some(&self.namespace)
    }

    fn namespace_resource_kind(&self) -> Option<NamespaceResourceKind> {
        Some(self.resource_kind)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(NamespacePath {
            namespace: self.namespace.clone(),
            context: self.context.clone(),
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        Ok(Box::new(NamespaceNamePath {
            name: str.into(),
            namespace: self.namespace.clone(),
            resource_kind: self.resource_kind,
            context: self.context.clone(),
        }))
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((&self.context, (self.resource_kind, &self.namespace).into()))
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::NamespaceResourceTable(self.resource_kind)
    }
}

#[derive(Clone, Debug, Hash)]
struct ClusterResourcePath {
    resource_kind: ClusterResourceKind,
    context: KContext,
}

impl fmt::Display for ClusterResourcePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "/{}/@cluster/{}", self.context, self.resource_kind)
    }
}

impl Path for ClusterResourcePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn cluster_resource_kind(&self) -> Option<ClusterResourceKind> {
        Some(self.resource_kind)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(ClusterScopePath {
            context: self.context.clone(),
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        Ok(Box::new(ClusterNamePath {
            name: str.into(),
            resource_kind: self.resource_kind,
            context: self.context.clone(),
        }))
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((&self.context, self.resource_kind.into()))
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::ClusterResourceTable(self.resource_kind)
    }
}

#[derive(Clone, Debug)]
struct NamespaceNamePath {
    name: KName,
    namespace: KNamespace,
    resource_kind: NamespaceResourceKind,
    context: KContext,
}

impl fmt::Display for NamespaceNamePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "/{}/{}/{}/{}",
            self.context, self.namespace, self.resource_kind, self.name
        )
    }
}

impl Path for NamespaceNamePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn namespace(&self) -> Option<&KNamespace> {
        Some(&self.namespace)
    }

    fn namespace_resource_kind(&self) -> Option<NamespaceResourceKind> {
        Some(self.resource_kind)
    }

    fn name(&self) -> Option<&KName> {
        Some(&self.name)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(NamespaceResourcePath {
            context: self.context.clone(),
            namespace: self.namespace.clone(),
            resource_kind: self.resource_kind,
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        match (self.resource_kind, str) {
            (Pods, "logs") => Ok(Box::new(AllLogsPath {
                pod: self.name.clone(),
                namespace: self.namespace.clone(),
                context: self.context.clone(),
            })),
            _ => bail!("Cannot extend NamespaceNamePath path {} with {}", self, str),
        }
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((
            &self.context,
            (
                self.resource_kind,
                &self.name,
                &self.namespace,
                &self.context,
            )
                .into(),
        ))
    }

    fn display_kind(&self) -> DisplayKind {
        match self.resource_kind {
            Pods => DisplayKind::PodDetailPager,
            _ => DisplayKind::PrettyPager,
        }
    }
}

#[derive(Clone, Debug)]
struct ClusterNamePath {
    name: KName,
    resource_kind: ClusterResourceKind,
    context: KContext,
}

impl fmt::Display for ClusterNamePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "/{}/@cluster/{}/{}",
            self.context, self.resource_kind, self.name
        )
    }
}

impl Path for ClusterNamePath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn cluster_resource_kind(&self) -> Option<ClusterResourceKind> {
        Some(self.resource_kind)
    }

    fn name(&self) -> Option<&KName> {
        Some(&self.name)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(ClusterResourcePath {
            context: self.context.clone(),
            resource_kind: self.resource_kind,
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        bail!("Cannot extend ClusterNamePath path {} with {}", self, str);
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((
            &self.context,
            (self.resource_kind, &self.name, &self.context).into(),
        ))
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::PrettyPager
    }
}

#[derive(Clone, Debug)]
pub struct AllLogsPath {
    pod: KName,
    namespace: KNamespace,
    context: KContext,
}

impl AllLogsPath {
    pub fn new(pod: &KName, namespace: &KNamespace, context: &KContext) -> Self {
        Self {
            pod: pod.clone(),
            namespace: namespace.clone(),
            context: context.clone(),
        }
    }
}

impl fmt::Display for AllLogsPath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "/{}/{}/pods/{}/logs",
            self.context, self.namespace, self.pod
        )
    }
}

impl Path for AllLogsPath {
    fn context(&self) -> Option<&KContext> {
        Some(&self.context)
    }

    fn namespace(&self) -> Option<&KNamespace> {
        Some(&self.namespace)
    }

    fn namespace_resource_kind(&self) -> Option<NamespaceResourceKind> {
        Some(Pods)
    }

    fn name(&self) -> Option<&KName> {
        Some(&self.pod)
    }

    fn parent(&self) -> Option<Box<dyn Path>> {
        Some(Box::new(NamespaceNamePath {
            context: self.context.clone(),
            namespace: self.namespace.clone(),
            resource_kind: Pods,
            name: self.pod.clone(),
        }))
    }

    fn extend(&self, str: &str) -> OrError<Box<dyn Path>> {
        bail!("Cannot extend AllLogsPath path {} with {}", self, str)
    }

    fn cluster_query(&self) -> Option<(&KContext, cc::Query)> {
        Some((
            &self.context,
            cc::StreamAllLogs::new(&self.pod, &self.namespace, &self.context).into(),
        ))
    }

    fn display_kind(&self) -> DisplayKind {
        DisplayKind::PrettyPager
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn mk_path(parts: &[&str]) -> Box<dyn Path> {
        parts
            .iter()
            .fold(Box::new(RootPath), |p, x| p.extend(x.clone()).unwrap())
    }

    #[test]
    fn serialization() {
        assert_eq!(RootPath.to_string(), "/");
        assert_eq!(mk_path(&["hel2"]).to_string(), "/hel2");
        assert_eq!(mk_path(&["hel2", "default"]).to_string(), "/hel2/default");
        assert_eq!(mk_path(&["hel2", "@cluster"]).to_string(), "/hel2/@cluster");
        assert_eq!(
            mk_path(&["hel2", "default", "pods"]).to_string(),
            "/hel2/default/pods"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "pods", "pod-0"]).to_string(),
            "/hel2/default/pods/pod-0"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "services"]).to_string(),
            "/hel2/default/services"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "services", "svc-0"]).to_string(),
            "/hel2/default/services/svc-0"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "ingresses"]).to_string(),
            "/hel2/default/ingresses"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "ingresses", "ing-0"]).to_string(),
            "/hel2/default/ingresses/ing-0"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "deployments", "d-0"]).to_string(),
            "/hel2/default/deployments/d-0"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "daemonsets", "d-0"]).to_string(),
            "/hel2/default/daemonsets/d-0"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "statefulsets", "ss-0"]).to_string(),
            "/hel2/default/statefulsets/ss-0"
        );
        assert_eq!(
            mk_path(&["hel2", "@cluster", "persistentvolumes"]).to_string(),
            "/hel2/@cluster/persistentvolumes"
        );
        assert_eq!(
            mk_path(&["hel2", "@cluster", "nodes"]).to_string(),
            "/hel2/@cluster/nodes"
        );
        assert_eq!(
            mk_path(&["hel2", "default", "pods", "pod-0", "logs"]).to_string(),
            "/hel2/default/pods/pod-0/logs"
        );
    }

    #[test]
    fn deserialization() {
        let p = |str: &str| str.parse::<Box<dyn Path>>().unwrap();
        assert_eq!(&p("/"), &mk_path(&[]));
        assert_eq!(&p("/hel2"), &mk_path(&["hel2"]));
        assert_eq!(&p("/hel2/default"), &mk_path(&["hel2", "default"]));
        assert_eq!(&p("/hel2/@cluster"), &mk_path(&["hel2", "@cluster"]));
        assert_eq!(
            &p("/hel2/default/pods"),
            &mk_path(&["hel2", "default", "pods"])
        );
        assert_eq!(
            &p("/hel2/default/pods/pod-0"),
            &mk_path(&["hel2", "default", "pods", "pod-0"])
        );
        assert_eq!(
            &p("/hel2/default/services"),
            &mk_path(&["hel2", "default", "services"])
        );
        assert_eq!(
            &p("/hel2/default/services/svc-0"),
            &mk_path(&["hel2", "default", "services", "svc-0"])
        );
        assert_eq!(
            &p("/hel2/default/ingresses"),
            &mk_path(&["hel2", "default", "ingresses"])
        );
        assert_eq!(
            &p("/hel2/default/ingresses/ing-0"),
            &mk_path(&["hel2", "default", "ingresses", "ing-0"])
        );
        assert_eq!(
            &p("/hel2/default/deployments"),
            &mk_path(&["hel2", "default", "deployments"])
        );
        assert_eq!(
            &p("/hel2/default/deployments/d-0"),
            &mk_path(&["hel2", "default", "deployments", "d-0"])
        );
        assert_eq!(
            &p("/hel2/default/statefulsets"),
            &mk_path(&["hel2", "default", "statefulsets"])
        );
        assert_eq!(
            &p("/hel2/default/statefulsets/ss-0"),
            &mk_path(&["hel2", "default", "statefulsets", "ss-0"])
        );
        assert_eq!(
            &p("/hel2/@cluster/persistentvolumes/pv-0"),
            &mk_path(&["hel2", "@cluster", "persistentvolumes", "pv-0"])
        );
        assert_eq!(
            &p("/hel2/@cluster/nodes/hel-qws-app1"),
            &mk_path(&["hel2", "@cluster", "nodes", "hel-qws-app1"])
        );
        assert_eq!(
            &p("/hel2/default/pods/pod-0/logs"),
            &mk_path(&["hel2", "default", "pods", "pod-0", "logs"])
        );
    }

    #[test]
    fn parent() {
        let p = |str: &str| str.parse::<Box<dyn Path>>().unwrap().parent();
        assert_eq!(p("/"), None);
        assert_eq!(p("/hel2"), Some(mk_path(&[])));
        assert_eq!(p("/hel2/default"), Some(mk_path(&["hel2"])));
        assert_eq!(p("/hel2/@cluster"), Some(mk_path(&["hel2"])));
        assert_eq!(p("/hel2/default/pods"), Some(mk_path(&["hel2", "default"])));
        assert_eq!(
            p("/hel2/@cluster/persistentvolumes"),
            Some(mk_path(&["hel2", "@cluster"]))
        );
        assert_eq!(
            p("/hel2/default/pods/pod-0"),
            Some(mk_path(&["hel2", "default", "pods"]))
        );
        assert_eq!(
            p("/hel2/default/services"),
            Some(mk_path(&["hel2", "default"]))
        );
        assert_eq!(
            p("/hel2/default/services/svc-0"),
            Some(mk_path(&["hel2", "default", "services"]))
        );
        assert_eq!(
            p("/hel2/default/ingresses"),
            Some(mk_path(&["hel2", "default"]))
        );
        assert_eq!(
            p("/hel2/default/ingresses/ing-0"),
            Some(mk_path(&["hel2", "default", "ingresses"]))
        );
        assert_eq!(
            p("/hel2/default/deployments"),
            Some(mk_path(&["hel2", "default"]))
        );
        assert_eq!(
            p("/hel2/default/deployments/d-0"),
            Some(mk_path(&["hel2", "default", "deployments"]))
        );
        assert_eq!(
            p("/hel2/default/statefulsets"),
            Some(mk_path(&["hel2", "default"]))
        );
        assert_eq!(
            p("/hel2/default/statefulsets/ss-0"),
            Some(mk_path(&["hel2", "default", "statefulsets"]))
        );
        assert_eq!(
            p("/hel2/@cluster/persistentvolumes/pv-0"),
            Some(mk_path(&["hel2", "@cluster", "persistentvolumes"]))
        );
        assert_eq!(
            p("/hel2/@cluster/nodes/hel-qws-app1"),
            Some(mk_path(&["hel2", "@cluster", "nodes"]))
        );
        assert_eq!(
            p("/hel2/default/pods/pod-0/logs"),
            Some(mk_path(&["hel2", "default", "pods", "pod-0"]))
        );
    }
}
