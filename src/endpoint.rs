use crate::crippled_certificate_verifier::CrippledCertificateVerifier;
use crate::dummy_certificate_verifier::DummyCertificateVerifier;
use crate::fixed_resolver::FixedResolver;
use crate::import::*;

use std::sync::Arc;
use ureq::Agent;
use url::Url;

#[derive(Debug)]
pub struct Endpoint {
    pub context: KContext,
    server: Url,
    token: Option<Arc<Vec<u8>>>,
    client: Agent,
}

impl Endpoint {
    pub fn try_from_kube_context(context: &crate::kube_config::Context) -> OrError<Self> {
        use rustls::{internal::pemfile, ClientConfig, RootCertStore};
        use std::io::Cursor;
        use std::net::SocketAddr;
        let mut tls_config = ClientConfig::new();

        if let Some(cluster_cert) = context.cluster_certificate_authority_data() {
            let mut root_store = RootCertStore::empty();
            let cluster_certs =
                pemfile::certs(&mut Cursor::new(cluster_cert?.as_ref())).map_err(|()| {
                    anyhow!("Error getting certificates from cluster.certificate-authority-data")
                })?;
            for c in &cluster_certs {
                root_store.add(c)?;
            }
            tls_config.root_store = root_store;
        }
        if let Some(client_cert_and_key) = context.client_certificate_and_key() {
            let (client_cert, client_key) = client_cert_and_key?;
            let cert_chain =
                pemfile::certs(&mut Cursor::new(&*client_cert)).map_err(|()| {
                    anyhow!("Error getting certificate from client-certificate")
                })?;
            let keys_der = pemfile::rsa_private_keys(&mut Cursor::new(&*client_key))
                .map_err(|()| anyhow!("Error getting key from client-key"))?;
            match keys_der.len() {
                n if n < 1 => bail!("Found no keys in client-key"),
                n if n > 1 => info!("Found multiple keys in client-key; using the first"),
                _ => (),
            }
            tls_config.set_single_client_cert(cert_chain, keys_der[0].clone())?;
        }
        let token = if let Some(token) = context.client_token() {
            Some(token?)
        } else {
            None
        };
        let mut server = Url::parse(context.server())?;
        let workaround_webpki_does_not_support_ip_address: Option<SocketAddr> = {
            use std::net::{SocketAddrV4, SocketAddrV6};
            use url::Host::*;
            let port = server.port().unwrap_or_else(|| match server.scheme() {
                "https" => 443,
                _ => 80, // fingers crossed
            });
            match server.host() {
                None | Some(Domain(_)) => None,
                Some(Ipv4(ipv4)) => Some(SocketAddrV4::new(ipv4, port).into()),
                Some(Ipv6(ipv6)) => Some(SocketAddrV6::new(ipv6, port, 0, 0).into()),
            }
        };

        if let Some(true) = context.insecure_skip_tls_verify() {
            tls_config
                .dangerous()
                .set_certificate_verifier(Arc::new(DummyCertificateVerifier));
        } else if workaround_webpki_does_not_support_ip_address.is_some() {
            tls_config
                .dangerous()
                .set_certificate_verifier(Arc::new(CrippledCertificateVerifier));
        }
        let mut client = ureq::builder().tls_config(Arc::new(tls_config));
        if let Some(ref addr) = workaround_webpki_does_not_support_ip_address {
            // This is a workaround for https://github.com/briansmith/webpki/issues/54
            //
            // The basic problem is that webpki can't parse IP
            // addresses as `DnsNameRef`.  This parsing in rustls
            // happens early on in the pipeline, and there's no easy
            // way to disable it.  Practically, this means that
            // connecting to `https://1.1.1.1` will fail with
            // `InvalidDNSNameError`.
            //
            // There are several pieces to the workaround:
            // - we rewrite the server URL to a dummy hostname,
            // - we configure a DNS resolver that ignores the hostname
            // and always returns our server's IP address,
            // - we configure a certificate verifier that doesn't
            // verify the hostname (see the `set_certificate_verifier`
            // call above).
            info!(
                "Enabling webpki-does-not-support-ip-address workaround for {}",
                server
            );
            client = client.resolver(FixedResolver::new(*addr)?);
            server.set_host(Some(
                "dummy-host-workaround-webpki-does-not-support-ip-address",
            ))?;
        }
        Ok(Endpoint {
            context: context.name().clone(),
            server,
            token,
            client: client.build(),
        })
    }

    pub fn request(&self, method: &str, path: &str) -> ureq::Request {
        let mut url = self.server.clone();
        url.set_path(path);
        let req = self.client.request_url(method, &url);
        match self.token {
            None => req,
            Some(ref token) => {
                // https://kubernetes.io/docs/reference/access-authn-authz/authentication/
                req.set(
                    "Authorization",
                    &format!("Bearer {}", String::from_utf8_lossy(token)),
                )
            }
        }
    }
}
