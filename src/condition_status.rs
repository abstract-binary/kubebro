#[allow(unused_imports)]
use crate::import::*;

/// https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle#pod-conditions
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ConditionStatus {
    True,
    False,
    Unknown,
}
