#[allow(unused_imports)]
use crate::import::*;

use tui::style::Color;

// https://jonasjacek.github.io/colors/
// https://robotmoon.com/256-colors/
pub const ALTERNATE_ROW_BG: Color = Color::Indexed(237);
pub const CELL_HEALTHY_FG: Color = Color::Green;
pub const CELL_QUESTIONABLE_FG: Color = Color::Yellow;
pub const CELL_UNHEALTHY_FG: Color = Color::Red;
pub const ERROR_TEXT_FG: Color = Color::Red;
pub const HELP_EMPHASIS_FG: Color = Color::LightGreen;
pub const HELP_SUPER_EMPHASIS_FG: Color = Color::Red;
pub const HELP_TITLE_FG: Color = Color::LightBlue;
pub const JSON_KEY_FG: Color = Color::Cyan;
pub const JSON_STRING_FG: Color = Color::Green;
pub const OPTIONS_BG: Color = Color::Blue;
pub const OPTIONS_FG: Color = Color::White;
pub const SELECTED_TEXT_FG: Color = Color::LightYellow;
pub const SPINNER_COLOR: Color = Color::DarkGray;
pub const TABLE_HEADER_BG: Color = Color::Blue;
pub const TEXT_COLOR: Color = Color::White;
pub const WARNING_TEXT_FG: Color = Color::Yellow;
pub const HELP_LINE_TEXT_FG: Color = Color::Black;
pub const HELP_LINE_BG: Color = Color::Gray;
pub const OVERFLOW_CELL_BG: Color = Color::White;
pub const OVERFLOW_CELL_TEXT_FG: Color = Color::Black;
pub const PAGER_SRC_FG: &[Color] = &[
    Color::LightBlue,
    Color::LightGreen,
    Color::LightRed,
    Color::Yellow,
    Color::Green,
    Color::Red,
];
