use crate::cache::Cache;
use crate::explorer::Explorer;
use crate::help::Help;
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::kube_config::KubeConfig;
use crate::path::Path;
use crate::prompt::Prompt;
use crate::screen_logger::{Notice, ScreenLogger};
use crate::window::Window;
use crate::window_result::{WindowResult, WindowResult as WR};

use std::os::unix::io::RawFd;
use std::process::Child;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration, Instant};
use tui::{
    backend::Backend,
    buffer::Buffer,
    layout::Rect,
    style::Style,
    terminal::{Frame, Terminal},
    text::{Span, Spans},
    widgets::{Paragraph, Widget},
};

const ZERO_SECS: Duration = Duration::from_secs(0);
const TICK: Duration = Duration::from_micros(16666); // 60 fps

enum Screen {
    /// displaying kubernetes objects
    Explore(Box<dyn Window>),
    /// displaying keybindings and other help
    Help(Box<dyn Window>),
    /// show the log with `less`
    Log(Child),
}

use Screen::*;

struct Ui {
    kube_config: Arc<Box<dyn KubeConfig>>,
    /// What is currently shown in the UI
    screen: Screen,
    /// A stack of windows that are currently hidden.  These are not
    /// tick'd and their key bindings are ignored.
    window_stack: Vec<Box<dyn Window>>,
    /// A stack of overlay windows that are drawn on top of `screen`.
    /// They are tick'd.  The key bindings of the top-most overlay
    /// window are used for the UI.
    overlay_window_stack: Vec<Box<dyn Window>>,
    shutting_down: bool,
    tick: u64,
    cache: Arc<Cache>,
    screen_logger: ScreenLogger,
    unackd_notices: Vec<Notice>,
    pin: bool,
}

impl Ui {
    fn push_screen(&mut self, screen: Screen) {
        use std::mem::replace;
        match replace(&mut self.screen, screen) {
            Explore(window) => self.window_stack.push(window),
            Log(_) | Help(_) => (),
        }
    }

    fn pop_screen(&mut self) {
        match self.window_stack.pop() {
            None => {
                if self.pin {
                    info!("No window to pop")
                } else {
                    let parent_path = match self.screen {
                        Help(_) | Log(_) => None,
                        Explore(ref window) => {
                            window.path().and_then(|p| p.parent().map(Arc::new))
                        }
                    };
                    if let Some(parent_path) = parent_path {
                        // TODO-soon In the new explorer window,
                        // we should select the child path that
                        // was just exited.
                        match Explorer::new(
                            Arc::clone(&parent_path),
                            Arc::clone(&self.kube_config),
                            Arc::clone(&self.cache),
                        ) {
                            Err(error) => {
                                error!("Failed to navigate to {}: {}", parent_path, error)
                            }
                            Ok(explorer) => {
                                self.screen = Screen::Explore(Box::new(explorer))
                            }
                        }
                    }
                }
            }
            Some(screen) => self.screen = Explore(screen),
        }
    }
}

pub fn run<B, F>(
    terminal: Arc<Mutex<Terminal<B>>>,
    poll_next_key: &mut F,
    kube_config: Arc<Box<dyn KubeConfig>>,
    path: Arc<Box<dyn Path>>,
    pin: bool,
    log_fd: RawFd,
    screen_logger: ScreenLogger,
) -> OrError<()>
where
    B: Backend,
    F: FnMut() -> OrError<Option<Key>>,
{
    let cache = Arc::new(Cache::new(Arc::clone(&kube_config))?);
    let mut ui = Ui {
        screen: Screen::Explore(Box::new(Explorer::new(
            path,
            Arc::clone(&kube_config),
            Arc::clone(&cache),
        )?)),
        kube_config,
        window_stack: vec![],
        overlay_window_stack: vec![],
        shutting_down: false,
        tick: 0,
        cache,
        screen_logger,
        unackd_notices: vec![],
        pin,
    };
    let mut realtime = Instant::now();
    let mut faketime = Instant::now();
    while !ui.shutting_down {
        ui.tick += 1;
        progress_time(&mut realtime, &mut faketime);
        while realtime > faketime {
            let mut terminal = terminal
                .lock()
                .map_err(|err| anyhow!("Terminal mutex poisoned: {}", err))?;
            draw(&mut ui, &mut terminal)?;

            match ui.screen {
                Explore(_) | Help(_) => {
                    pull_logger_notices(&mut ui.unackd_notices, &ui.screen_logger);
                    handle_term_events(&mut ui, &mut terminal, poll_next_key, &log_fd)
                        .context("ui::handle_term_events")?;
                    match ui.screen {
                        Explore(ref mut window) | Help(ref mut window) => window.tick(),
                        Log(_) => {
                            // The `handle_term_events` above replaced
                            // the ui.screen.
                        }
                    };
                }
                Log(_) => handle_child_death(&mut ui, &mut terminal)?,
            }

            faketime += TICK;
        }
    }
    Ok(())
}

fn progress_time(realtime: &mut Instant, faketime: &mut Instant) {
    *realtime = Instant::now();
    let sleep_duration = faketime
        .checked_duration_since(*realtime)
        .unwrap_or(ZERO_SECS);
    thread::sleep(sleep_duration);
    *realtime = Instant::now();
}

fn draw<B>(ui: &mut Ui, terminal: &mut Terminal<B>) -> OrError<()>
where
    B: Backend,
{
    match ui.screen {
        Help(ref mut window) | Explore(ref mut window) => draw_window(
            window,
            &mut ui.overlay_window_stack,
            terminal,
            ui.tick,
            &ui.unackd_notices,
        )?,
        Log(_) => (),
    }
    Ok(())
}

struct WindowWidget<'a> {
    window: &'a mut dyn Window,
    tick: u64,
}

impl<'a> WindowWidget<'a> {
    fn new(window: &'a mut Box<dyn Window>, tick: u64) -> Self {
        Self {
            window: &mut **window,
            tick,
        }
    }
}

impl<'a> Widget for WindowWidget<'a> {
    fn render(self, rect: Rect, buffer: &mut Buffer) {
        self.window.render(buffer, self.tick, rect)
    }
}

fn draw_window<B>(
    window: &mut Box<dyn Window>,
    overlay_window_stack: &mut Vec<Box<dyn Window>>,
    terminal: &mut Terminal<B>,
    tick: u64,
    unackd_notices: &[Notice],
) -> OrError<()>
where
    B: Backend,
{
    terminal.draw(|frame| {
        let (notices_space, errors_height) = notices_space(unackd_notices, frame.size());
        let prompt_height = if window.prompt().is_some() { 1 } else { 0 };
        let rects = {
            use tui::layout::{Constraint::*, Layout};
            // - A :: centerpiece with room for path, table header, and a row
            // - B :: help line
            // - C :: unack'd errors
            // - D :: prompt_leight
            Layout::default()
                .constraints(
                    [
                        Min(3),                // A
                        Length(1),             // B
                        Length(errors_height), // C
                        Length(prompt_height), // D
                    ]
                    .as_ref(),
                )
                .split(frame.size())
        };
        frame.render_widget(WindowWidget::new(window, tick), rects[0]);
        overlay_window_stack
            .iter_mut()
            .for_each(|w| frame.render_widget(WindowWidget::new(w, tick), rects[0]));
        render_key_binds(frame, rects[1], window.key_binds());
        if let Some(notices_space) = notices_space {
            frame.render_widget(notices_space, rects[2])
        };
        if let Some(prompt) = window.prompt() {
            render_prompt(frame, rects[3], prompt);
        }
    })?;
    Ok(())
}

fn notices_space(
    unackd_notices: &[Notice],
    frame_size: Rect,
) -> (Option<Paragraph>, u16) {
    let (widget, height) = if !unackd_notices.is_empty() {
        let max_width = frame_size.width as usize;
        let to_skip = {
            use std::cmp::{max, min};
            let max_errors = max(1, (frame_size.height / 2) as usize);
            let lines_to_show = min(unackd_notices.len(), max_errors);
            if unackd_notices.len() > lines_to_show {
                unackd_notices.len() - lines_to_show
            } else {
                0
            }
        };
        let mut lines: Vec<Spans> = unackd_notices
            .iter()
            .skip(to_skip)
            .map(|notice| {
                let (mut str, color) = if notice.error {
                    (format!("Error: {}", notice.text), palette::ERROR_TEXT_FG)
                } else {
                    (format!("Info: {}", notice.text), palette::WARNING_TEXT_FG)
                };
                if str.len() > max_width {
                    str.truncate(max_width - 3);
                    str.push_str("...");
                };
                Spans::from(vec![Span::styled(str, Style::default().fg(color))])
            })
            .collect();
        lines.push(Spans::from(vec![Span::styled(
            "(C-l for details, SPACE to clear)",
            Style::default(),
        )]));
        let height = lines.len() as u16;
        (Some(Paragraph::new(lines)), height)
    } else {
        (None, 0)
    };
    (widget, height)
}

fn render_key_binds<B>(frame: &mut Frame<B>, rect: Rect, key_binds: Vec<KeyBindDesc>)
where
    B: Backend,
{
    use tui::style::Modifier;
    let mut pairs: Vec<(String, String)> = key_binds
        .into_iter()
        .chain(common_key_binds())
        .filter_map(|kbd| {
            if kbd.advanced {
                None
            } else {
                Some((kbd.keys.to_string(), kbd.short_desc.to_string()))
            }
        })
        .collect();
    pairs.sort_by(|(key1, _), (key2, _)| key1.cmp(key2));
    let help_line = Paragraph::new(Spans::from(
        pairs
            .into_iter()
            .flat_map(|(key, desc)| {
                vec![
                    Span::styled(
                        key,
                        Style::default()
                            .fg(palette::HELP_LINE_TEXT_FG)
                            .add_modifier(Modifier::BOLD),
                    ),
                    Span::styled(
                        desc,
                        Style::default()
                            .bg(palette::OPTIONS_BG)
                            .fg(palette::OPTIONS_FG),
                    ),
                    Span::raw(" "),
                ]
            })
            .collect::<Vec<Span>>(),
    ))
    .style(Style::default().bg(palette::HELP_LINE_BG));
    frame.render_widget(help_line, rect);
}

fn render_prompt<B>(frame: &mut Frame<B>, rect: Rect, prompt: &Prompt)
where
    B: Backend,
{
    use tui::style::Modifier;
    let prompt_line = Paragraph::new(Spans::from(vec![
        Span::styled(
            &prompt.before_text,
            Style::default().add_modifier(Modifier::BOLD),
        ),
        Span::raw(" "),
        Span::raw(&prompt.text),
    ]));
    frame.render_widget(prompt_line, rect);
}

fn handle_child_death<B>(ui: &mut Ui, terminal: &mut Terminal<B>) -> OrError<()>
where
    B: Backend,
{
    match ui.screen {
        Explore(_) | Help(_) => (),
        Log(ref mut child) => match child.try_wait()? {
            None => (),
            Some(_exit_code) => {
                info!("Log process exited");
                ui.pop_screen();
                terminal.clear()?;
            }
        },
    }
    Ok(())
}

fn handle_term_events<B, F>(
    ui: &mut Ui,
    terminal: &mut Terminal<B>,
    poll_next_key: &mut F,
    log_fd: &RawFd,
) -> OrError<()>
where
    B: Backend,
    F: FnMut() -> OrError<Option<Key>>,
{
    while let Some(key) = poll_next_key()? {
        // Handle global keys
        debug!("Handling key event {}", key);
        match key.to_string().as_ref() {
            "C-c" => {
                info!("Shutting down");
                ui.shutting_down = true;
                return Ok(());
            }
            _ => {
                #[allow(clippy::single_match)]
                match key.to_string().as_ref() {
                    "Spc" => ui.unackd_notices.clear(),
                    _ => (),
                };
                // Handle screen-specific keys
                let handled: bool = match ui.overlay_window_stack.last_mut() {
                    Some(ref mut window) => {
                        let hr = window.handle_key_event(key);
                        handle_handle_result(HHR::OverlayWindow, ui, terminal, hr)?
                    }
                    None => match ui.screen {
                        Help(ref mut window) | Explore(ref mut window) => {
                            let hr = window.handle_key_event(key);
                            handle_handle_result(HHR::MainWindow, ui, terminal, hr)?
                        }
                        Log(_) => {
                            // Shouldn't happen because input capture
                            // should be paused
                            true
                        }
                    },
                };
                if !handled {
                    match key.to_string().as_ref() {
                        "C-h" | "?" => {
                            let key_binds = match ui.screen {
                                Help(ref mut window) | Explore(ref mut window) => {
                                    window.key_binds()
                                }
                                Log(_) => vec![],
                            };
                            ui.push_screen(Screen::Help(Box::new(Help::new(
                                key_binds,
                                common_key_binds(),
                            ))))
                        }
                        "C-l" => {
                            use std::process::Command;
                            terminal.clear()?;
                            let pid = unsafe { libc::getpid() };
                            let child = Command::new("less")
                                .args(&[
                                    "+G", // start at the end
                                    &format!("/proc/{}/fd/{}", pid, log_fd),
                                ])
                                .spawn()?;
                            ui.push_screen(Screen::Log(child))
                        }
                        _ => (),
                    }
                }
            }
        }
    }
    Ok(())
}

fn common_key_binds() -> Vec<KeyBindDesc> {
    vec![
        key_bind_adv!(["C-c"], "Exit", "Exit kubebro"),
        key_bind!(["C-h", "?"], "Help", "Show this help text"),
        key_bind_adv!(["C-l"], "Log", "Show log"),
        key_bind_adv!(["Spc"], "Clear notices", "Clear notices"),
    ]
}

enum HHR {
    MainWindow,
    OverlayWindow,
}

fn handle_handle_result<B>(
    which_window: HHR,
    ui: &mut Ui,
    terminal: &mut Terminal<B>,
    res: WindowResult,
) -> OrError<bool>
where
    B: Backend,
{
    match res {
        WR::Handled => Ok(true),
        WR::Goto(path) => {
            ui.push_screen(Screen::Explore(Box::new(Explorer::new(
                path,
                Arc::clone(&ui.kube_config),
                Arc::clone(&ui.cache),
            )?)));
            terminal.clear()?;
            Ok(true)
        }
        WR::NotHandled => Ok(false),
        WR::PushOverlayWindow(window) => {
            ui.overlay_window_stack.push(window);
            terminal.clear()?;
            Ok(true)
        }
        WR::CloseSelf => {
            match which_window {
                HHR::MainWindow => ui.pop_screen(),
                HHR::OverlayWindow => {
                    let _: Option<_> = ui.overlay_window_stack.pop();
                }
            }
            terminal.clear()?;
            Ok(true)
        }
    }
}

fn pull_logger_notices(notices: &mut Vec<Notice>, screen_logger: &ScreenLogger) {
    notices.append(&mut screen_logger.take_notices());
}
