//! The navigation screen that make up the center of `kubebro`'s UI.
//! These have a path and contain `Table`s.

use crate::cache::Cache;
use crate::cluster_data as cd;
use crate::cluster_overview_table::ClusterOverviewTable;
use crate::contexts_table::ContextsTable;
use crate::explorer_pager::ExplorerPager;
use crate::explorer_table::ExplorerTable;
use crate::explorer_view::ExplorerView;
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::kube_config::KubeConfig;
use crate::last_updated::{LastUpdated, LastUpdated as LU};
use crate::path::{self, DisplayKind, Path};
use crate::pod_detail_pager::PodDetailPager;
use crate::pretty_pager::PrettyPager;
use crate::prompt::Prompt;
use crate::resource_table::ResourceTable;
use crate::window::Window;
use crate::window_result::WindowResult;

use std::sync::Arc;
use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect, widgets::Widget};

const TWO_SECS: Duration = Duration::from_secs(2);
const TEN_SECS: Duration = Duration::from_secs(10);

pub struct Explorer {
    path: Arc<Box<dyn Path>>,
    main_view: Box<dyn ExplorerView>,
    refresh_rate: Duration,
}

impl Explorer {
    pub fn new(
        path: Arc<Box<dyn Path>>,
        kube_config: Arc<Box<dyn KubeConfig>>,
        cache: Arc<Cache>,
    ) -> OrError<Self> {
        info!("Creating Explorer for {}", path);
        let (main_view, refresh_rate) = match path.display_kind() {
            DisplayKind::ContextTable => {
                let refresh_rate = TEN_SECS;
                (
                    ExplorerTable::new_view(
                        ContextsTable::new_shared(
                            Arc::clone(&kube_config),
                            Arc::clone(&path),
                        ),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::NamespaceTable => {
                let refresh_rate = TWO_SECS;
                (
                    ExplorerTable::new_view(
                        ResourceTable::<cd::Namespace>::new_table_with_extra_data(
                            Arc::clone(&cache),
                            Arc::clone(&path),
                            (
                                "@cluster".into(),
                                cd::Namespace {
                                    name: "@cluster".into(),
                                },
                            ),
                        ),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::ClusterOverviewTableNamespaceScope => {
                let refresh_rate = TWO_SECS;
                (
                    ExplorerTable::new_view(
                        ClusterOverviewTable::new_shared(
                            crate::cluster_overview_table::TableScope::Namespace,
                            Arc::clone(&cache),
                            Arc::clone(&path),
                        ),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::ClusterOverviewTableClusterScope => {
                let refresh_rate = TWO_SECS;
                (
                    ExplorerTable::new_view(
                        ClusterOverviewTable::new_shared(
                            crate::cluster_overview_table::TableScope::Cluster,
                            Arc::clone(&cache),
                            Arc::clone(&path),
                        ),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::NamespaceResourceTable(resource_kind) => {
                let refresh_rate = TWO_SECS;
                use path::NamespaceResourceKind::*;
                let constructor = match resource_kind {
                    Pods => ResourceTable::<cd::Pod>::new_table,
                    Services => ResourceTable::<cd::Service>::new_table,
                    Ingresses => ResourceTable::<cd::Ingress>::new_table,
                    Deployments => ResourceTable::<cd::Deployment>::new_table,
                    DaemonSets => ResourceTable::<cd::DaemonSet>::new_table,
                    StatefulSets => ResourceTable::<cd::StatefulSet>::new_table,
                    ServiceAccounts => ResourceTable::<cd::ServiceAccount>::new_table,
                    RoleBindings => ResourceTable::<cd::RoleBinding>::new_table,
                    Roles => ResourceTable::<cd::Role>::new_table,
                    ConfigMaps => ResourceTable::<cd::ConfigMap>::new_table,
                    PersistentVolumeClaims => {
                        ResourceTable::<cd::PersistentVolumeClaim>::new_table
                    }
                    Secrets => ResourceTable::<cd::Secret>::new_table,
                };
                (
                    ExplorerTable::new_view(
                        constructor(Arc::clone(&cache), Arc::clone(&path)),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::ClusterResourceTable(resource_kind) => {
                let refresh_rate = TWO_SECS;
                use path::ClusterResourceKind::*;
                let constructor = match resource_kind {
                    Nodes => ResourceTable::<cd::Node>::new_table,
                    PersistentVolumes => ResourceTable::<cd::PersistentVolume>::new_table,
                };
                (
                    ExplorerTable::new_view(
                        constructor(Arc::clone(&cache), Arc::clone(&path)),
                        refresh_rate,
                    ),
                    refresh_rate,
                )
            }
            DisplayKind::PrettyPager => {
                let refresh_rate = TWO_SECS;
                let pager =
                    PrettyPager::new(Arc::clone(&path), Arc::clone(&cache), refresh_rate);
                (ExplorerPager::new_view(pager), refresh_rate)
            }
            DisplayKind::PodDetailPager => {
                let refresh_rate = TWO_SECS;
                let pager =
                    PrettyPager::new(Arc::clone(&path), Arc::clone(&cache), refresh_rate);
                (
                    PodDetailPager::new_view(ExplorerPager::new(pager)),
                    refresh_rate,
                )
            }
        };
        Ok(Self {
            path: Arc::clone(&path),
            main_view,
            refresh_rate,
        })
    }
}

impl Window for Explorer {
    fn tick(&mut self) {
        if let Err(err) = self.main_view.self_update(self.refresh_rate) {
            error!("Explorer view self-update error: {}", err);
        }
    }

    fn prompt(&self) -> Option<&Prompt> {
        self.main_view.prompt()
    }

    fn render(&mut self, buffer: &mut Buffer, tick: u64, rect: Rect) {
        if rect.height < 3 {
            error!("Not enough space to render explorer: {:?}", rect);
            return;
        }
        let rects = {
            use tui::layout::{Constraint::*, Layout};
            // - A :: the path line
            // - B :: the explorer tables (at least one line for header and one row)
            Layout::default()
                .constraints([Length(1) /* A */, Min(2) /* B */].as_ref())
                .split(rect)
        };
        render_path_line(
            buffer,
            &self.path,
            &self.main_view.last_updated(),
            tick,
            rects[0],
        );
        self.main_view.render(buffer, rects[1])
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        self.main_view.handle_key_event(key)
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        self.main_view.key_binds()
    }

    fn path(&self) -> Option<Arc<Box<dyn Path>>> {
        Some(Arc::clone(&self.path))
    }
}

fn render_path_line(
    buffer: &mut Buffer,
    path: &Arc<Box<dyn Path>>,
    last_updated: &LastUpdated,
    tick: u64,
    rect: Rect,
) {
    use tui::{
        layout::Alignment,
        style::Style,
        text::{Span, Spans},
        widgets::Paragraph,
    };
    let spinner = match last_updated {
        LU::NeverUpdates | LU::Updated(_) => Span::raw("  "),
        LU::FetchingData => {
            let text = match (tick / 10) % 8 {
                0 => " ",
                1 => "▝",
                2 => "▐",
                3 => "▟",
                4 => "█",
                5 => "▙",
                6 => "▌",
                7 => "▘",
                _ => panic!("Impossible"),
            };
            Span::styled(
                format!("{} ", text),
                Style::default().fg(palette::SPINNER_COLOR),
            )
        }
    };
    let path = Span::raw(format!("{}", path));
    let path = Paragraph::new(vec![Spans::from(vec![spinner, path])])
        .alignment(Alignment::Center);
    path.render(rect, buffer);
}
