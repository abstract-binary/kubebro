//! How this is supposed to work.
//!
//! 1. There's a dispatch at the top of [`Explorer`] selecting which
//! `Box<dyn ExplorerView>` to store in the state.  One of the
//! [`ExplorerView`]s is [`ExplorerTable`] which contains a `dyn
//! Table`.
//!
//! 2. There are different implementations of `Table` for each of the
//! possible path patterns.  Some of them are completely custom like
//! [`ClusterOverviewTable`] and some are reused for many different
//! tables like [`ResourceTable`].
//!
//! 3. When the explorer ticks, [`ExplorerTable::self_update`] is
//! called, which then calls [`Table::self_update`].  Each `Table` is
//! supposed to use the opportunity to check updated data in the
//! [`Cache`].  This will be called in accordance with `last_updated`
//! and the explorer's refresh rate.  If the underlying data has
//! changed, it is important for [`Table::self_update`] to update the
//! `cached_id` and `last_updated` parameters.  The former causes
//! [`ExplorerTable`] to render the new rows, and the latter is used
//! to draw the progress spinner in the terminal window.
//!
//! 4. When the display is rendered, if `cached_id` was changed by
//! [`Table::self_update`], then [`ExplorerTable`] will call
//! [`Table::rows`] and draw those rows.

use crate::cell::Cell;
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::LastUpdated;
use crate::path::Path;
use crate::unique::Unique;
use crate::window_result::{WindowResult, WindowResult as WR};

use std::fmt;
use std::ops::Deref;
use std::sync::Arc;
use std::time::Duration;

pub trait Table
where
    Self: fmt::Debug + 'static,
{
    fn self_update(
        &mut self,
        refresh_rate: Duration,
        cached_id: &mut Unique,
        last_updated: &mut LastUpdated,
    ) -> OrError<()>;

    fn index_header(&self) -> &str;
    fn non_index_headers(&self) -> &[&str];
    fn rows(&self) -> Vec<(Index, OrError<Vec<Cell>>)>;

    fn path(&self) -> Arc<Box<dyn Path>>;

    fn key_binds(&self, _context: TableContext) -> Vec<KeyBindDesc> {
        vec![]
    }

    fn handle_key_event(&mut self, _key: Key, _context: TableContext) -> WindowResult {
        WR::NotHandled
    }
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Index(String);

impl From<&str> for Index {
    fn from(str: &str) -> Self {
        Self(str.into())
    }
}

impl Deref for Index {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl fmt::Display for Index {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&**self, f)
    }
}

pub struct TableContext {
    pub selected: Option<Index>,
}
