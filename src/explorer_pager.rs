use crate::explorer_view::ExplorerView;
use crate::import::*;
use crate::key::Key;
use crate::key_bind::KeyBindDesc;
use crate::last_updated::LastUpdated;
use crate::pager::{Pager, RefTextFrags};
use crate::path::Path;
use crate::prompt::Prompt;
use crate::text_frag::TextFrag;
use crate::window_result::WindowResult;

use regex::Regex;
use std::cmp::{max, min};
use std::sync::Arc;
use std::time::Duration;
use tui::{buffer::Buffer, layout::Rect};

// The numbers below are i64 for ease of coding.  Semantically, they
// should be usize of u16, but we frequently have to subtract them in
// this module, and it's annoying to have to cast back and forth
// between i64s and the smaller unsigned values.
pub struct ExplorerPager {
    pager: Box<dyn Pager>,
    offset_cols: i64,
    offset_rows: i64,
    window_height: i64,
    window_width: i64,
    num_lines: i64,
    num_columns: i64,
    prompt: Option<Prompt>,
    active_search: Option<Regex>,
    focused_line: Option<i64>,
    tailing: bool,
    wrap: bool,
}

impl ExplorerPager {
    pub fn new(pager: Box<dyn Pager>) -> Self {
        Self {
            pager,
            offset_cols: 0,
            offset_rows: 0,
            window_height: 0,
            window_width: 0,
            num_lines: 0,
            num_columns: 0,
            prompt: None,
            active_search: None,
            focused_line: None,
            tailing: false,
            wrap: false,
        }
    }

    pub fn path(&self) -> Arc<Box<dyn Path>> {
        self.pager.path()
    }

    pub fn new_view(pager: Box<dyn Pager>) -> Box<dyn ExplorerView> {
        Box::new(Self::new(pager))
    }

    fn recenter(&mut self, focused_line: i64) {
        let new_offset_rows = focused_line - (self.window_height / 2);
        if new_offset_rows < 0 {
            self.offset_rows = 0;
        } else if new_offset_rows + self.window_height > self.num_lines {
            self.offset_rows = max(0, self.num_lines - self.window_height);
        } else {
            self.offset_rows = new_offset_rows;
        }
    }

    fn search_next(&mut self) -> OrError<()> {
        if let Some(active_search) = self.active_search.as_ref() {
            let start_from = self.focused_line.map(|x| x + 1).unwrap_or(self.offset_rows);
            self.focused_line = {
                let active_search = active_search.clone();
                self.pager
                    .with_text_iter(
                        start_from as usize,
                        Box::new(move |iter| find_gen(iter.enumerate(), &active_search)),
                    )?
                    .map(|x| x + start_from)
            };
            if self.focused_line.is_none() {
                self.focused_line = {
                    let active_search = active_search.clone();
                    self.pager.with_text_iter(
                        0,
                        Box::new(move |iter| find_gen(iter.enumerate(), &active_search)),
                    )?
                };
            }
            if let Some(focused_line) = self.focused_line {
                self.recenter(focused_line);
            }
        }
        Ok(())
    }

    fn search_previous(&mut self) -> OrError<()> {
        if let Some(active_search) = self.active_search.as_ref() {
            let start_from = if let Some(focused_line) = self.focused_line {
                if focused_line > 0 {
                    focused_line - 1
                } else {
                    self.num_lines - 1
                }
            } else {
                min(
                    self.offset_rows + self.window_height - 1,
                    self.num_lines - 1,
                )
            };
            // TODO-soon Actually implement column wrapping
            self.focused_line = {
                let active_search = active_search.clone();
                self.pager
                    .with_text_iter_rev(
                        (self.num_lines - start_from - 1) as usize,
                        Box::new(move |iter| find_gen(iter.enumerate(), &active_search)),
                    )?
                    .map(|x| start_from - x)
            };
            if self.focused_line.is_none() {
                self.focused_line = {
                    let active_search = active_search.clone();
                    self.pager
                        .with_text_iter_rev(
                            0,
                            Box::new(move |iter| {
                                find_gen(iter.enumerate(), &active_search)
                            }),
                        )?
                        .map(|x| self.num_lines - x - 1)
                };
            }
            if let Some(focused_line) = self.focused_line {
                self.recenter(focused_line);
            }
        }
        Ok(())
    }

    fn wrap_columns(&self) -> Option<usize> {
        if self.wrap {
            Some(self.window_width as usize)
        } else {
            None
        }
    }
}

impl ExplorerView for ExplorerPager {
    fn self_update(&mut self, refresh_rate: Duration) -> OrError<()> {
        self.pager.self_update(refresh_rate)
    }

    fn last_updated(&self) -> LastUpdated {
        self.pager.last_updated()
    }

    fn key_binds(&self) -> Vec<KeyBindDesc> {
        if self.prompt.is_some() {
            vec![
                key_bind!(["Esc"], "Exit search", "Exit search"),
                key_bind!(
                    ["Ret"],
                    "Search",
                    "Highlight matching lines and navigate between them"
                ),
            ]
        } else {
            let mut key_binds = vec![
                key_bind!(["/"], "Search", "Search the text"),
                key_bind_adv!(
                    ["h", "j", "k", "l", "Left", "Down", "Up", "Right"],
                    "Move",
                    "Move around the text"
                ),
                key_bind_adv!(
                    ["Spc", "PageDown", "PageUp"],
                    "Move",
                    "Move around text by screenfuls"
                ),
                key_bind_adv!(["0", "^"], "Move to left", "Move to the extreme left"),
                key_bind_adv!(
                    ["g", "Home"],
                    "Move to top",
                    "Move to the top of the text"
                ),
                key_bind_adv!(
                    ["F", "G", "End"],
                    "Move to bottom (tail)",
                    "Move to the bottom of the text and start tailing"
                ),
                key_bind_adv!(
                    ["W"],
                    "Wrap lines",
                    "Toggle wrapping lines to the screen width"
                ),
                key_bind!(["Esc", "Bksp"], "Up", "Go to the parent table"),
            ];
            if self.active_search.is_some() {
                key_binds.push(key_bind!(["n"], "Next", "Go to the next search match"));
                key_binds.push(key_bind!(
                    ["N"],
                    "Prev",
                    "Go to the previous search match"
                ));
            }
            key_binds
        }
    }

    fn prompt(&self) -> Option<&Prompt> {
        self.prompt.as_ref()
    }

    fn handle_key_event(&mut self, key: Key) -> WindowResult {
        use WindowResult as WR;
        if self.prompt.is_some() {
            match key.to_string().as_str() {
                "Esc" => {
                    self.prompt = None;
                    WR::Handled
                }
                "Bksp" => {
                    let _: Option<char> = self.prompt.as_mut().unwrap().text.pop();
                    WR::Handled
                }
                "Ret" => {
                    let search_text = self.prompt.take().unwrap().text;
                    if search_text.is_empty() {
                        self.active_search = None;
                        self.focused_line = None;
                    } else {
                        self.active_search = smart_regex::new(&search_text);
                        if let Err(err) = self.search_next() {
                            error!("Search error: {}", err);
                        }
                    }
                    WR::Handled
                }
                "Spc" => {
                    // Prompt is definitely `Some` in this branch.
                    self.prompt.as_mut().unwrap().text.push(' ');
                    WR::Handled
                }
                str => match str.chars().collect::<Vec<char>>().as_slice() {
                    [ch] => {
                        // Prompt is definitely `Some` in this branch.
                        self.prompt.as_mut().unwrap().text.push(*ch);
                        WR::Handled
                    }
                    _ => WR::NotHandled,
                },
            }
        } else {
            match key.to_string().as_ref() {
                "Esc" | "Bksp" => WR::CloseSelf,
                "Down" | "j" => {
                    if self.offset_rows + self.window_height < self.num_lines {
                        self.offset_rows += 1;
                    }
                    WR::Handled
                }
                "Up" | "k" => {
                    if self.offset_rows > 0 {
                        self.offset_rows -= 1;
                    }
                    self.tailing = false;
                    WR::Handled
                }
                "Right" | "l" => {
                    if self.offset_cols + self.window_width < self.num_columns {
                        self.offset_cols += 1;
                    }
                    WR::Handled
                }
                "Left" | "h" => {
                    if self.offset_cols > 0 {
                        self.offset_cols -= 1;
                    }
                    WR::Handled
                }
                "g" | "Home" => {
                    self.offset_rows = 0;
                    self.tailing = false;
                    WR::Handled
                }
                "F" | "G" | "End" => {
                    if self.num_lines >= self.window_height {
                        self.offset_rows = self.num_lines - self.window_height;
                    }
                    self.tailing = true;
                    WR::Handled
                }
                "Spc" | "PageDown" => {
                    if self.offset_rows + self.window_height < self.num_lines {
                        self.offset_rows += self.window_height;
                    }
                    WR::Handled
                }
                "PageUp" => {
                    self.offset_rows = max(0, self.offset_rows - self.window_height);
                    self.tailing = false;
                    WR::Handled
                }
                "0" | "^" => {
                    self.offset_cols = 0;
                    WR::Handled
                }
                "/" => {
                    self.prompt = Some(Prompt::new("Find:"));
                    self.tailing = false;
                    WR::Handled
                }
                "n" => {
                    if let Err(err) = self.search_next() {
                        error!("Search error: {}", err);
                    }
                    self.tailing = false;
                    WR::Handled
                }
                "N" => {
                    if let Err(err) = self.search_previous() {
                        error!("Search error: {}", err);
                    }
                    self.tailing = false;
                    WR::Handled
                }
                "W" => {
                    self.wrap = !self.wrap;
                    WR::Handled
                }
                _ => WR::NotHandled,
            }
        }
    }

    fn render(&mut self, buf: &mut Buffer, rect: Rect) {
        self.window_height = rect.height as i64;
        self.window_width = rect.width as i64;
        let wrap_columns = self.wrap_columns();
        self.num_lines = self.pager.num_lines(wrap_columns).unwrap_or(0) as i64;
        if self.tailing && self.num_lines > self.offset_rows + self.window_height {
            self.offset_rows = self.num_lines - self.window_height;
        }
        let offset_rows = self.offset_rows as usize;
        self.num_columns = self
            .pager
            .num_columns(offset_rows, rect.height as usize, wrap_columns)
            .unwrap_or(0) as i64;
        let mut y = rect.y;
        #[allow(clippy::redundant_closure)]
        for (idx, line_frags) in self
            .pager
            .text(offset_rows, rect.height as usize, wrap_columns)
            .unwrap_or_else(|_| vec![])
            .into_iter()
            .enumerate()
        {
            let idx = idx + offset_rows;
            let line_frags = highlight_frags(
                line_frags,
                &self.active_search,
                self.focused_line.map(|x| idx as i64 == x).unwrap_or(false),
                self.window_width,
            );
            let mut x = 0;
            let mut line_pos = 0;
            'line_printer: for s in line_frags {
                for frag_char in s.text.chars().collect::<Vec<char>>() {
                    if self.offset_cols <= line_pos {
                        buf.set_string(x, y, frag_char.to_string(), s.style);
                        x += 1;
                        if x >= rect.width {
                            break 'line_printer;
                        }
                    }
                    line_pos += 1;
                }
            }
            y += 1;
        }
    }
}

fn highlight_frags(
    mut frags: Vec<TextFrag>,
    active_search: &Option<Regex>,
    line_focused: bool,
    window_width: i64,
) -> Vec<TextFrag> {
    use tui::style::{Modifier, Style};
    let line_style = {
        let mut line_style = Style::default();
        if line_focused {
            line_style = line_style.add_modifier(Modifier::REVERSED);
        }
        line_style
    };
    frags.iter_mut().for_each(|x| x.patch_style(line_style));
    let line = line_of_frags(&frags);
    if let Some(active_search) = active_search {
        let captures: Vec<_> = active_search.captures_iter(&line).collect();
        if !captures.is_empty() {
            let mut styled_line: Vec<_> = frags
                .into_iter()
                .flat_map(|x| TextFrag::split_chars(&x))
                .collect();
            for cap in captures.into_iter() {
                // There's always a whole capture so this can never fail.
                let mat = cap.get(0).unwrap();
                for i in mat.start()..mat.end() {
                    if i < styled_line.len() {
                        // TODO-soon There's a weird interaction
                        // between utf-8 and captures: When we split
                        // the text with `TextFrag::split_chars` we do
                        // so at `char` boundries.  When we get match
                        // ranges with `mat.start()` and `mat.end()`,
                        // we get byte offsets.  So, the matches might
                        // be longer numerically than the actual
                        // number of characters.  I think we should
                        // probably do the cleverer thing there
                        // instead of just splitting into one
                        // `TextFrag` per `char`.
                        styled_line[i].patch_style(
                            Style::default()
                                .fg(palette::SELECTED_TEXT_FG)
                                .add_modifier(Modifier::BOLD),
                        )
                    }
                }
            }
            frags = styled_line;
        }
    }
    if window_width > line.len() as i64 {
        frags.push(TextFrag::styled(
            " ".repeat(window_width as usize - line.len()),
            line_style,
        ))
    }
    frags
}

fn find_gen<'a>(
    mut lines: impl Iterator<Item = (usize, RefTextFrags<'a>)>,
    active_search: &Regex,
) -> Option<i64> {
    lines.find_map(|(idx, frags)| {
        let line = line_of_frags(frags.as_ref());
        if active_search.is_match(&line) {
            Some(idx as i64)
        } else {
            None
        }
    })
}

fn line_of_frags(frags: &[TextFrag]) -> String {
    frags
        .iter()
        .map(|s| s.text.clone())
        .collect::<Vec<String>>()
        .join("")
}
