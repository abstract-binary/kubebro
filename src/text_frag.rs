#[allow(unused_imports)]
use crate::import::*;

use std::fmt;
use tui::style::Style;

#[derive(Clone, Debug)]
pub struct TextFrag {
    pub text: String,
    pub style: Style,
}

impl TextFrag {
    pub fn new(text: String) -> Self {
        Self {
            text,
            style: Style::default(),
        }
    }

    pub fn styled(text: String, style: Style) -> Self {
        Self { text, style }
    }

    pub fn patch_style(&mut self, other_style: Style) {
        self.style = self.style.patch(other_style);
    }

    pub fn split_chars(&self) -> Vec<Self> {
        self.text
            .chars()
            .map(|ch| Self {
                text: ch.to_string(),
                style: self.style,
            })
            .collect()
    }
}

impl fmt::Display for TextFrag {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}
