#[allow(unused_imports)]
use crate::import::*;

use std::fmt;
use std::sync::atomic::{AtomicU64, Ordering};

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Unique(u64);

static NEXT_ID: AtomicU64 = AtomicU64::new(1);

impl Unique {
    #[allow(clippy::new_without_default)]
    /// Creates a new `Id` that has never been returned in this run of
    /// the program.
    pub fn new() -> Self {
        let id = NEXT_ID.fetch_add(1, Ordering::Relaxed);
        Self(id)
    }
}

impl fmt::Display for Unique {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Unique[{}]", self.0)
    }
}
