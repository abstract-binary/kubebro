use crate::cluster_command::{self as cc, ClusterCommand};
use crate::endpoint::Endpoint;
use crate::import::*;
use crate::kube_config::KubeConfig;
use crate::path::Path;
use crate::tagged_line::TaggedLine;
use crate::unique::Unique;

use std::collections::HashMap;
use std::fmt;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time::{Duration, Instant};

const ZERO_SECS: Duration = Duration::from_secs(0);
const ONE_SEC: Duration = Duration::from_secs(1);
const TEN_YEARS: Duration = Duration::from_secs(60 * 60 * 24 * 365 * 10);

#[derive(Debug)]
pub struct Cache {
    endpoints: Arc<HashMap<KContext, Arc<Endpoint>>>,
    table: Arc<Mutex<HashMap<Key, Entry>>>,
}

#[derive(Clone, Debug)]
enum Key {
    Get(Arc<Box<dyn Path>>),
    Oneoff {
        cmd: Arc<Box<dyn cc::OneoffCommand>>,
        msg: String,
    },
    StreamLines(Arc<Box<dyn cc::StreamCommand>>),
}

use Key::*;

#[derive(Debug)]
struct Entry {
    id: Unique,
    time: Instant,
    value: EntryValue,
    in_progress: bool,
}

#[derive(Debug)]
enum EntryValue {
    NoneYet,
    ResponseValue(OrError<Arc<cc::Response>>),
    StreamedLines(OrError<Arc<Mutex<Vec<TaggedLine>>>>),
}

use EntryValue::*;

impl PartialEq for Key {
    fn eq(&self, other: &Self) -> bool {
        // TODO-soon One this PR is merged in rust 1.53, we can
        // shorten the explicit matches at the end of the match:
        // https://github.com/rust-lang/rust/pull/79278
        match (self, other) {
            (Get(path), Get(other_path)) => path.eq(other_path),
            (Oneoff { cmd, .. }, Oneoff { cmd: other_cmd, .. }) => cmd.eq(other_cmd),
            (StreamLines(path), StreamLines(other_path)) => path.eq(other_path),
            (Get(_), Oneoff { .. })
            | (Get(_), StreamLines(_))
            | (Oneoff { .. }, Get(_))
            | (Oneoff { .. }, StreamLines(_))
            | (StreamLines(_), Get(_))
            | (StreamLines(_), Oneoff { .. }) => false,
        }
    }
}

impl Eq for Key {}

impl std::hash::Hash for Key {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        match self {
            Get(path) => path.hash(state),
            Oneoff { cmd, .. } => cmd.hash(state),
            StreamLines(path) => path.hash(state),
        }
    }
}

impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Get(path) => write!(f, "GET {}", path),
            Oneoff { cmd, .. } => write!(f, "ONEOFF {}", cmd),
            StreamLines(path) => write!(f, "STREAM {}", path),
        }
    }
}

#[must_use = "The result of a Cache query must be used."]
pub enum Result<T> {
    Missing,
    Fresh(T, Instant, Unique),
    Stale(T, Instant, Unique),
}

impl<T> Result<T> {
    pub fn map<F, U>(self, f: F) -> Result<U>
    where
        F: Fn(T) -> U,
    {
        use self::Result::*;
        match self {
            Missing => Missing,
            Fresh(x, time, id) => Fresh(f(x), time, id),
            Stale(x, time, id) => Stale(f(x), time, id),
        }
    }
}

enum ExecuteInternalResult {
    Resp(Arc<cc::Response>),
    Lines(Arc<Mutex<Vec<TaggedLine>>>),
}

use ExecuteInternalResult as EIR;

impl Cache {
    pub fn new(kube_config: Arc<Box<dyn KubeConfig>>) -> OrError<Self> {
        // TODO-soon Don't crate all endpoints eagerly.
        // Create all the possible endpoints eagerly so that we can
        // use them later.  Creating them lazily is probably more
        // trouble than it's worth and creating them on every request
        // is going to be wasetful because we're planning to do lots
        // of requests.
        let endpoints = kube_config
            .contexts()
            .iter()
            .filter_map(|(context_name, context)| {
                match Endpoint::try_from_kube_context(context) {
                    Err(error) => {
                        warn!(
                            "Could not create endpoint for context {}: {}",
                            context_name, error
                        );
                        None
                    }
                    Ok(endpoint) => Some((context_name.clone(), Arc::new(endpoint))),
                }
            })
            .collect();
        Ok(Self {
            endpoints: Arc::new(endpoints),
            table: Arc::new(Mutex::new(HashMap::new())),
        })
    }

    /// Get the value associated with a `Path`.  This function never
    /// blocks (other than waiting for a mutex which should always be
    /// unlocked quickly.)
    ///
    /// The function returns `None`:
    ///
    /// - If the value isn't in the cache, or
    ///
    /// - If the value in the cache is older than `max_stale`, or
    ///
    /// - If another `get` was run less more recently than
    /// `max_stale`.
    ///
    /// The function returns `Err` if either the query failed or if
    /// the cache has been poisoned (e.g. by another thread panicking
    /// while holding the mutex).
    pub fn get(
        &self,
        path: Arc<Box<dyn Path>>,
        max_stale: Duration,
    ) -> OrError<Result<Arc<cc::Response>>> {
        use self::Result::*;
        match self.execute_internal(Get(Arc::clone(&path)), max_stale)? {
            Missing => Ok(Missing),
            Fresh(EIR::Resp(resp), time, id) => Ok(Fresh(resp, time, id)),
            Stale(EIR::Resp(resp), time, id) => Ok(Stale(resp, time, id)),
            Fresh(EIR::Lines(_), _, _) | Stale(EIR::Lines(_), _, _) => bail!(
                "Cache::get is broken. Got Lines instead of Response for {}",
                path
            ),
        }
    }

    pub fn oneoff(
        &self,
        cmd: Arc<Box<dyn cc::OneoffCommand>>,
        msg: String,
    ) -> OrError<Result<Arc<cc::Response>>> {
        use self::Result::*;
        match self.execute_internal(
            Oneoff {
                cmd: Arc::clone(&cmd),
                msg,
            },
            TEN_YEARS,
        )? {
            Missing => Ok(Missing),
            Fresh(EIR::Resp(resp), time, id) => Ok(Fresh(resp, time, id)),
            Stale(EIR::Resp(resp), time, id) => Ok(Stale(resp, time, id)),
            Fresh(EIR::Lines(_), _, _) | Stale(EIR::Lines(_), _, _) => bail!(
                "Cache::oneoff is broken. Got Lines instead of Response for {:?}",
                cmd
            ),
        }
    }

    pub fn stream_lines(
        &self,
        cmd: Arc<Box<dyn cc::StreamCommand>>,
    ) -> OrError<Result<Arc<Mutex<Vec<TaggedLine>>>>> {
        use self::Result::*;
        match self.execute_internal(StreamLines(Arc::clone(&cmd)), TEN_YEARS)? {
            Missing => Ok(Missing),
            Fresh(EIR::Lines(lines), time, id) => Ok(Fresh(lines, time, id)),
            Stale(EIR::Lines(lines), time, id) => Ok(Stale(lines, time, id)),
            Fresh(EIR::Resp(_), _, _) | Stale(EIR::Resp(_), _, _) => bail!(
                "Cache::stream_lines is broken. Got Response instead of Lines for {:?}",
                cmd
            ),
        }
    }

    fn execute_internal(
        &self,
        key: Key,
        max_stale: Duration,
    ) -> OrError<Result<ExecuteInternalResult>> {
        use self::Result::*;
        let now = Instant::now();
        match self.table.lock() {
            Ok(mut table) => {
                use std::collections::hash_map::Entry as HashMapEntry;
                let entry = table.entry(key.clone());
                match entry {
                    HashMapEntry::Vacant(entry) => {
                        // New entry => new id
                        let id = Unique::new();
                        entry.insert(Entry {
                            id,
                            value: NoneYet,
                            time: now,
                            in_progress: true,
                        });
                        start_running_query(
                            Arc::clone(&self.table),
                            key,
                            Arc::clone(&self.endpoints),
                            id,
                        );
                        Ok(Missing)
                    }
                    HashMapEntry::Occupied(mut entry) => {
                        let res = entry.get_mut();
                        if now.checked_duration_since(res.time).unwrap_or(ZERO_SECS)
                            >= max_stale
                        {
                            if !res.in_progress {
                                // Refreshing the value => new id
                                let id = Unique::new();
                                res.in_progress = true;
                                start_running_query(
                                    Arc::clone(&self.table),
                                    key,
                                    Arc::clone(&self.endpoints),
                                    id,
                                );
                            };
                            match res.value {
                                NoneYet
                                | ResponseValue(Err(_))
                                | StreamedLines(Err(_)) => Ok(Missing),
                                ResponseValue(Ok(ref v)) => {
                                    Ok(Stale(EIR::Resp(Arc::clone(v)), res.time, res.id))
                                }
                                StreamedLines(Ok(ref v)) => {
                                    Ok(Stale(EIR::Lines(Arc::clone(v)), res.time, res.id))
                                }
                            }
                        } else {
                            match res.value {
                                NoneYet => Ok(Missing),
                                ResponseValue(Ok(ref v)) => {
                                    Ok(Fresh(EIR::Resp(Arc::clone(v)), res.time, res.id))
                                }
                                StreamedLines(Ok(ref v)) => {
                                    Ok(Fresh(EIR::Lines(Arc::clone(v)), res.time, res.id))
                                }
                                ResponseValue(Err(ref error))
                                | StreamedLines(Err(ref error)) => {
                                    bail!("Previous error: {}", error)
                                }
                            }
                        }
                    }
                }
            }
            Err(error) => bail!("Cache's table is poisoned: {}", error),
        }
    }
}

fn start_running_query(
    table: Arc<Mutex<HashMap<Key, Entry>>>,
    key: Key,
    endpoints: Arc<HashMap<KContext, Arc<Endpoint>>>,
    id: Unique,
) {
    // We spawn the query here.  It will fill out the table slot
    // itself.  If it fails, it leave the table slot with
    // `in_progress=true` forever.
    let _: thread::JoinHandle<()> =
        thread::spawn(move || start_running_query_wrapper(table, key, endpoints, id));
}

fn start_running_query_wrapper(
    table: Arc<Mutex<HashMap<Key, Entry>>>,
    key: Key,
    endpoints: Arc<HashMap<KContext, Arc<Endpoint>>>,
    id: Unique,
) {
    if let Err(err) =
        start_running_query_internal(Arc::clone(&table), key.clone(), endpoints, id)
    {
        match table.lock() {
            Err(error) => error!(
                "Error updating cache for {}: Cache's table is poisoned: {}",
                id, error
            ),
            Ok(mut table) => {
                error!("Query error for {}: {}", key, err);
                if let Some(ref mut v) = table.get_mut(&key) {
                    v.id = id;
                    v.time = Instant::now();
                    v.value = match key {
                        Get(_) | Oneoff { .. } => ResponseValue(Err(err)),
                        StreamLines(_) => StreamedLines(Err(err)),
                    };
                    v.in_progress = false;
                };
            }
        };
    }
}

fn start_running_query_internal(
    table: Arc<Mutex<HashMap<Key, Entry>>>,
    key: Key,
    endpoints: Arc<HashMap<KContext, Arc<Endpoint>>>,
    id: Unique,
) -> OrError<()> {
    // TODO-soon Abstract these three branches since they look so similar.
    match key {
        Get(ref path) => match path.cluster_query() {
            Some((context, query)) => {
                let (responses_tx, responses_rx) = mpsc::sync_channel(1);
                let runner = {
                    let context = context.clone();
                    thread::spawn(move || {
                        run_and_time(&query, &context, endpoints, responses_tx)
                    })
                };
                for resp in responses_rx.into_iter() {
                    match table.lock() {
                        Err(error) => {
                            bail!("Cache's table is poisoned: {}", error);
                        }
                        Ok(mut table) => {
                            if let Some(ref mut v) = table.get_mut(&key) {
                                v.id = id;
                                v.time = Instant::now();
                                v.value = ResponseValue(Ok(Arc::new(resp)));
                                v.in_progress = false;
                            }
                        }
                    }
                }
                if let Err(err) = runner.join() {
                    bail!("Executor thread crashed: {:?}", err);
                }
            }
            None => bail!("Cluster query not implemented for {}", path),
        },
        Oneoff { ref cmd, ref msg } => {
            let (responses_tx, responses_rx) = mpsc::sync_channel(1);
            let runner = {
                let cmd = Arc::clone(cmd);
                thread::spawn(move || {
                    run_and_time(&**cmd, cmd.context(), endpoints, responses_tx)
                })
            };
            for resp in responses_rx.into_iter() {
                match table.lock() {
                    Err(error) => {
                        bail!("Cache's table is poisoned: {}", error);
                    }
                    Ok(mut table) => {
                        if let Some(ref mut v) = table.get_mut(&key) {
                            v.id = id;
                            v.time = Instant::now();
                            v.value = ResponseValue(Ok(Arc::new(resp.into())));
                            v.in_progress = false;
                        }
                    }
                }
            }
            match runner.join() {
                Ok(Ok(())) => warn!("{}", msg),
                Ok(Err(err)) => error!("Failed to execute {}: {}", msg, err),
                Err(err) => bail!("Executor thread crashed: {:?}", err),
            }
        }
        StreamLines(ref cmd) => {
            let (responses_tx, responses_rx) = mpsc::sync_channel(1);
            let runner = {
                let cmd = Arc::clone(cmd);
                thread::spawn(move || {
                    run_and_time(&**cmd, cmd.context(), endpoints, responses_tx)
                })
            };
            for resp in responses_rx.into_iter() {
                match table.lock() {
                    Err(error) => {
                        bail!("Cache's table is poisoned: {}", error);
                    }
                    Ok(mut table) => {
                        if let Some(ref mut v) = table.get_mut(&key) {
                            v.id = id;
                            v.time = Instant::now();
                            match &mut v.value {
                                NoneYet | ResponseValue(_) | StreamedLines(Err(_)) => {
                                    v.value =
                                        StreamedLines(Ok(Arc::new(Mutex::new(vec![
                                            resp.line,
                                        ]))));
                                }
                                StreamedLines(Ok(ref mut val)) => match val.lock() {
                                    Err(err) => {
                                        error!(
                                            "Cache mutex for {} was poisoned: {}",
                                            key, err
                                        );
                                        // TODO-soon Maybe replace the error with the new
                                        // value here.
                                    }
                                    Ok(ref mut val) => val.push(resp.line),
                                },
                            }
                            v.in_progress = false;
                        }
                    }
                }
            }
            match runner.join() {
                Ok(Ok(())) => (),
                Ok(Err(err)) => error!("Failed to execute StreamLines: {}", err),
                Err(err) => bail!("Executor thread crashed for StreamLines: {:?}", err),
            }
        }
    }
    Ok(())
}

fn run_and_time<CC, R>(
    query: &CC,
    context: &KContext,
    endpoints: Arc<HashMap<KContext, Arc<Endpoint>>>,
    responses_tx: mpsc::SyncSender<R>,
) -> OrError<()>
where
    CC: ClusterCommand<Response = R> + fmt::Debug + ?Sized,
    R: fmt::Debug,
{
    let query_str = format!("{:?}", query);
    info!("Querying {}", query_str);
    let endpoint = get_endpoint(context, &endpoints)?;
    let start = Instant::now();
    let res = query.execute(endpoint, responses_tx);
    debug!("Querying {} done: {:?}", query_str, res);
    let delta = Instant::now() - start;
    if delta >= ONE_SEC {
        info!("Long query {}: {:?}", query_str, delta);
    }
    res
}

fn get_endpoint(
    context: &KContext,
    endpoints: &Arc<HashMap<KContext, Arc<Endpoint>>>,
) -> OrError<Arc<Endpoint>> {
    endpoints
        .get(context)
        .ok_or_else(|| {
            anyhow!(
                "No Endpoint for context '{}' \
             (is there an earlier error about this endpoint?)",
                context
            )
        })
        .map(|x| Arc::clone(x))
}
