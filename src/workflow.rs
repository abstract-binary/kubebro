use crate::import::*;

use serde_derive::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct Workflow {
    pub initial_path: String,
    pub step: HashMap<String, Step>,
    pub service_config: String,
}

#[derive(Deserialize)]
pub struct Step {
    pub inputs: Vec<String>,
    #[serde(default)]
    pub expect_regex: Vec<String>,
    #[serde(default)]
    pub expect_not_regex: Vec<String>,
    #[serde(default)]
    pub expect_http_request_regex: Vec<String>,
}

impl Workflow {
    pub fn read_file(filename: &str) -> OrError<Workflow> {
        let text = std::fs::read_to_string(filename)
            .context(format!("Error reading workflow file {}", filename))?;
        let t = toml::from_str(&text)?;
        Ok(t)
    }

    pub fn steps(&self) -> Vec<(&String, &Step)> {
        let mut steps: Vec<(&String, &Step)> = self.step.iter().collect();
        steps.sort_by(|(name1, _), (name2, _)| name1.cmp(name2));
        steps
    }
}
