#[allow(unused_imports)]
use crate::import::*;

use crate::path::Path;
use crate::window::Window;

use std::fmt;
use std::sync::Arc;

#[must_use = "All WindowResult values must be used."]
pub enum WindowResult {
    /// The event was fully handled and there's nothing more to do
    Handled,
    /// The event was not handled and should be handled by the
    /// receiver
    NotHandled,
    /// Close this `Window` revealing the window underneath
    CloseSelf,
    /// Push a new overlay `Window`.  An overlay is drawn on top of
    /// the full window.  See `Ui` for more details.
    PushOverlayWindow(Box<dyn Window>),
    /// Push a new `Explorer` for the given path.
    Goto(Arc<Box<dyn Path>>),
}

use WindowResult::*;

impl fmt::Debug for WindowResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Handled => write!(f, "WR::Handled"),
            NotHandled => write!(f, "WR::NotHandled"),
            CloseSelf => write!(f, "WR::CloseSelf"),
            PushOverlayWindow(_) => write!(f, "WR::PushOverlayWindow(_)"),
            Goto(path) => write!(f, "WR::Goto({})", path),
        }
    }
}
