kubebro
=======

> Kubebro is a command-line Kubernetes resource browser. It lets you
> interactively explore Kubernetes clusters and exposes some common
> tasks as easy-to-use commands.

## Demo Video

[![Demo Video](demo/screenshot.png)](https://850dac34-bfb8-11ef-8df0-2b4d0581634f-dump.nbg1.your-objectstorage.com/recording.webm "Demo Video")

## Features

- View deployments, services, pods, configmaps, and much more in an
  interactive UI.
- View pod logs.
- Restart deployments, delete pods, and interact with the cluster with
  immediate feedback.

## Goals

- **Snappy** :: The primary directive is for the `kubebro` to not
  freeze/lag/pause on user input.
- **Compatible** :: `kubebro` should be compatible with other
  user-land Kubernetes tools, in particular with `kubectl`.
- **Useful** :: `kubebro` should commit to supporting specific
  workflows.  These will be documented and support for them will not
  be dropped in future versions.

## Non-goals

- **Complete** :: Although `kubebro` strives to support as many use
  cases as possible, this is secondary to the Goals.  In particular,
  `kubebro` is not aiming to be a `kubectl` replacement.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

A copy of the GNU General Public License is available in
[LICENSE](LICENSE) and at [GNU
Licenses](http://www.gnu.org/licenses/).
