TBD
===

Unreleased changes. Release notes have not yet been written.

### New features:
  - feature

### Changes:
  - change

### Bugfixes:
  - fix

v0.4 (2021-08-03)
===

This release adds cluster-scoped resources, log tailing for pods, and
horizontally scrollable tables.

### New features:
  - Add keybind to reload ingresses
  - Show pod logs by pressing 'L'
  - Show IPs and all ports in pods and services tables
  - Show Nodes and PersistentVolumes under the "@cluster" namespace
  - Support Bearer token authentication and getting the token from an
    external command
  - Truncate tables with many columns and scroll horizontally

### Changes:
  - Change log keybind from L to C-l help keybinding from H to C-h so
    that global keybinds always start with Ctrl.
  - Use smart-case for searches
  - Show unready pods as unhealthy
  - Optimize pager searching and rendering
  - Don't use Rust's async at all and just use threads

### Bugfixes:
  - Don't capture H, L, and ? when prompting
  - Fix crash when displaying utf-8 characters in pager
  - Fix crash when some numeric fields were omitted by Kubernetes in
    JSON responses

v0.3 (2021-03-30)
===

This release adds three more tables and adds tests.  The same testing
framework is then used to generate demo videos.

### New features:
  - Add ConfigMap, PersistentVolumeClaim, Secrets tables
  - Automatically generated demo video

### Changes:
  - Add usecase tests

### Bugfixes:
  - Fix artifact name in CI config


0.2 (2021-03-22)
===

This release adds coloring for the JSON and Help screens and improves
the scrolling behaviour of tables.  Under the hood, a lot of code was
refactored.

### New features:
  - Colour JSON and Help screens

### Changes:
  - Automatically go to the first result when searching
  - Refactor the table and JSON views
  - Refactor key handling (and include all the keys in the Help text)
  - Improve table scrolling

### Bugfixes:
  - none


0.1 (2021-03-09)
================

Initial release.

### Features:
  - Table-like navigation
  - Basic display of: `deployments`, `ingresses`, `pods`, `services`,
    `statefulsets`, `serviceaccounts`, `rolebindings`, `roles`
  - Basic JSON display of the above resources
  - Support for restarting `deployments` and `statefulsets`
  - Support for deleting `pods`
  - Support for searching through the JSON
  - Basic help screen
  - Log viewing by shelling out to `less` and `tail`
