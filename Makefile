.PHONY: all release test check

all:
	cargo build --workspace

release:
	cargo build --release --workspace

test:
	cargo build --tests
	timeout 60 cargo test --workspace -- --nocapture

check:
	cargo check --workspace
