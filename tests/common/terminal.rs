use crate::common::import::*;

use tui::{backend::TestBackend, terminal::Terminal};

pub fn new() -> TestTerminal {
    let backend = TestBackend::new(80, 10);
    Terminal::new(backend).unwrap()
}
