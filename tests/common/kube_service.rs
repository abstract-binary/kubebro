#[allow(unused_imports)]
use crate::common::import::*;

use hyper::{
    service::{make_service_fn, service_fn},
    {Body, Method, Request, Response, Server, Uri},
};
use kubebro::kube_config::KubeConfig;
use regex::Regex;
use serde_derive::Deserialize;
use std::convert::Infallible;
use std::net::SocketAddr;
use std::sync::{mpsc, Arc, Mutex};
use tokio::{runtime::Runtime, sync::oneshot};

#[derive(Deserialize)]
pub struct Config {
    pub route: Vec<Route>,
}

impl Config {
    pub fn read_file(filename: &str) -> OrError<Config> {
        let text = std::fs::read_to_string(filename)
            .context(format!("Reading kube_service config: {}", filename))?;
        let t = toml::from_str(&text)?;
        Ok(t)
    }

    fn find_matching_route(&self, method: &Method, uri: &Uri) -> Option<&Route> {
        self.route.iter().find(|route| {
            let query = Regex::new(&route.query).unwrap();
            route.method == method.to_string() && query.is_match(&uri.to_string())
        })
    }
}

#[derive(Deserialize)]
pub struct Route {
    pub method: String,
    pub query: String,
    pub response: String,
}

macro_rules! log {
    ($test_debug_enabled:ident, $($e:expr),*) => {
	if $test_debug_enabled {
            println!($($e),*);
        }
    }
}

pub fn run(
    config: Arc<Config>,
    shut_down_rx: oneshot::Receiver<()>,
    kube_config_tx: mpsc::Sender<Arc<Box<dyn KubeConfig>>>,
    http_requests: Arc<Mutex<Vec<String>>>,
    test_debug_enabled: bool,
) {
    let rt = Runtime::new().unwrap();
    let addr = ([127, 0, 0, 1], 0).into();
    let make_service = make_service_fn(|_conn| {
        let config = Arc::clone(&config);
        let http_requests = Arc::clone(&http_requests);
        async move {
            Ok::<_, Infallible>(service_fn(move |req| {
                let config = Arc::clone(&config);
                let http_requests = Arc::clone(&http_requests);
                handle(req, config, http_requests, test_debug_enabled)
            }))
        }
    });
    rt.block_on(async {
        log!(test_debug_enabled, "Kube service starting");
        let server = Server::bind(&addr).serve(make_service);
        log!(
            test_debug_enabled,
            "Kube service running on {}",
            server.local_addr()
        );
        kube_config_tx
            .send(Arc::new(kube_config(server.local_addr())))
            .unwrap();
        let shutdown = server
            .with_graceful_shutdown(async {
                shut_down_rx.await.ok();
            })
            .await;
        log!(test_debug_enabled, "Kube service stopped");
        shutdown.unwrap()
    });
}

pub fn kube_config(addr: SocketAddr) -> Box<dyn KubeConfig> {
    use kubebro::kube_config::FileKubeConfig;
    Box::new(FileKubeConfig::new_for_test(addr.port()))
}

async fn handle(
    req: Request<Body>,
    config: Arc<Config>,
    http_requests: Arc<Mutex<Vec<String>>>,
    test_debug_enabled: bool,
) -> OrError<Response<Body>> {
    use hyper::StatusCode;
    log!(
        test_debug_enabled,
        "[{} {}] Kube service got request",
        req.method(),
        req.uri()
    );
    {
        let mut http_requests = http_requests
            .lock()
            .map_err(|err| anyhow!("Error locking http_requests: {}", err))?;
        http_requests.push(format!("{} {}", req.method(), req.uri()));
    }
    match config.find_matching_route(req.method(), req.uri()) {
        None => {
            log!(
                test_debug_enabled,
                "[{} {}] Kube service did not find matching route",
                req.method(),
                req.uri()
            );
            let resp = Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())?;
            Ok(resp)
        }
        Some(route) => {
            let contents =
                std::fs::read_to_string(format!("tests/data/{}", route.response))?;
            log!(
                test_debug_enabled,
                "[{} {}] Kube service found matching route.  Sending {} ({} bytes)",
                req.method(),
                req.uri(),
                route.response,
                contents.as_bytes().len()
            );
            let resp = Response::builder().body(contents.into())?;
            Ok(resp)
        }
    }
}
