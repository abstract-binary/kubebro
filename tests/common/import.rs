//! Common imports for all the modules.

pub type OrError<T> = Result<T, anyhow::Error>;
pub type TestTerminal = tui::terminal::Terminal<tui::backend::TestBackend>;

pub use anyhow::{anyhow, bail, Context};
