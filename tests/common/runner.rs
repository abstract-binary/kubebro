#[allow(unused_imports)]
use crate::common::import::*;

use super::kube_service;
use kubebro::{key::Key, kube_config::KubeConfig, path::Path, workflow::Workflow};
use regex::Regex;
use std::fs::File;
use std::io::Write;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time::Duration;
use tokio::sync::oneshot;
use tui::{backend::TestBackend, buffer::Buffer, terminal::Terminal};

const TEST_STEP_TIMEOUT: Duration = Duration::from_millis(1000);

pub fn run_test(workflow_name: &str) -> OrError<()> {
    let workflow = Workflow::read_file(&format!("tests/data/{}.toml", workflow_name))?;
    let (shut_down_tx, shut_down_rx) = oneshot::channel();
    let service_config = Arc::new(kube_service::Config::read_file(&format!(
        "tests/data/{}",
        workflow.service_config
    ))?);
    let (kube_config_tx, kube_config_rx) = mpsc::channel();
    let http_requests = Arc::new(Mutex::new(vec![]));
    let kube_service = thread::spawn({
        let http_requests = Arc::clone(&http_requests);
        move || {
            kube_service::run(
                service_config,
                shut_down_rx,
                kube_config_tx,
                http_requests,
                test_debug_enabled(),
            )
        }
    });
    let kube_config: Arc<Box<dyn KubeConfig>> = kube_config_rx.recv()?;
    let (keys_tx, keys_rx) = mpsc::channel::<String>();
    let send_key = |key| keys_tx.send(key).unwrap();
    let terminal = Arc::new(Mutex::new(super::terminal::new()));
    let (log_fd, screen_logger) =
        kubebro::setup_logging(None, log::LevelFilter::Debug, false)?;
    let mut log_process = {
        use std::process::Command;
        if test_debug_enabled() {
            let pid = unsafe { libc::getpid() };
            Command::new("tail")
                .args(&["-f", "-s", "0.01", &format!("/proc/{}/fd/{}", pid, log_fd)])
                .spawn()?
        } else {
            Command::new("true").spawn()?
        }
    };
    let ui_thread = {
        let terminal = Arc::clone(&terminal);
        let screen_logger = screen_logger.clone();
        let kube_config = Arc::clone(&kube_config);
        let path = Arc::new(workflow.initial_path.parse::<Box<dyn Path>>().unwrap());
        thread::spawn(move || {
            kubebro::enable_stable_times();
            let mut poll_next_key = || -> OrError<Option<Key>> {
                match keys_rx.try_recv() {
                    Err(_) => Ok(None),
                    Ok(str) => Ok(Some(str.parse::<Key>()?)),
                }
            };
            kubebro::ui::run(
                terminal,
                &mut poll_next_key,
                kube_config,
                path,
                false,
                log_fd,
                screen_logger,
            )
            .unwrap()
        })
    };
    let mut trace_file = File::create(format!("tests/shots/{}.txt", workflow_name))?;
    let mut stdout = std::io::stdout();
    for (name, step) in workflow.steps() {
        step.inputs.iter().for_each(|s| send_key(s.clone()));
        {
            http_requests
                .lock()
                .map_err(|err| anyhow!("Locking http_requests failed: {}", err))?
                .clear();
        }
        if test_debug_enabled() {
            println!("Executing step {}", name);
            println!("Sending inputs: {:?}", step.inputs);
        }
        let mk_regexes = |strs: &Vec<String>| {
            strs.iter()
                .map(|s| Regex::new(s))
                .collect::<Result<Vec<_>, _>>()
        };
        let expect_regexes = mk_regexes(&step.expect_regex)?;
        let expect_not_regexes = mk_regexes(&step.expect_not_regex)?;
        let expect_http_request_regexes = mk_regexes(&step.expect_http_request_regex)?;
        let mut poll_res = poll_buffer_test(&terminal, |content| {
            let positive_matches = {
                let http_requests = http_requests
                    .lock()
                    .map_err(|err| anyhow!("Locking http_requests failed: {}", err))?;
                expect_regexes.iter().all(|r| r.is_match(content))
                    && expect_http_request_regexes.iter().all(|r| {
                        http_requests.iter().find(|req| r.is_match(req)).is_some()
                    })
            };
            if positive_matches {
                Ok(expect_not_regexes
                    .iter()
                    .find(|r| r.is_match(content))
                    .is_none())
            } else {
                Ok(false)
            }
        })
        .context(format!(
            "Expecting {:?}, not {:?}, and HTTP requests {:?}",
            expect_regexes, expect_not_regexes, expect_http_request_regexes
        ));
        print_terminal(&terminal, &mut trace_file)?;
        if test_debug_enabled() {
            println!("Terminal:");
            print_terminal(&terminal, &mut stdout)?;
        }
        let notices = screen_logger.take_notices();
        if !notices.is_empty() {
            println!("Notices logged:");
            for e in &notices {
                println!("{:?}", e);
            }
            poll_res = poll_res.context(format!("Notices logged: {:?}", notices.len()));
        }
        poll_res.context(format!("Failed at step {}", name))?;
    }
    send_key("C-c".into());
    shut_down_tx
        .send(())
        .map_err(|()| anyhow!("Error sending shutdown signal to HTTP server"))?;
    ui_thread
        .join()
        .map_err(|err| anyhow!("Error joining ui_thread: {:?}", err))?;
    // Waiting 50ms should be sufficient here because we're
    // running `tail` with a delay of 0.01s.
    thread::sleep(Duration::from_millis(50));
    log_process.kill()?;
    kube_service
        .join()
        .map_err(|err| anyhow!("Error joining ui_thread: {:?}", err))?;
    Ok(())
}

fn poll_buffer_test<F>(terminal: &Mutex<Terminal<TestBackend>>, test: F) -> OrError<()>
where
    F: Fn(&str) -> OrError<bool>,
{
    let mut time_spent = Duration::from_millis(0);
    loop {
        {
            let terminal = terminal
                .lock()
                .map_err(|err| anyhow!("Error locking terminal mutex: {}", err))?;
            let content = terminal
                .backend()
                .buffer()
                .content
                .iter()
                .map(|c| c.symbol.as_str())
                .collect::<Vec<&str>>()
                .join("");
            if test(&content)? {
                break;
            }
        }
        let delta = Duration::from_millis(10);
        time_spent += delta;
        if time_spent >= TEST_STEP_TIMEOUT {
            bail!("Timeout polling buffer");
        }
        // It's important to not be holding the mutex lock here
        thread::sleep(delta);
    }
    Ok(())
}

fn print_terminal(
    terminal: &Mutex<Terminal<TestBackend>>,
    out: &mut impl Write,
) -> OrError<()> {
    let terminal = terminal.lock().unwrap();
    print_buffer(terminal.backend().buffer(), out)
}

fn print_buffer(buf: &Buffer, out: &mut impl Write) -> OrError<()> {
    macro_rules! hr {
        () => {
            write!(out, "  +")?;
            for _ in 0..buf.area.width {
                write!(out, "-")?;
            }
            writeln!(out, "+")?;
        };
    }
    hr!();
    for row in 0..buf.area.height {
        write!(out, "  |")?;
        for col in 0..buf.area.width {
            write!(
                out,
                "{}",
                buf.content[(row * buf.area.width + col) as usize].symbol
            )?;
        }
        writeln!(out, "|")?;
    }
    hr!();
    Ok(())
}

fn test_debug_enabled() -> bool {
    std::env::var("TEST_DEBUG") == Ok("true".into())
}
