  +--------------------------------------------------------------------------------+
  |                                         /                                      |
  |Context                      User                  Cluster                      |
  |test-context                 test-user             test-cluster                 |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help EscUp RetDrill in                                             |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                                   /test-context                                |
  |Namespace                                                                       |
  |@cluster                                                                        |
  |default                                                                         |
  |kube-node-lease                                                                 |
  |kube-public                                                                     |
  |kube-system                                                                     |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help EscUp RetDrill in                                             |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |pvc-big-one     38Gi      longhorn     ReadWriteOnc Bound     Ok        >       |
  |pvc-small-one   2Gi       longhorn     ReadWriteOnc Released  Ok        >       |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help EscUp RetDrill in                                             |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |p┌Confirmation───────────────────────────────────────────────────────────────┐  |
  |p│ Are you sure you want to delete persistent volume @cluster/pvc-small-one? │  |
  | │                                                                           │  |
  | │                                 yYes nNo                                  │  |
  | └───────────────────────────────────────────────────────────────────────────┘  |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help DDelete EscUp NPrev RetDrill in nNext                         |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |pvc-big-one     38Gi      longhorn     ReadWriteOnc Bound     Ok        >       |
  |pvc-small-one   2Gi       longhorn     ReadWriteOnc Released  Ok        >       |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help DDelete EscUp NPrev RetDrill in nNext                         |
  |Info: Deleted pvc-small-one                                                     |
  |(C-l for details, SPACE to clear)                                               |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |pvc-big-one     38Gi      longhorn     ReadWriteOnc Bound     Ok        >       |
  |pvc-small-one   2Gi       longhorn     ReadWriteOnc Released  Ok        >       |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help DDelete EscUp NPrev RetDrill in nNext                         |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |pvc-big-one     38Gi      longhorn     ReadWriteOnc Bound     Ok        >       |
  |pvc-small-one   2Gi       longhorn     ReadWriteOnc Released  Ok        >       |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help DDelete EscUp NPrev RetDrill in nNext                         |
  |Error: Will not delete a bound PV. Unbind it first.                             |
  |(C-l for details, SPACE to clear)                                               |
  +--------------------------------------------------------------------------------+
  +--------------------------------------------------------------------------------+
  |                     /test-context/@cluster/persistentvolumes                   |
  |Persistent Volu Capacity  Storage Clas AccessModes  Phase     Healthy   >       |
  |pvc-big-one     38Gi      longhorn     ReadWriteOnc Bound     Ok        >       |
  |pvc-small-one   2Gi       longhorn     ReadWriteOnc Released  Ok        >       |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |                                                                                |
  |/Search C-h,?Help DDelete EscUp NPrev RetDrill in nNext                         |
  +--------------------------------------------------------------------------------+
