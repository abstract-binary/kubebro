mod common;

#[cfg(test)]
mod deployment_manip {
    use super::*;
    #[allow(unused_imports)]
    use common::import::*;
    use common::run_test;

    #[test]
    fn test() {
        run_test("deployment_manip").unwrap();
    }
}
