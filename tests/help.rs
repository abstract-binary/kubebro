mod common;

#[cfg(test)]
mod help {
    use super::*;
    #[allow(unused_imports)]
    use common::import::*;
    use common::run_test;

    #[test]
    fn test() {
        run_test("help").unwrap();
    }
}
