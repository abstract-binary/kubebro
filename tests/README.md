Kubebro Tests
=============

## All tests

There are unit tests and functional tests split across multiple
crates.  To execute all the tests, run this in the top directory:

```
$ make test
```

If the tests change the expected output in the repo (check `git
diff`), then double-check that this is desired.


## Debugging

To debug a single test (e.g. `explore` or `pod_manip`):

```
./run_test_debug.sh TEST_NAME
```
