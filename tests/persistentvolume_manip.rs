mod common;

#[cfg(test)]
mod pod_manip {
    use super::*;
    #[allow(unused_imports)]
    use common::import::*;
    use common::run_test;

    #[test]
    fn test() {
        run_test("persistentvolume_manip").unwrap();
    }
}
