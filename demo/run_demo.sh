#!/bin/bash

set -e -x

if [[ $# -lt 1 ]]; then
    echo "USAGE: $0 <workflow>"
    exit 1
fi
WORKFLOW="$1"

(cd .. && make release)
cp ../target/release/kubebro .

docker build -t kubebro-demo .
mkdir -p export
docker run --rm \
       --net host \
       --volume './:/demo:Z' \
       --volume './export:/export:Z' \
       --volume "$HOME/.kube/config:/root/.kube/config:Z,ro" \
       kubebro-demo \
       /demo/run_demo_inside_docker.sh "${WORKFLOW}"
