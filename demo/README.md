How to create demos
===================

## Record demo

Spin up a container with `Xvfb` and `ffmpeg` and record an `xterm`
with `kubebro` running in it.  The output will be in the `./export`
folder (which is not under version control).  The actual script run
inside of the container is `./run_demo_inside_docker.sh`.

```
$ ./run_demo.sh
```

## Getting frames

An easy way to get a poster for the video is to extract a frame a
second and look through them.

```
ffmpeg -i recording.webm -filter:v fps=fps=1/1 frame%03d.png
```
