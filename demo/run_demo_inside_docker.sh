#!/bin/bash

set -e -x

if [[ $# -lt 1 ]]; then
    echo "USAGE: $0 <workflow>"
    exit 1
fi
WORKFLOW="$1"

Xvfb :1 -screen 0 730x580x24 &
sleep 1
export DISPLAY=:1
export LANG=en_US.UTF-8
unset TERM
urxvt \
    +sb \
    -geometry 66x25+0+0 \
    -bg black \
    -fg white \
    -fn "xft:Fira Code:pixelsize=18,xft:DejaVu Sans Mono:pixelsize=18" \
    -e /bin/bash -l -c "/demo/kubebro --save-log /export/kubebro.log demo --workflow /demo/${WORKFLOW}; sleep 60" \
    &
ffmpeg \
    -video_size 730x580 \
    -framerate 10 \
    -f x11grab \
    -draw_mouse 0 \
    -i :1.0+0,0 \
    -c:v libvpx \
    -b:v 0 \
    -crf 10 \
    -y \
    export/recording.webm &
sleep 1
while pidof kubebro; do
    echo "Waiting for kubebro to terminate"
    sleep 1
done
kill $(pidof ffmpeg)
while pidof ffmpeg; do
    echo "Waiting for ffmpeg to terminate"
    sleep 1
done
