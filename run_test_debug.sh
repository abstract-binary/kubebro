#!/bin/bash

set -e -x

export TEST_DEBUG=true
cargo build --tests
timeout 60 cargo test --workspace --test "$1" -- --nocapture
