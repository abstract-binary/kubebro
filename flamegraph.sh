#!/bin/sh

set -e -x

# cargo install flamegraph
CARGO_PROFILE_RELEASE_DEBUG=true cargo flamegraph --bin kubebro "$@"

# Record with `perf record -p $(pidof kubebro) -g`
# perf script > out.perf
# stackcollapse-perf.pl out.perf > out.folded
# flamegraph.pl out.folded > flamegraph.svg
# rm out.perf out.folded
