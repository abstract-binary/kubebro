Release Checklist
-----------------

<!-- Copied and modified from
https://github.com/BurntSushi/ripgrep/blob/master/RELEASE-CHECKLIST.md
-->

* Run `cargo update` and review dependency updates. Commit updated
  `Cargo.lock`.
* Run `cargo outdated` and review semver incompatible updates. Unless there is
  a strong motivation otherwise, review and update every dependency.
* Update the CHANGELOG.md as appropriate.  Try using `git shortlog
  v0.1..master`.
* Commit the changes and create a new signed tag (`git tag -as vN.M`
  and `git push --tags`).
* Wait for CI to finish creating the release. If the release build fails, then
  delete the tag from GitLab, make fixes, re-tag, delete the release and push.
* Copy the relevant section of the CHANGELOG to the tagged release
  notes in the GitLab UI.
  Include this blurb describing what kubebro is:
  > As a reminder, `kubebro` is a command-line Kubernetes resource browser. It
  > facilitates exploration of Kubernetes clusters and exposes some common
  > tasks as easy to use commands.
<!-- * Run `cargo publish`. -->
* Add TBD section to the top of the CHANGELOG:
  ```
  TBD
  ===

  Unreleased changes. Release notes have not yet been written.

  ### New features:
    - feature

  ### Changes:
    - change

  ### Bugfixes:
    - fix
  ```
* Edit the `Cargo.toml` to set the new kubebro version.
